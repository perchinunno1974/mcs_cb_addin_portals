﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net;
using System.IO.Pipes;
using System.IO;
using System.Threading;
using System.Net.Sockets;

namespace BizmaticaAddin
{
    public partial class Launcher : Form
    {
        public Launcher()
        {
            InitializeComponent();

            button1.BringToFront();
            button1.Text = "login";

            var current = Process.GetCurrentProcess().Id;
            var chromeDriverProcesses = Process.GetProcesses().Where(pr => pr.ProcessName.StartsWith("InteractionDesktopLauncher") && current!= pr.Id);

            foreach (var process in chromeDriverProcesses)
            {
                process.Kill();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            login();
        }
        static Win32.AuthCookies credentials=new Win32.AuthCookies();
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                login();
                return true;    // indicate that you handled this keystroke
            }

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private string userNameEnc="";
        private string passwordEnc = "";
        private string domainEnc = "";

        private void login()
        {

            button1.Enabled = false;
            button1.Text = "wait";
            try
            {

                string userName = textBox1.Text.Replace(System.Environment.NewLine, "");
                string password = textBox2.Text.Replace(System.Environment.NewLine, "");
                string domain = textBox3.Text.Replace(System.Environment.NewLine, "");

                string machineName = System.Environment.MachineName; 
                var pathWithEnv = Environment.ExpandEnvironmentVariables(@"%" +Environment.SpecialFolder.UserProfile + @"%\AppData\Roaming\Interactive Intelligence\InteractionDesktop\Profiles\");
                var filePath = pathWithEnv + "Default.ininprofile";

                try
                {

                    FileStream fs1 = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter writer = new StreamWriter(fs1);
                    //writer.WriteLine("Workstation=" + machineName);
                    writer.WriteLine("LoginStationType=SipStation");
                    writer.Close();
                    
                }
                catch (Exception) {

                    Directory.CreateDirectory(pathWithEnv);
                    FileStream fs1 = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter writer = new StreamWriter(fs1);
                    //writer.WriteLine("Workstation=" + machineName);
                    writer.WriteLine("LoginStationType=SipStation");
                    writer.Close();
                }


                credentials = ContactServlet(textBox5.Text, userName, password, domain, "", Properties.Settings.Default.CookieName);
                Thread.Sleep(100);
                if (String.IsNullOrEmpty(credentials.SMSESSION))
                {
                    Thread.Sleep(100);
                    credentials = ContactServlet(textBox5.Text, userName, password, domain, "", Properties.Settings.Default.CookieName);
                }

                if (String.IsNullOrEmpty(credentials.SMSESSION))
                {
                    MessageBox.Show("Autenticazione fallita", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                    button1.Visible = true;
                    button1.Enabled = true;
                    button1.Text = "login";
                    return;
                }
                else
                {
                    MessageBox.Show("Autenticazione OK", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        public Win32.AuthCookies ContactServlet(string authUrl, string userName, string password, string domain, string machineName, string cookiename)
        {
            //string resultCookies = "";
            Win32.AuthCookies resultCookies = new Win32.AuthCookies();
            try
            {

                bool authOk = false;
                bool reqOk = false;

                string request = authUrl;
                int hops = 0;
                int maxHops = 15;
                HttpWebResponse webResp;
                System.Net.ServicePointManager.Expect100Continue = true;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(Win32.AcceptAllCertifications);

                CookieContainer cookies = new CookieContainer();
                NetworkCredential credential = null;
                if (!userName.Equals(""))
                    credential = new System.Net.NetworkCredential(userName, password, domain);
                else
                    credential = System.Net.CredentialCache.DefaultCredentials.GetCredential(new Uri(request), "Basic");
                do
                {
                    hops++;

                    textBox4.Text += Environment.NewLine + Environment.NewLine + ">>>>> Connessione #" + hops.ToString()+ " : " + request + Environment.NewLine;

                    System.Net.HttpWebRequest webReq = System.Net.WebRequest.Create(request) as System.Net.HttpWebRequest;
                    webReq.Credentials = credential;
                    webReq.PreAuthenticate = true;
                    webReq.CookieContainer = cookies;
                    webReq.KeepAlive = false;
                    webReq.Timeout = 20000;


                    webReq.ServicePoint.ConnectionLeaseTimeout = 20000;
                    webReq.ServicePoint.MaxIdleTime = 20000;

                    webReq.Method = "GET";
                    webReq.AllowAutoRedirect = false;
                    try
                    {
                        // Send the data.

                        webResp = webReq.GetResponse() as HttpWebResponse;
                        string sWebResponse = new System.IO.StreamReader(webResp.GetResponseStream()).ReadToEnd();
                        textBox4.Text += "**Http StatusCode: " + webResp.StatusCode.ToString() + Environment.NewLine;
                        textBox4.Text += "**WebResponse: " + sWebResponse + Environment.NewLine;
                        

                        webResp.Close();
                        textBox4.Text += "**Cookies: " + Environment.NewLine;
                        foreach (System.Net.Cookie cookie in webResp.Cookies)
                        {
                            textBox4.Text += "  >" + cookie.Name+": "+ cookie.Value + Environment.NewLine;

                            if (cookie.Name.Equals("cb-clicktrack-id"))
                            {
                                resultCookies.cbclicktrackId = cookie.Value;
                            }
                        }

                        foreach (System.Net.Cookie cookie in webResp.Cookies)
                        {
                            if (cookie.Name.Equals(cookiename))//"SMSESSION")
                            {
                                resultCookies.SMSESSION = cookie.Value;
                                if (!String.IsNullOrEmpty(resultCookies.SMSESSION) && !String.IsNullOrEmpty(resultCookies.cbclicktrackId))
                                {

                                    textBox4.Text += Environment.NewLine + Environment.NewLine + "Trovati i cookie di autenticazione" + Environment.NewLine;
                                    return resultCookies;
                                }
                            }
                        }

                        foreach (Cookie co in webResp.Cookies)
                            cookies.Add(co);

                        if (webResp.StatusCode == HttpStatusCode.Found)
                        {
                            request = webResp.Headers["Location"] as String;
                            textBox4.Text += "StatusCode is Found using " + request + " for next request" +  Environment.NewLine;

                        }
                        else
                        {
                            if (!authOk)
                            {
                                request = authUrl;


                                foreach (System.Net.Cookie cookie in webResp.Cookies)
                                {
                                    if (cookie.Name.Equals("cb-clicktrack-id"))
                                    {
                                        resultCookies.cbclicktrackId = cookie.Value;
                                    }
                                }

                                foreach (System.Net.Cookie cookie in webResp.Cookies)
                                {
                                    if (cookie.Name.Equals(cookiename))//"SMSESSION")
                                    {
                                        resultCookies.SMSESSION = cookie.Value;
                                        textBox4.Text += Environment.NewLine + Environment.NewLine + "Trovati i cookie di autenticazione" + Environment.NewLine;
                                        return resultCookies;
                                    }
                                }

                                authOk = true;
                            }
                            else
                            {

                                reqOk = true;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        textBox4.Text += System.Environment.NewLine + "Exception: "+ ex.Message;
                    }
                    finally
                    {
                        webReq.Abort();
                        webReq = null;
                    }
                } while ((hops < maxHops) && (!reqOk));
            }
            catch (Exception) { }

            return resultCookies;
        }
        private void Launcher_Load(object sender, EventArgs e)
        {
            //textBox1.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            textBox3.Text = Properties.Settings.Default.DOMAIN; 
            textBox1.Text = Environment.UserName;
            textBox2.Text = "";
            textBox2.Focus();
            button1.Enabled = true;
            linkLabel1.Text = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion;
            textBox4.Text = "SYSTEM INFO v" + FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion + System.Environment.NewLine + "User: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name + System.Environment.NewLine + "FQDN: " + GetLocalhostFqdn()+ System.Environment.NewLine + GetIP() + System.Environment.NewLine;



            TcpClient tc = null;
            try
            {
                tc = new TcpClient(Properties.Settings.Default.CICServer, 3952);
                textBox4.Text += "CIC connectivity OK" + Environment.NewLine;
                // If we get here, port is open
            }
            catch (SocketException se)
            {
                textBox4.Text += "CIC connection failure: " + se.Message + Environment.NewLine;
                // If we get here, port is not open, or host is not reachable
            }
            finally
            {
                if (tc != null)
                {
                    tc.Close();
                }
            }

            try
            {
                Uri myUri = new Uri(Properties.Settings.Default.AuthUrl);
                string host = myUri.Host;

                tc = new TcpClient(host, 443);
                textBox4.Text += "AUTH connectivity OK" + Environment.NewLine;
                // If we get here, port is open
            }
            catch (SocketException se)
            {
                textBox4.Text += "AUTH connection failure: " + se.Message + Environment.NewLine;
                // If we get here, port is not open, or host is not reachable
            }
            finally
            {
                if (tc != null)
                {
                    tc.Close();
                }
            }
            textBox4.Visible = true;


        }

        void StartServer()
        {
            Task.Factory.StartNew(() =>
            {
                var server = new NamedPipeServerStream("BizmaticaAddin");
                server.WaitForConnection();
                StreamReader reader = new StreamReader(server);
                StreamWriter writer = new StreamWriter(server);
                while (true)
                {
                    var line = reader.ReadLine();
                    if (line !=null && line.Equals("credentials")) { 
                        writer.WriteLine(String.Join("|", credentials.cbclicktrackId,credentials.SMSESSION, domainEnc, userNameEnc, passwordEnc, Base64Encode(Properties.Settings.Default.AuthUrl), Properties.Settings.Default.CookieName));
                        writer.Flush();
                        Thread.Sleep(2000);
                        this.Dispose();
                    }
                }
            });
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            textBox4.Visible = !textBox4.Visible;

            Thread thread = new Thread(() => Clipboard.SetText(textBox4.Text));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();
            if (textBox4.Visible) {
                this.Size = new Size(this.Size.Width, 278);
            }
            else {
                this.Size = new Size(this.Size.Width, 197);
            }

        }
        public static string GetLocalhostFqdn()
        {
            var ipProperties = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties();
            return string.IsNullOrWhiteSpace(ipProperties.DomainName) ? ipProperties.HostName : string.Format("{0}.{1}", ipProperties.HostName, ipProperties.DomainName);
        }
        public static string GetIP()
        {
            string res = "";
            int i = 1;
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress addr in localIPs)
            {
                if (addr.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    res+="IP"+ i.ToString()+": "+(addr)+Environment.NewLine;
                }
            }
            return res;
        }
    }
}
