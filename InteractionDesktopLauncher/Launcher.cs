﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net;
using System.IO.Pipes;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using ININ.IceLib;

namespace BizmaticaAddin
{
    public partial class Launcher : Form
    {
        public Launcher()
        {
            
            InitializeComponent();
            try
            { 
                pictureBox3.BringToFront();
                button1.BringToFront();
                button1.Text = "login";

                var current = Process.GetCurrentProcess().Id;
                var chromeDriverProcesses = Process.GetProcesses().Where(pr => pr.ProcessName.StartsWith("InteractionDesktopLauncher") && current!= pr.Id);

                foreach (var process in chromeDriverProcesses)
                {
                    process.Kill();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Si è verificato un errore durante l'inializzazione del processo:" + System.Environment.NewLine + e.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            login();
        }

        internal Win32.AuthCookies credentials=new Win32.AuthCookies();
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                login();
                return true;    // indicate that you handled this keystroke
            }

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private string userNameEnc="";
        private string passwordEnc = "";
        private string domainEnc = "";

        private async void login()
        {


            button1.Enabled = false;
            button1.Text = "wait";
            pictureBox2.Visible = true;
            pictureBox2.BringToFront();
            textBox1.ReadOnly = true;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;


            try
            {
                Tracing.TraceAlways("login()");
                string userName = textBox1.Text.Replace(System.Environment.NewLine, "");
                string password = textBox2.Text.Replace(System.Environment.NewLine, "");
                string domain = textBox3.Text.Replace(System.Environment.NewLine, "");
                bool firstTime = false;
                string machineName = System.Environment.MachineName;

                //Scrittura file di configurazione iniziale utilizzato dal client CIC
                var pathWithEnv = Environment.ExpandEnvironmentVariables(@"%" + Environment.SpecialFolder.UserProfile + @"%\AppData\Roaming\Interactive Intelligence\InteractionDesktop\Profiles\");
                var filePath = pathWithEnv + "Default.ininprofile";


                try
                {
                    if (!System.IO.Directory.Exists(pathWithEnv))
                    {
                        Tracing.TraceAlways("creating " + pathWithEnv);
                        System.IO.Directory.CreateDirectory(pathWithEnv);
                        firstTime = true;
                        Directory.CreateDirectory(pathWithEnv);
                    }

                    FileStream fs1 = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter writer = new StreamWriter(fs1);
                    //writer.WriteLine("Workstation=" + machineName);
                    writer.WriteLine("LoginStationType=SipStation");
                    writer.Close();
                    Tracing.TraceAlways("Scritture  " + filePath);

                }
                catch (Exception ex)
                {
                    Tracing.TraceException(ex, "Errore durante gestione cartella " + pathWithEnv);
                    //Non è un problema bloccante continua
                }

                //spostare in altro thread/task, per non bloccare interfaccia
                credentials = await Task.Run(() => Win32.ContactServlet(Properties.Settings.Default.AuthUrl, userName, password, domain, "", Properties.Settings.Default.CookieName));
                
                if (String.IsNullOrEmpty(credentials.SMSESSION))
                {
                    MessageBox.Show("Autenticazione fallita", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                    button1.Visible = true;
                    button1.Enabled = true;
                    button1.Text = "login";
                    pictureBox2.Visible = false;
                    textBox1.ReadOnly = false;
                    textBox2.ReadOnly = false;
                    textBox3.ReadOnly = false;
                    return;
                }

                domainEnc = Utilities.Base64Encode(domain);
                userNameEnc = Utilities.Base64Encode(userName);
                passwordEnc = Utilities.Base64Encode(password);

                //Faccio partire client CIC passando credenziali
                Tracing.TraceAlways("Lancio InteractionDesktop.exe");
                Process myProcess = new Process();
                StartServer();
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = "../../InteractionDesktop.exe";
                if (!firstTime)
                {
                    myProcess.StartInfo.Arguments = "/U=" + textBox3.Text + "\\" + textBox1.Text + " /P=" + textBox2.Text + " /N=" + Properties.Settings.Default.CICServer;
                }
                else
                {
                    myProcess.StartInfo.Arguments = "/U=" + textBox3.Text + "\\" + textBox1.Text + " /P=a /N=" + Properties.Settings.Default.CICServer;
                }
                Tracing.TraceAlways("Parametri " + myProcess.StartInfo.Arguments);
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();

                timer1.Enabled = true;
                this.Visible = false;


            }
            catch (Exception ex)
            {
                Tracing.TraceException(ex, "Si è verificato un errore durante la login");
                MessageBox.Show("Si è verificato un errore durante la login:" + System.Environment.NewLine + ex.Message);
            }
        }

        private void Launcher_Load(object sender, EventArgs e)
        {
            //textBox1.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            textBox3.Text = Properties.Settings.Default.DOMAIN; 
            textBox1.Text = Environment.UserName;
            textBox2.Text = "";
            textBox2.Focus();
            button1.Enabled = true;
            linkLabel1.Text = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion;
            textBox4.Text = "SYSTEM INFO v" + FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion + System.Environment.NewLine + "User: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name + System.Environment.NewLine + "FQDN: " + GetLocalhostFqdn()+ System.Environment.NewLine + GetIP() + System.Environment.NewLine;


            //Check connettività verso server CIC
            TcpClient tc = null;
            try
            {
                tc = new TcpClient(Properties.Settings.Default.CICServer, 3952);
                Tracing.TraceAlways("CIC connectivity OK");
                textBox4.Text += "CIC connectivity OK" + Environment.NewLine;
                // If we get here, port is open
            }
            catch (SocketException se)
            {
                Tracing.TraceException(se, "CIC connection failure");
                textBox4.Text += "CIC connection failure: " + se.Message + Environment.NewLine;
                // If we get here, port is not open, or host is not reachable
            }
            finally
            {
                if (tc != null)
                {
                    tc.Close();
                }
            }

            //Check connettività verso server Autenticazione
            try
            {
                Uri myUri = new Uri(Properties.Settings.Default.AuthUrl);
                string host = myUri.Host;

                tc = new TcpClient(host, 443);
                Tracing.TraceAlways("AUTH connectivity OK");
                textBox4.Text += "AUTH connectivity OK" + Environment.NewLine;
                // If we get here, port is open
            }
            catch (SocketException se)
            {
                Tracing.TraceException(se, "AUTH connection failure");
                textBox4.Text += "AUTH connection failure: " + se.Message + Environment.NewLine;
                // If we get here, port is not open, or host is not reachable
            }
            finally
            {
                if (tc != null)
                {
                    tc.Close();
                }
            }



        }

        //Mantengo credenziali finchè il client CIC non è partito e le prende in carico
        void StartServer()
        {
            Task.Factory.StartNew(() =>
            {
                Tracing.TraceAlways("NamedPipeServerStream \"BizmaticaAddin\" started");
                try { 
                    var server = new NamedPipeServerStream("BizmaticaAddin");
                    server.WaitForConnection();
                    StreamReader reader = new StreamReader(server);
                    StreamWriter writer = new StreamWriter(server);
                    while (true)
                    {
                        var line = reader.ReadLine();
                        if (line !=null && line.Equals("credentials")) { 
                            writer.WriteLine(String.Join("|", credentials.cbclicktrackId,credentials.SMSESSION, domainEnc, userNameEnc, passwordEnc, Utilities.Base64Encode(Properties.Settings.Default.AuthUrl), Properties.Settings.Default.CookieName));
                            writer.Flush();
                            Tracing.TraceAlways("NamedPipeServerStream is closing:" +System.Environment.NewLine+ String.Join("|", credentials.cbclicktrackId, credentials.SMSESSION, domainEnc, userNameEnc, "********", Utilities.Base64Encode(Properties.Settings.Default.AuthUrl), Properties.Settings.Default.CookieName));
                            Thread.Sleep(2000);
                            this.Dispose();
                        }
                    }
                }
                catch(Exception e)
                { 
                    MessageBox.Show("Si è verificato un errore durante il trasporto delle credenziali:" + System.Environment.NewLine + e.Message);
                }
            });
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            Tracing.TraceAlways("Il client CIC non è partito entro " + (timer1.Interval / 1000).ToString() + " secondi. Chiudo il launcher");
            //spengo il launcher dopo 10 min se non è partito il client CIC
            this.Dispose();
        }


        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            Tracing.TraceAlways("linkLabel1_LinkClicked");
            //copio contenuto textbox in clipboard
            Thread thread = new Thread(() => Clipboard.SetText(textBox4.Text));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            //rendo visibile textarea di log
            textBox4.Visible = !textBox4.Visible;
            if (textBox4.Visible) {
                this.Size = new Size(this.Size.Width, 278);
            }
            else {
                this.Size = new Size(this.Size.Width, 197);
            }

        }
        public static string GetLocalhostFqdn()
        {
            var ipProperties = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties();
            return string.IsNullOrWhiteSpace(ipProperties.DomainName) ? ipProperties.HostName : string.Format("{0}.{1}", ipProperties.HostName, ipProperties.DomainName);
        }
        public static string GetIP()
        {
            
            string res = "";
            try
            {
                int i = 1;
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (IPAddress addr in localIPs)
                {
                    if (addr.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        res+="IP"+ i.ToString()+": "+(addr)+Environment.NewLine;
                    }
                }
            }
            catch (Exception e) {
                MessageBox.Show("Si è verificato un errore durante il recupero dell'IP:" + System.Environment.NewLine + e.Message);
            }
            return res;
        }
    }
}
