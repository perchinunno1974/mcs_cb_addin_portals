﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using System.Threading;
using ININ.InteractionClient.AddIn;
using System.Text.RegularExpressions;
using ININ.IceLib.Configuration;
using System.Windows.Forms.VisualStyles;

namespace BizmaticaAddin
{

    public partial class TrasferimentoMassivo : Form
    {

        private ListViewColumnSorter lvwColumnSorter;

        public SynchronizationContext SyncContext { get; set; }
        public Session _session { get; set; }
        public ITraceContext _trace { get; set; }
        public CustomNotification _Notifier { get; set; }
        public IInteraction _SelectedInteraction { get; set; }
        

        public object workgroups { get; set; }
        public object users { get; set; }

        public TrasferimentoMassivo()
        {
            InitializeComponent();
        }

        private bool checkall = true;


        private void CBSchedulata_Load(object sender, EventArgs e)
        {
            lvwColumnSorter = new ListViewColumnSorter();
            this.listView1.ListViewItemSorter = lvwColumnSorter;
            this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView1_ColumnClick);
            this.comboBox2.GotFocus += ComboBox2_GotFocus;
        }


        internal void CustomNotificationReceivedHandler(object sender, CustomNotificationReceivedEventArgs e)
        {
            string sCustomNotification = "BIZMATICA: ";
            string[] strings = null;
            switch (e.Message.Header.CustomMessageType)
            {
                case CustomMessageType.ApplicationRequest:
                    sCustomNotification += "ApplicationRequest received";
                    break;
                case CustomMessageType.ApplicationResponse:
                    sCustomNotification += "ApplicationResponse received";
                    break;
                case CustomMessageType.ServerNotification:
                    sCustomNotification += "ServerNotification received";
                    sCustomNotification += System.Environment.NewLine + "EventId:" + e.Message.EventId;
                    sCustomNotification += System.Environment.NewLine + "ObjectId:" + e.Message.ObjectId;
                    sCustomNotification += System.Environment.NewLine + "ObjectType:" + e.Message.ObjectType;
                    sCustomNotification += System.Environment.NewLine + "Parameters:";
                    strings = e.Message.ReadStrings();
                    foreach (string s in strings)
                        sCustomNotification += System.Environment.NewLine + "          " + s;
                    break;
            }
            _trace.Always(sCustomNotification);

            if (e.Message.ObjectId.CompareTo("SearchResult") == 0)
            {
                populateView(strings);
            }


        }



        public void ShowForm()
        {
            _Notifier.CustomNotificationReceived += CustomNotificationReceivedHandler;
            CustomMessageHeader headerResponseTM = new CustomMessageHeader(CustomMessageType.ServerNotification, "SearchResult", _session.UserId);
            CustomMessageHeader[] headersList = new CustomMessageHeader[1];
            headersList[0] = headerResponseTM;
            _Notifier.StartWatching(headersList);

            foreach (WorkgroupConfiguration wg in (List<WorkgroupConfiguration>)workgroups)
            {
                lwitem currwg = new lwitem();
                currwg.Tag = wg;
                currwg.Text = wg.ConfigurationId.DisplayName;
                comboBox1.Items.Add(currwg);
                comboBox2.Items.Add(currwg);
            }
            foreach (UserConfiguration uC in (List<UserConfiguration>)users)
            {
                lwitem currwg = new lwitem();
                currwg.Tag = uC;
                currwg.Text = uC.ConfigurationId.Id + " - " + uC.ConfigurationId.DisplayName;
                comboBox2.Items.Add(currwg);
            }


            comboBox2.DisplayMember = "Text";
            comboBox2.PropertySelector = collection => collection.Cast<lwitem>().Select(p => p.Text);

            comboBox2.FilterRule = (item, text) => item.ToLower().Contains(text.ToLower().Trim());
            //suggestComboBox1.SuggestListOrderRule = s => s.Split(' ')[1];

            Show();


        }

        private void listView1_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
        {

            if (e.Column == 0)
            {
                if (checkall)
                {
                    checkall = false;
                }
                else
                {
                    checkall = true;
                }
                foreach(ListViewItem lv in listView1.Items)
                {
                    lv.Checked = !checkall;
                }

                return;
            }

            ListView myListView = (ListView)sender;
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            myListView.Sort();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null)
            {
                label7.Visible = true;
                return;
            }

            CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "EmailSearch", _session.UserId);
            CustomRequest request = new CustomRequest(header);
            string[] parametri = new string[] { comboBox1.SelectedItem.ToString(), textBox3.Text.Trim(), dateTimePicker1.Checked ? dateTimePicker1.Value.ToString("yyyyMMdd") : "", dateTimePicker2.Checked ? dateTimePicker2.Value.ToString("yyyyMMdd") : "", textBox1.Text.Trim(), textBox2.Text.Trim() };
            request.Write(parametri);

            _trace.Always("BIZMATICA:custom notification EmailSearch\n>" + parametri[0] + "\n>" + parametri[1] + "\n>" + parametri[2] + "\n>" + parametri [3] + "\n>" + parametri[4] + "\n>" + parametri[5]);
            _Notifier.SendServerRequestNoResponse(request);
        }
        private void ComboBox1_GotFocus(object sender, EventArgs e)
        {
            label7.Visible = false;
        }

        internal void populateView(string[] strings)
        {
            listView1.Items.Clear();
            foreach(string riga in strings)
            { 
                var listViewItem = new ListViewItem(riga.Split('§'));
                listView1.Items.Add(listViewItem);
            }
        }

        private void ComboBox2_GotFocus(object sender, EventArgs e)
        {
            label8.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            foreach (ListViewItem lwi in listView1.Items)
            {
                if (lwi.Checked) lwi.Selected = true;
                else lwi.Selected = false;
            }

            if (listView1.SelectedItems.Count == 0)
            {
                return;
            }
            if (comboBox2.SelectedItem==null)
            {
                label8.Visible = true;
                return;
            }
            string queue = "";

            

            if (((lwitem)comboBox2.SelectedItem).Tag.GetType().ToString().ToUpper().Contains("USER"))
            {
                queue = "User Queue:" + ((UserConfiguration)(((lwitem)comboBox2.SelectedItem).Tag)).ConfigurationId.Id;
            }
            else
                queue = "Workgroup Queue:" + ((WorkgroupConfiguration)(((lwitem)comboBox2.SelectedItem).Tag)).ConfigurationId.Id;

            string[] cnParams=new string[listView1.SelectedItems.Count+1];
            cnParams[0] = queue;
            for (int i=0; i<listView1.SelectedItems.Count;i++)
            {
                cnParams[i+1] = listView1.SelectedItems[i].Text;
            }
            foreach (ListViewItem lwi in listView1.Items)
            {
                lwi.Selected = false;
            }

            CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "TrasferimentoMassivo", _session.UserId);
            CustomRequest request = new CustomRequest(header);
            request.Write(cnParams);
            _trace.Always("BIZMATICA:custom notification TrasferimentoMassivo\n>" + _session.UserId + "\n" + string.Join("\n", cnParams));
            _Notifier.SendServerRequestNoResponse(request);
            this.Dispose();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            label8.Visible = false;
        }
    }
    internal class lwitem : System.Windows.Forms.ListViewItem
    {
        public override string ToString()
        {
            return this.Text;
        }
    }
}
