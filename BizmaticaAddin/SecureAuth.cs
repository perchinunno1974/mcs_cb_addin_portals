﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ININ.InteractionClient.AddIn;

namespace BizmaticaAddin
{
    public partial class SecureAuth : UserControl, ISecureInputForm
    {

        private bool _isValid;
        public event EventHandler IsValidChanged;

        public SecureAuth()
        {
            InitializeComponent();
            IsValid = true;///da modificare in seguito al comportamento dell'interfaccia, per esempio premendo un bottone
            SecureParameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }
        public object Content
        {
            get
            {
                return this;
            }
        }

        public bool IsValid
        {
            get
            {
                return _isValid;
            }
            private set
            {
                if (_isValid == value) return;
                _isValid = value;
                var evt = IsValidChanged;
                if (evt != null)
                {
                    evt(this, EventArgs.Empty);
                }
            }}

        public IDictionary<string, string> SecureParameters { get; private set; }

    }
}
