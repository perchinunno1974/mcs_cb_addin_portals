﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ININ.InteractionClient.AddIn;
using System.Threading;
using ININ.IceLib.Configuration;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.People;
using System.Net;
using System.IO.Pipes;
using System.IO;
using System.Xml;
using ININ.IceLib.QualityManagement;
using ININ.IceLib.Data.TransactionBuilder;
using System.Data;
using System.Diagnostics;
using System.Timers;

namespace BizmaticaAddin
{

    public class AddinQM : QueueMonitor
    {
        

        private SynchronizationContext _syncContext;
        private ConfigurationManager configurationManager;

        private XmlDocument xdom = new XmlDocument();
        private ITraceContext _trace;
        private Session _session;
        private CustomNotification _Notifier;
        private ININ.IceLib.Interactions.InteractionsManager interactionManager;
        private IInteraction SelectedInteraction;
        private INotificationService notificationService;
        private PeopleManager _PeopleManager;

        private string Inventia_HeartbeatURL="";
        private string Inventia_DisconnessioneURL = "";
        private int Inventia_HeartbeatInterval = 5;
        private string InventiaLauncher = "";
        private bool InventiaEnabled = false;
        private bool InventiaIsStopped = false;
        private bool InventiaFirstTime = true;

        private int Inventia_nTentativiPrimaSuccesso = 0;
        private int Inventia_maxTentativi = 10;

        private int Inventia_maxHeartbeatInterval = 30;
        private int WrapupTimeout = 300;
        private string AuthCredentials = "";

        private RecordingsManager _RecordingsManager;
        private TransactionClient transactionClient;
        private QualityManagementManager _QualityManagementManager;

        protected override IEnumerable<string> Attributes
        {
            get
            {
                return new string[]
                {
                    InteractionAttributes.State
                };
            }
        }



        public class lwitem : System.Windows.Forms.ListViewItem
        {
            public override string ToString()
            {
                return this.Text;
            }
        }

        


        protected override void OnLoad(IServiceProvider serviceProvider)
        {
            var parameters = Utilities.getInstance();
            string sofarsogood = "";
            
            try
            {
                

                #region Inizializzazione oggetti CIC
                _trace = serviceProvider.GetService(typeof(ITraceContext)) as ITraceContext;
                _trace.Always("BIZMATICA: addin.cs -  Load configuration");
                _syncContext = SynchronizationContext.Current;
                sofarsogood = "Inizializzazione oggetti";
                _session = serviceProvider.GetService(typeof(Session)) as Session;
                interactionManager = ININ.IceLib.Interactions.InteractionsManager.GetInstance(_session);
                _session.ConnectionStateChanged += _session_ConnectionStateChanged;
                configurationManager = ConfigurationManager.GetInstance(_session);
                _PeopleManager = PeopleManager.GetInstance(_session);
                UserEntryList uel = new UserEntryList(_PeopleManager);
                _QualityManagementManager = QualityManagementManager.GetInstance(_session);
                _RecordingsManager = _QualityManagementManager.RecordingsManager;
                transactionClient = new TransactionClient(_session);
                notificationService = (INotificationService)serviceProvider.GetService(typeof(INotificationService));
                IInteractionSelector selectInteractionEvent = (IInteractionSelector)serviceProvider.GetService(typeof(IInteractionSelector));
                selectInteractionEvent.SelectedInteractionChanged += new EventHandler(onSelectedInteractionChanged);
                _trace.Always("BIZMATICA: addin.cs -  Inizializzazione oggetti CIC per recupero configurazioni");

                #endregion

                #region StructuredParameters
                sofarsogood = "Caricamento StructuredParameters";
                _trace.Always("BIZMATICA: addin.cs -  Caricamento Lista StructuredParameters");
                StructuredParameterConfigurationList spl = new StructuredParameterConfigurationList(configurationManager);
                var querySP = spl.CreateQuerySettings().SetPropertiesToRetrieveToAll().SetRightsFilterToView();
                spl.StartCaching(querySP);
                parameters.structureParameters = spl.GetConfigurationList();
                spl.StopCaching();

                _trace.Always("BIZMATICA: addin.cs -  Assegnazione StructuredParameters");
                sofarsogood = "Assegnazione UrlAutenticazione";
                parameters.UrlAutenticazione = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("UrlAutenticazione")).Values[0];
                sofarsogood = "Assegnazione SMCookie";
                parameters.SMCookie = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("SMCookie")).Values[0];
                sofarsogood = "Assegnazione MsgCredenzialiErrate";
                parameters.MsgCredenzialiErrate = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("MsgCredenzialiErrate")).Values[0];
                sofarsogood = "Assegnazione MsgSessionExpired";
                parameters.MsgSessionExpired = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("MsgSessionExpired")).Values[0];
                sofarsogood = "Assegnazione WrapupTimeout";
                Int32.TryParse(parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("WrapupTimeout")).Values[0], out WrapupTimeout);
                sofarsogood = "Assegnazione PollingTimeout";
                if (WrapupTimeout > 3600) WrapupTimeout = 3600;
                int temp = parameters.PollingTimeout;
                Int32.TryParse(parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("PollingTimeout")).Values[0], out temp);
                parameters.PollingTimeout = temp;
                sofarsogood = "Assegnazione PollingUrl";
                parameters.PollingUrl = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("PollingUrl")).Values[0];
                sofarsogood = "Assegnazione ScheduledCallbackConfiguration Interval";
                temp = parameters.ScheduledCallbackConfigurationInterval;
                Int32.TryParse(parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("ScheduledCallbackConfiguration")).Parameters.Value.Single(x => x.Name.Equals("Interval")).Values[0], out temp);
                parameters.ScheduledCallbackConfigurationInterval = temp;
                sofarsogood = "Assegnazione unbluAutenticazioneUrl";
                parameters.unbluAutenticazioneUrl = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("unbluAutenticazioneUrl")).Values[0];
                sofarsogood = "Assegnazione unbluCredentials";
                parameters.unbluCredentials = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("unbluCredentials")).Values[0];
                sofarsogood = "Assegnazione unbluCookie";
                parameters.unbluCookie = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("unbluCookie")).Values[0];
                sofarsogood = "Assegnazione Inventia_HeartbeatURL";
                Inventia_HeartbeatURL = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("Inventia_HeartbeatURL")).Values[0];
                sofarsogood = "Assegnazione Inventia_DisconnessioneURL";
                Inventia_DisconnessioneURL = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("Inventia_DisconnessioneURL")).Values[0];
                sofarsogood = "Assegnazione Inventia_HeartbeatInterval";
                Int32.TryParse(parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("Inventia_HeartbeatInterval")).Values[0], out Inventia_HeartbeatInterval);
                sofarsogood = "Assegnazione Inventia_MaxHeartbeatInterval";
                Int32.TryParse(parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("Inventia_MaxHeartbeatInterval")).Values[0], out Inventia_maxHeartbeatInterval);
                sofarsogood = "Assegnazione Inventia_Launcher";
                InventiaLauncher = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals("AddinConfigurazione")).Parameters.Value.Single(x => x.Name.Equals("Inventia_Launcher")).Values[0];
                _trace.Always("BIZMATICA: addin.cs -  structureParameters configurated:" + String.Join(System.Environment.NewLine, "UrlAutenticazione: " + parameters.UrlAutenticazione, "SMCookie: " + parameters.SMCookie, "MsgCredenzialiErrate: " + parameters.MsgCredenzialiErrate, "MsgSessionExpired: " + parameters.MsgSessionExpired, "WrapupTimeout: " + WrapupTimeout.ToString(), "PollingTimeout: " + parameters.PollingTimeout.ToString(), "PollingUrl: " + parameters.PollingUrl, "ScheduledCallbackConfigurationInterval: " + parameters.ScheduledCallbackConfigurationInterval.ToString(), "unbluAutenticazioneUrl: " + parameters.unbluAutenticazioneUrl, "unbluCredentials: " + parameters.unbluCredentials, "unbluCookie: " + parameters.unbluCookie));
                #endregion

                #region Server parameters: Esitazione - gestione errore interna
                sofarsogood = "Caricamento valori Esitazione";
                string defaultEsitazione = "<root><item id=\"L3000\" desc=\"Inbound\"><item id=\"IDE\" desc=\"Default\"><item id=\"RDE\" desc=\"Default\"/></item></item><item id=\"L5000\" desc=\"Outbound\"><item id=\"ODE\" desc=\"Default\"><item id=\"RDE\" desc=\"Default\"/></item></item></root>";
                string ValoriEsitazione = "";
                ServerParameterConfiguration vE=null;
                try {
                    _trace.Always("BIZMATICA: addin.cs -  recupero Server parameters");
                    ServerParameterConfigurationList spcl = new ServerParameterConfigurationList(configurationManager);
                    var querySPLC = spcl.CreateQuerySettings().SetPropertiesToRetrieveToAll().SetRightsFilterToView();
                    spcl.StartCaching(querySPLC);
                    var serverParametersList = spcl.GetConfigurationList();
                    spcl.StopCaching();
                    vE = serverParametersList.FirstOrDefault(x => x.ConfigurationId.Id.Equals("ValoriEsitazione"));
                    ValoriEsitazione = vE==null? defaultEsitazione:vE.Value.Value;
                    xdom.LoadXml(ValoriEsitazione);
                }
                catch(Exception exc)
                {
                    xdom.LoadXml(defaultEsitazione);
                    _trace.Exception(exc, "BIZMATICA: addin.cs - Errore caricamento esiti xml da server parameter");
                    notificationService.Notify("Non è stato possibile caricare i valori di esitazione dalla configurazione. Verranno usati i valori di default.", "SP ValoriEsitazione", NotificationType.Error, new TimeSpan(0, 0, 30));
                }

                

                int conteggioNodiErrati = xdom.SelectNodes("//*/*").Cast<XmlNode>().Where(x=> (!x.ParentNode.Name.Equals("item") && !x.ParentNode.Name.Equals("root")) || !x.Name.Equals("item") || x.Attributes.Cast<XmlAttribute>().Count(y=>y.Name.Equals("id"))!=1 || x.Attributes.Cast<XmlAttribute>().Count(y => y.Name.Equals("desc")) != 1).Count();

                if (vE==null || !xdom.DocumentElement.Name.Equals("root") || conteggioNodiErrati>0 || xdom.SelectNodes("//*/root").Count>0)
                {
                    _trace.Error("BIZMATICA: addin.cs - Errore validazione xml esiti");
                    notificationService.Notify("Non è stato possibile caricare i valori di esitazione dalla configurazione. Verranno usati i valori di default.", "Xml ValoriEsitazione", NotificationType.Error, new TimeSpan(0, 0, 30));
                }
                #endregion

                #region Recupero credenziali da Launcher - gestione errore interna

                try { 
                    _trace.Always("BIZMATICA: addin.cs -  Looking for Bizmatica Launcher to get credentials");
                    //Client
                    var client = new NamedPipeClientStream("BizmaticaAddin");
                    client.Connect(1000);
                    StreamReader reader = new StreamReader(client);
                    StreamWriter writer = new StreamWriter(client);

                    _trace.Always("BIZMATICA: addin.cs -  Connesso a NamedPipeClientStream BizmaticaAddin");
                    while (true)
                    {
                        writer.WriteLine("credentials");
                        writer.Flush();
                        AuthCredentials = reader.ReadLine();
                        if (!String.IsNullOrEmpty(AuthCredentials)) break;
                    }
                
                    _trace.Always("BIZMATICA: addin.cs -  Credenziali recuperate da launcher");
                    
                    if (!String.IsNullOrEmpty(AuthCredentials) && AuthCredentials.Split('|').Length == 7)
                    {

                        parameters.clicktrackId = AuthCredentials.Split('|')[0];
                        parameters.AuthSession = AuthCredentials.Split('|')[1];

                        parameters.Domain = Utilities.Base64Decode(AuthCredentials.Split('|')[2]);
                        parameters.UserName = Utilities.Base64Decode(AuthCredentials.Split('|')[3]);
                        parameters.Password = Utilities.Base64Decode(AuthCredentials.Split('|')[4]);

                        parameters.AuthUrl = Utilities.Base64Decode(AuthCredentials.Split('|')[5]);
                        parameters.AuthCookieName = AuthCredentials.Split('|')[6];

                        _trace.Always("BIZMATICA: addin.cs -  clicktrackId >> " + System.Environment.NewLine + parameters.clicktrackId);
                        _trace.Always("BIZMATICA: addin.cs -  AuthSession >> " + System.Environment.NewLine + parameters.AuthSession);
                        _trace.Always("BIZMATICA: addin.cs -  Domain >> " + System.Environment.NewLine + parameters.Domain);
                        _trace.Always("BIZMATICA: addin.cs -  UserName >> " + System.Environment.NewLine + parameters.UserName);
                        _trace.Always("BIZMATICA: addin.cs -  AuthUrl >> " + System.Environment.NewLine + parameters.AuthUrl);
                        _trace.Always("BIZMATICA: addin.cs -  AuthCookieName >> " + System.Environment.NewLine + parameters.AuthCookieName);


                    }
                    else
                    {
                        sofarsogood = "Credenziali non recuperate correttamente";
                        _trace.Error("BIZMATICA: addin.cs -  Credenziali non recuperate correttamente");
                        throw new Exception("Credenziali non recuperate correttamente");
                    }
                }
                catch(Exception exc)
                {
                    _trace.Exception(exc, "BIZMATICA: addin.cs - Errore caricamento credenziali");
                    notificationService.Notify("Non è stato possibile recuperare correttamente le credenziali di autenticazione dei portali.", "Autenticazione", NotificationType.Error, new TimeSpan(0, 1, 0));

                }
                #endregion

                #region Credenziali Unblu - gestione errore interna
                try
                {
                    if (!String.IsNullOrEmpty(parameters.unbluCredentials)) parameters.unbluCookieValue = Win32.unbluSession(parameters.unbluAutenticazioneUrl, parameters.unbluCredentials, parameters.unbluCookie);
                    _trace.Always("BIZMATICA: addin.cs -  Credenziali unblu >> " + parameters.unbluCookieValue);
                }
                catch (Exception exc)
                {
                    _trace.Exception(exc, "BIZMATICA: addin.cs - Errore autenticazione Unblu ");
                    notificationService.Notify("Non è stato possibile autenticare il portale Unblu.", "Autenticazione", NotificationType.Error, new TimeSpan(0, 1, 0));

                }
                #endregion

                #region Inizializzazione eventi: notification e interazione selezionata - gestione errore interna
                try
                {
                    _trace.Always("BIZMATICA: addin.cs -  notification eventi");
                    _Notifier = new CustomNotification(_session);
                    _Notifier.CustomNotificationReceived += CustomNotificationReceivedHandler;

                    CustomMessageHeader headerResponseCB = new CustomMessageHeader(CustomMessageType.ServerNotification, "Client Button:CBSchedulata", _session.UserId);
                    CustomMessageHeader headerResponseTMButton = new CustomMessageHeader(CustomMessageType.ServerNotification, "Client Button:TrasferimentoMassivoEmail", _session.UserId);
                    //CustomMessageHeader headerResponseTM = new CustomMessageHeader(CustomMessageType.ServerNotification, "SearchResult", _session.UserId);
                    CustomMessageHeader headerResponseR = new CustomMessageHeader(CustomMessageType.ServerNotification, "CustomRequestResult", _session.UserId);
                    CustomMessageHeader headerResponseContext = new CustomMessageHeader(CustomMessageType.ServerNotification, "Context", _session.UserId);
                    CustomMessageHeader headerResponseCD = new CustomMessageHeader(CustomMessageType.ServerNotification, "CustomDisconnect", _session.UserId);
                    CustomMessageHeader[] headersList = new CustomMessageHeader[5];
                    headersList[0] = headerResponseCB;
                    headersList[1] = headerResponseR;
                    headersList[2] = headerResponseTMButton;
                    headersList[3] = headerResponseContext;
                    headersList[4] = headerResponseCD;
                    //headersList[3] = headerResponseTM;


                    _Notifier.StartWatching(headersList);
                    _trace.Always("BIZMATICA: addin.cs -  Listening CustomNotification - Client Button: CallbackCreation, " + _session.UserId);
                    _trace.Always("BIZMATICA: addin.cs -  Listening CustomNotification - CustomRequestResult, " + _session.UserId);
                    _trace.Always("BIZMATICA: addin.cs -  Listening CustomNotification - Client Button: TrasferimentoMassivoEmail, " + _session.UserId);
                    _trace.Always("BIZMATICA: addin.cs -  Listening CustomNotification - SearchResult, " + _session.UserId);
                    _trace.Always("BIZMATICA: addin.cs -  Listening CustomNotification - Context, " + _session.UserId);
                    _trace.Always("BIZMATICA: addin.cs -  Listening CustomNotification - CustomDisconnect, " + _session.UserId);
                }
                catch (Exception exc)
                {
                    _trace.Exception(exc, "BIZMATICA: addin.cs - Inizializzazione eventi ");
                    notificationService.Notify("Non è stato possibile inizializzare il gestore degli eventi", "Inizializzazione", NotificationType.Error, new TimeSpan(0, 1, 0));

                }
                _trace.Always("BIZMATICA: addin.cs -  Inizializzazione eventi completata");
                #endregion

                #region Workgroups - gestione errore interna
                try
                {
                    _trace.Always("BIZMATICA: addin.cs -  Caricamento Workgroups");
                    WorkgroupConfigurationList wgl = new WorkgroupConfigurationList(configurationManager);
                    var queryWG = wgl.CreateQuerySettings().SetPropertiesToRetrieve(WorkgroupConfiguration.Property.StandardProperties_CustomAttributes, WorkgroupConfiguration.Property.Members).SetRightsFilterToView();
                    queryWG.SetResultCountLimit(QueryResultLimit.Unlimited);
                    wgl.StartCaching(queryWG);
                    var workgroups = wgl.GetConfigurationList();
                    wgl.StopCaching();
                    _trace.Always("BIZMATICA: addin.cs -  Raggruppamento Workgroups");

                    parameters.workgroupsEmail.AddRange(workgroups.Where(x => x.StandardProperties.CustomAttributes.Value.Keys.Contains("ADDRESSINFILTER")));
                    parameters.workgroupsEmailTrasferimento.AddRange(workgroups.Where(x => x.StandardProperties.CustomAttributes.Value.Keys.Contains("DISPLAYINFILTER") && x.StandardProperties.CustomAttributes.Value["DISPLAYINFILTER"].Equals("1")));
                    parameters.workgroupsCB.AddRange(workgroups.Where(x => x.StandardProperties.CustomAttributes.Value.Keys.Contains("CALLBACKLIST") && x.StandardProperties.CustomAttributes.Value["CALLBACKLIST"].Equals("1")));
                    parameters.workgroupsINVENTIA.AddRange(workgroups.Where(x => x.StandardProperties.CustomAttributes.Value.Keys.Contains("INVENTIA") && x.StandardProperties.CustomAttributes.Value["INVENTIA"].Equals("1") && (x.Members.Value.Count(y => y.Id.ToUpper()==_session.UserId.ToUpper())>0)));
                    _trace.Always("BIZMATICA: addin.cs -  Assegnazione Workgroups completata");
                }
                catch (Exception exc)
                {
                    _trace.Exception(exc, "BIZMATICA: addin.cs - Caricamento Workgroups ");
                    notificationService.Notify("Non è stato possibile caricare la lista dei workgroup", "Inizializzazione", NotificationType.Error, new TimeSpan(0, 1, 0));

                }
                #endregion

                #region Inventia - gestione errore interna
                try
                {
                    _trace.Always("BIZMATICA: addin.cs - Inizializzazione Inventia");
                    activateInventiaWorkgroup(false);


                    Inventia_maxTentativi = Inventia_maxHeartbeatInterval / Inventia_HeartbeatInterval;
                    if (InventiaEnabled)
                    {
                        if (!InventiaLauncher.Equals("")) { 
                            _trace.Always("BIZMATICA: addin.cs - Lancio " + InventiaLauncher);
                            Process myProcess = new Process();
                            myProcess.StartInfo.UseShellExecute = false;
                            myProcess.StartInfo.FileName = InventiaLauncher;
                            //myProcess.StartInfo.Arguments = "/U=" + textBox3.Text + "\\" + textBox1.Text + " /P=" + textBox2.Text + " /N=" + Properties.Settings.Default.CICServer;
                            //_trace.Always("Parametri " + myProcess.StartInfo.Arguments);
                            myProcess.StartInfo.CreateNoWindow = true;
                            myProcess.Start();
                            _trace.Always("BIZMATICA: addin.cs - Processo Inventia partito");
                        }
                        else
                        {
                            _trace.Error("BIZMATICA: addin.cs - Inventia launcher not set");
                        }
                        _trace.Always("BIZMATICA: addin.cs - Starting timers");

                        System.Timers.Timer timerLogin = new System.Timers.Timer(Inventia_HeartbeatInterval*1000);
                        timerLogin.Elapsed += new System.Timers.ElapsedEventHandler(timerInventia_Elapsed);
                        timerLogin.Start();
                    }


                }
                catch (Exception exc)
                {
                    _trace.Exception(exc, "BIZMATICA: addin.cs - Inizializzazione Inventia");
                    notificationService.Notify("Non è stato possibile Inizializzare il flusso Inventia", "Inizializzazione", NotificationType.Error, new TimeSpan(0, 1, 0));

                }

                #endregion

                #region StatusMessages: Login - gestione errore interna
                _trace.Always("BIZMATICA: addin.cs -  StatusMessages");
                try
                {
                    StatusMessageConfigurationList smcl = new StatusMessageConfigurationList(configurationManager);
                    var querySMLC = smcl.CreateQuerySettings().SetPropertiesToRetrieveToAll().SetRightsFilterToView();
                    smcl.StartCaching(querySMLC);
                    var statusesList = smcl.GetConfigurationList();
                    smcl.StopCaching();
 
                    _trace.Always("BIZMATICA: addin.cs -  Impostazione Stato utente");
                    string UserInitialStatus = "";
                    StatusMessageList statuses = new StatusMessageList(_PeopleManager);
                    statuses.StartWatching();
                    StatusMessageDetails selectedStatus;

                    UserInitialStatus = statusesList.Single(x => x.StandardProperties.CustomAttributes.Value.Keys.Contains("Initial")).ConfigurationId.Id;
                    _trace.Always("BIZMATICA: addin.cs - Aggiornamento stato utente");
                    selectedStatus = statuses[UserInitialStatus];
                    UserStatusUpdate statusNew = new UserStatusUpdate(_PeopleManager);
                    statusNew.StatusMessageDetails = selectedStatus;
                    statusNew.UpdateRequest();
                    statuses.StopWatching();
                    _trace.Always("BIZMATICA: addin.cs -  Impostato stato utente:" + selectedStatus);
                }
                catch (Exception e)
                {
                    notificationService.Notify("Si è verificato un errore nell'impostazione dello stato iniziale di Login", "Sessione", NotificationType.Error, new TimeSpan(0, 0, 30));
                    _trace.Exception(e, "Si è verificato un errore nell'impostazione dello stato iniziale di Login");

                }


                #endregion

                #region Users - gestione errore interna
                try
                {
                    _trace.Always("BIZMATICA: addin.cs -  Caricamento Utenti");
                    UserConfigurationList ul = new UserConfigurationList(configurationManager);
                    var queryUL = ul.CreateQuerySettings().SetPropertiesToRetrieve(UserConfiguration.Property.DisplayName, UserConfiguration.Property.Id).SetRightsFilterToView();
                    queryUL.SetResultCountLimit(QueryResultLimit.Unlimited);
                    ul.StartCaching(queryUL);
                    var usersList = ul.GetConfigurationList();
                    ul.StopCaching();
                    foreach (UserConfiguration uc in usersList)
                    {
                        parameters.users.Add(uc);
                    }

                    _trace.Always("BIZMATICA: addin.cs -  Inizialiazzazione classe addin avvenuta correttamente");

                }
                catch (Exception e)
                {
                    notificationService.Notify("Si è verificato un errore durante il caricamento Utenti", "Config", NotificationType.Error, new TimeSpan(0, 0, 45));
                    _trace.Exception(e, "Si è verificato un errore durante il caricamento Utenti.");

                }
                #endregion

            }
            catch(Exception exc)
            {
                notificationService.Notify("Si è verificato un errore durante l'inzializzazione del sistema. " + sofarsogood, "Inizializzazione", NotificationType.Error, new TimeSpan(0, 5, 0));
                _trace.Exception(exc,"Si è verificato un errore durante l'inzializzazione del sistema.");
            }
        }

        private void timerInventia_Elapsed(object sender, ElapsedEventArgs e)
        {
            try {
                if (_session==null) return;
                if (_session.ConnectionState!= ININ.IceLib.Connection.ConnectionState.Up) return;
                ((System.Timers.Timer)sender).Stop();
                _trace.Always("BIZMATICA: addin.cs - timerInventia_Elapsed: connection state=" + _session.ConnectionState.ToString());
                if (_session.ConnectionState != ININ.IceLib.Connection.ConnectionState.Up) return;
                if (Win32.InventiaHeartBeat(Inventia_HeartbeatURL, _session.UserId, "active"))
                {
                    Inventia_nTentativiPrimaSuccesso = 0;
                    _trace.Always("BIZMATICA: addin.cs - timerInventia_Elapsed: connection state=" + _session.ConnectionState.ToString());
                    
                    if (InventiaFirstTime)
                    {
                        activateInventiaWorkgroup(true);
                        InventiaFirstTime = false;
                        notificationService.Notify("Servizio Videostation attivo", "Inventia", NotificationType.Info, new TimeSpan(0,0,30));
                        Inventia_maxTentativi = 1;
                    }
                    else
                    {
                        if (InventiaIsStopped)
                        {
                            activateInventiaWorkgroup(true);
                            notificationService.Notify("Servizio Videostation riattivato ", "Inventia", NotificationType.Info, new TimeSpan(0, 0, 30));
                        }
                    }
                    InventiaIsStopped = false;
                }
                else
                {
                    //non risponde
                    if (!InventiaFirstTime)
                    { 
                        InventiaIsStopped = true;
                    }
                    Inventia_nTentativiPrimaSuccesso++;
                    if (Inventia_nTentativiPrimaSuccesso==Inventia_maxTentativi)
                    {
                        if (InventiaFirstTime)
                        {
                            notificationService.Notify("Non è stato possibile attivare il workgroup delle code Inventia", "Inventia", NotificationType.Error, new TimeSpan(0, 0, 60));
                        }
                        else
                        {
                            notificationService.Notify("Il servizio Videostation non risponde", "Inventia", NotificationType.Error, new TimeSpan(0, 0, 60));
                            activateInventiaWorkgroup(false);
                        }
                    }
                }
                ((System.Timers.Timer)sender).Start();
            }
            catch(Exception ex)
            {
                _trace.Exception(ex, "timerInventia_Elapsed");
                ((System.Timers.Timer)sender).Start();
            }
        }

        private void activateInventiaWorkgroup(bool active)
        {
            var parameters = Utilities.getInstance();
            try
            {
                _trace.Always("BIZMATICA: addin.cs - timerInventia_Elapsed: connection state=" + _session.ConnectionState.ToString());
                if (_session.ConnectionState != ININ.IceLib.Connection.ConnectionState.Up) return;

                UserWorkgroupActivationList activationList = new UserWorkgroupActivationList(_PeopleManager);
                if (parameters.workgroupsINVENTIA.Count > 0)
                {
                    activationList.StartWatching(parameters.workgroupsINVENTIA.Select(x => x.ConfigurationId.Id).ToArray());
                    foreach (var activation in activationList.GetList())
                    {
                        _trace.Always("BIZMATICA: addin.cs - deactivating " + activation.Workgroup.ToString());
                        UserWorkgroupActivationUpdate update = new UserWorkgroupActivationUpdate(_PeopleManager, activation);
                        update.IsActive = active;
                        update.UserId = _session.UserId;
                        update.UpdateRequest();
                        _trace.Always("BIZMATICA: addin.cs - deactivated " + activation.Workgroup.ToString());
                    }
                    InventiaEnabled = true;
                }
                else
                {
                    _trace.Always("BIZMATICA: addin.cs - activationList vuota");
                }
            }
            catch(Exception ex)
            {
                notificationService.Notify("Si è verificato un errore durante " + (active ? "l'attivazione" : "disattivazione") + " dei workgroup della Service Station","Errore",NotificationType.Error,new TimeSpan(0, 0, 30));
                _trace.Exception(ex, "BIZMATICA: addin.cs - enableInventia");
            }
        }



        private void _session_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            try { 
                if (e.State == ININ.IceLib.Connection.ConnectionState.Down)
                {
                    if (InventiaEnabled && !Inventia_DisconnessioneURL.Equals("")) Win32.InventiaHeartBeat(Inventia_DisconnessioneURL, _session.UserId, "disconnected");
                    notificationService.Notify("La sessione è disconnessa uscire dal programma e ripetere la login", "Sessione", NotificationType.Warning, new TimeSpan(0, 0, 20));
                }
            }
            catch(Exception ex)
            {
                notificationService.Notify("Si è verificato un errore contattare l'amministratore.", "ConnectionStateChanged", NotificationType.Warning, new TimeSpan(0, 0, 20));
                _trace.Exception(ex, "BIZMATICA: addin.cs - _session_ConnectionStateChanged");
            }
        }

        #region Gestione Eventi Interazioni

        protected override void InteractionAdded(IInteraction interaction)
        {
            var parameters = Utilities.getInstance();
            try
            {
                interaction.SetAttribute("Eic_DeallocationTime", WrapupTimeout.ToString());
                if (interaction.GetAttribute(InteractionAttributes.State) == InteractionAttributeValues.State.Connected && interaction.GetAttribute("Videochat").Equals("1"))
                {
                    interaction.SetAttribute("Vidyo_Member", _session.DisplayName);
                }
                interaction.SetAttribute("Agente_UserId", _session.UserId);
                interaction.SetAttribute("Agente_SMSESSION", parameters.AuthSession);
                interaction.SetAttribute("Agente_cb_clicktrack_id", parameters.clicktrackId);

            
                if ((interaction.GetAttribute(InteractionAttributes.InteractionType).Equals(InteractionAttributeValues.InteractionType.Recorder)))
                {
                    if (!(interaction.GetAttribute("Eic_IRSnippetRecordingId").Equals("")))
                    {
                        interaction.SetAttribute("Eic_RecordingTargetingIR", "1");
                        interaction.SetAttribute("Encryption", "1");
                    }
                }
                if (interaction.GetAttribute(InteractionAttributes.Direction).Equals(InteractionAttributeValues.Direction.Incoming) && interaction.GetAttribute(InteractionAttributes.InteractionType).Contains(InteractionAttributeValues.InteractionType.Email) && interaction.GetAttribute(InteractionAttributes.State).Equals(InteractionAttributeValues.State.Connected))
                {
                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "ConnectedUser", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { interaction.InteractionId };
                    request.Write(parametri);

                    _trace.Always("BIZMATICA: addin.cs - custom notification ConnectedUser interazione \n" + parametri[0]);
                    _Notifier.SendServerRequestNoResponse(request);
                }
            }
            catch (Exception ex)
            {
                notificationService.Notify("Si è verificato un errore contattare l'amministratore.", "InteractionAdded", NotificationType.Warning, new TimeSpan(0, 0, 20));
                _trace.Exception(ex, "BIZMATICA: addin.cs -  InteractionAdded");
            }

        }

        protected override void InteractionChanged(IInteraction interaction)
        {
            var parameters = Utilities.getInstance();
            try
            {
                if (interaction.GetAttribute(InteractionAttributes.State) == InteractionAttributeValues.State.ExternalDisconnect || interaction.GetAttribute(InteractionAttributes.State) == InteractionAttributeValues.State.InternalDisconnect)
                {
                    interaction.SetAttribute("Agente_UserId", _session.UserId);
                    interaction.SetAttribute("Agente_SMSESSION", parameters.AuthSession);
                    interaction.SetAttribute("Agente_cb_clicktrack_id", parameters.clicktrackId);
                    if (!interaction.GetAttribute("EsitazioneNecessaria").Equals("1"))
                    {
                        _trace.Always("BIZMATICA: addin.cs -  Esitazione non necessaria per interazione " + interaction.InteractionId);
                        return;
                    }
                    if (interaction.GetAttribute("EmailDraft") != "1")WrapupThread(interaction);
                    else if (interaction.GetAttribute("Eic_EmailShadowSent") == "1") WrapupThread(interaction);
                }
                if (interaction.GetAttribute(InteractionAttributes.Direction).Equals(InteractionAttributeValues.Direction.Incoming) && interaction.GetAttribute(InteractionAttributes.InteractionType).Contains(InteractionAttributeValues.InteractionType.Email) && interaction.GetAttribute(InteractionAttributes.State).Equals(InteractionAttributeValues.State.Connected))
                {
                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "ConnectedUser", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { interaction.InteractionId};
                    request.Write(parametri);

                    _trace.Always("BIZMATICA: addin.cs - custom notification ConnectedUser interazione \n" + parametri[0]);
                    _Notifier.SendServerRequestNoResponse(request);
                }
            }
            catch (Exception ex)
            {
                notificationService.Notify("Si è verificato un errore contattare l'amministratore.", "InteractionChanged", NotificationType.Warning, new TimeSpan(0, 0, 20));
                _trace.Exception(ex, "BIZMATICA: addin.cs - InteractionChanged");
            }
        }
        protected override void InteractionRemoved(IInteraction interaction)
        {
            var parameters = Utilities.getInstance();
            try
            {
                if (interaction.GetAttribute("DisconnectedForSurvey") == "1")
                {
                    interaction.SetAttribute("Agente_UserId", _session.UserId);
                    interaction.SetAttribute("Agente_SMSESSION", parameters.AuthSession);
                    interaction.SetAttribute("Agente_cb_clicktrack_id", parameters.clicktrackId);
                    if (!interaction.GetAttribute("EsitazioneNecessaria").Equals("1"))
                    {
                        _trace.Always("BIZMATICA: addin.cs -  Esitazione non necessaria per interazione " + interaction.InteractionId);
                        return;
                    }

                    if (interaction.GetAttribute("EmailDraft") != "1") WrapupThread(interaction);
                    else if (interaction.GetAttribute("Eic_EmailShadowSent") == "1") WrapupThread(interaction);
                }
            }
            catch (Exception ex)
            {
                notificationService.Notify("Si è verificato un errore contattare l'amministratore.", "InteractionRemoved", NotificationType.Warning, new TimeSpan(0, 0, 20));
                _trace.Exception(ex, "BIZMATICA: addin.cs - InteractionRemoved");
            }
        }
        private void onSelectedInteractionChanged(object sender, EventArgs e)
        {
            try
            { 
                if (sender == null)
                {
                    SelectedInteraction = null;
                    return;
                }

                else
                {
                    SelectedInteraction = ((IInteractionSelector)sender).SelectedInteraction;
                }
            }
            catch(Exception EX)
            {
                notificationService.Notify("Si è verificato un errore contattare l'amministratore.", "SelectedInteractionChanged", NotificationType.Warning, new TimeSpan(0, 0, 20));
                _trace.Exception(EX, "Errore durante la selezione dell'interazione");
                SelectedInteraction = null;
                return;
            }
        }

        internal void CustomNotificationReceivedHandler(object sender, CustomNotificationReceivedEventArgs e)
        {

            try
            {


                var parameters = Utilities.getInstance();

                string sCustomNotification = "BIZMATICA: ";
                string[] strings = null;
                switch (e.Message.Header.CustomMessageType)
                {
                    case CustomMessageType.ApplicationRequest:
                        sCustomNotification += "ApplicationRequest received";
                        break;
                    case CustomMessageType.ApplicationResponse:
                        sCustomNotification += "ApplicationResponse received";
                        break;
                    case CustomMessageType.ServerNotification:
                        sCustomNotification += "ServerNotification received";
                        sCustomNotification += System.Environment.NewLine + "EventId:" + e.Message.EventId;
                        sCustomNotification += System.Environment.NewLine + "ObjectId:" + e.Message.ObjectId;
                        sCustomNotification += System.Environment.NewLine + "ObjectType:" + e.Message.ObjectType;
                        sCustomNotification += System.Environment.NewLine + "Parameters:";
                        strings = e.Message.ReadStrings();
                        foreach (string s in strings)
                            sCustomNotification += System.Environment.NewLine + "          " + s;
                        break;
                }
                _trace.Always(sCustomNotification);

                if (e.Message.ObjectId.CompareTo("Client Button:CBSchedulata") == 0)
                {
                    _trace.Always("BIZMATICA: addin.cs -  apertura popup callback");
                }
                else if (e.Message.ObjectId.CompareTo("Client Button:TrasferimentoMassivoEmail") == 0)
                {
                    TrasferimentoMassivoThread();
                    _trace.Always("BIZMATICA: addin.cs -  apertura popup TrasferimentoMassivo");
                }
                else if (e.Message.ObjectId.CompareTo("CustomRequestResult") == 0)
                {
                    if (strings[0].IndexOf("ERROR") < 0)
                        notificationService.Notify(strings[0], "Success", NotificationType.Info, new TimeSpan(0, 0, 3));
                    else
                        notificationService.Notify(strings[0].Replace("ERROR", ""), "Warning", NotificationType.Warning, new TimeSpan(0, 0, 3));
                }
                else if (e.Message.ObjectId.CompareTo("SearchResult") == 0)
                {
                    //trasferimentoMassivo.populateView(strings);
                }
                else if (e.Message.ObjectId.CompareTo("Context") == 0)
                {
                    if (parameters.tabList.ContainsKey(strings[0])) Win32.SendArgs(((IntPtr)parameters.tabList[strings[0]]), strings[2]);
                }
                else if (e.Message.ObjectId.CompareTo("CustomDisconnect") == 0)
                {

                    var i = interactionManager.CreateInteraction(new ININ.IceLib.Interactions.InteractionId(long.Parse(strings[0])));
                    i.Disconnect();
                }
            }
            catch (Exception exc)
            {
                _trace.Exception(exc, "BIZMATICA: Errore CustomNotificationReceivedHandler");
                notificationService.Notify("Si è verificato un errore durante la ricezione di un evento", "Errore", NotificationType.Error, new TimeSpan(0, 0, 5));
            }
        }


        #endregion

        #region popup Wrapup

        private void WrapupThread(IInteraction interaction)
        {

            _syncContext.Post(Wrapup, interaction);
        }

        private void Wrapup(object _interaction)
        {

            WrapupWindow wrapUp = new WrapupWindow()
            {

                SyncContext = _syncContext,
                Interaction = (IInteraction)_interaction,
                _session = _session,
                _trace= _trace,
                _Notifier = _Notifier,
                WrapupTimeout = WrapupTimeout,
                xDom=xdom

            };
            wrapUp.ShowForm();
        }

        #endregion

        #region popup trasferimentoMassivo

        private void TrasferimentoMassivoThread()
        {

            _syncContext.Post(TrasferimentoMassivo, null);
        }

        private void TrasferimentoMassivo(object param)
        {
            var parameters = Utilities.getInstance();
            TrasferimentoMassivo trasferimentoMassivo = new TrasferimentoMassivo()
            {

                SyncContext = _syncContext,
                workgroups = parameters.workgroupsEmailTrasferimento,
                users = parameters.users,
                _session = _session,
                _trace = _trace,
                _Notifier = _Notifier

            };
            trasferimentoMassivo.ShowForm();
        }

        #endregion



    }
    
 
}
