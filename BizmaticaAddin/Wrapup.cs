﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ININ.InteractionClient.AddIn;
using System.Threading;

using ININ.IceLib.Configuration;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using System.Xml;

namespace BizmaticaAddin
{

    public partial class WrapupWindow : Form
    {
        public WrapupWindow()
        {
            InitializeComponent();
        }


        public SynchronizationContext SyncContext { get; set; }
        public IInteraction Interaction { get; set; }
        public XmlDocument xDom { get; set; }
        public Session _session { get; set; }
        public ITraceContext _trace { get; set; }
        public INotificationService notificationService;
        public CustomNotification _Notifier { get; set; }

        public int WrapupTimeout { get; set; }
        

        public void ShowForm()
        {
            

            try {
                Show();
                _trace.Always("BIZMATICA: Popup Wrapup shown");

                progressBar1.Minimum = 1;
                progressBar1.Maximum = WrapupTimeout-5;
                progressBar1.Step = 1;

                _trace.Always("BIZMATICA: Valorizzazione label finestra -> NDG: " + Interaction.GetAttribute("NDG") + " - Coda: " + Interaction.GetAttribute(InteractionAttributes.WorkgroupName) + " - " + (Interaction.GetAttribute(InteractionAttributes.Direction).Equals("I") ? "Inbound" : "Outbound"));
                this.Text = "NDG: " + Interaction.GetAttribute("NDG") + " - Coda: " + Interaction.GetAttribute(InteractionAttributes.WorkgroupName) + " - " + (Interaction.GetAttribute(InteractionAttributes.Direction).Equals("I") ? "Inbound": "Outbound");

                _trace.Always("BIZMATICA: Valorizzazione combobox");
                foreach (XmlNode item in xDom.SelectNodes("//root/*"))
                {
                    comboBox1.Items.Add(new ItemCmb(item.Attributes["id"].Value, item.Attributes["desc"].Value));
                    _trace.Always("BIZMATICA: valori" + item.Attributes["id"].Value + ">" + item.Attributes["desc"].Value);
                }
                comboBox1.Tag = comboBox2;
                comboBox2.Tag = comboBox3;
                comboBox3.Tag = comboBox3;

                comboBox1.GotFocus += ComboBox_GotFocus;
                comboBox2.GotFocus += ComboBox_GotFocus;
                comboBox3.GotFocus += ComboBox_GotFocus;

                _trace.Always("BIZMATICA: Valorizzazione combobox1 e inzializzazione eventi ");

                comboBox1.SelectedIndexChanged += ComboBox_SelectedIndexChanged;
                comboBox2.SelectedIndexChanged += ComboBox_SelectedIndexChanged;
                if (Interaction.GetAttribute(InteractionAttributes.Direction).Equals("I"))
                {
                    _trace.Always("BIZMATICA: Set attributes per inbound ");
                    Interaction.SetAttribute("contactTypeID", "IDE");
                    Interaction.SetAttribute("reasonID", "RDE");
                }else
                {
                    _trace.Always("BIZMATICA: Set attributes per outbound ");
                    Interaction.SetAttribute("contactTypeID", "ODE");
                    Interaction.SetAttribute("contactOutcomeID", "RDE");
                }
                _trace.Always("BIZMATICA: Wrapup impostati attributi default per esitazione");
            }
            catch (Exception e)
            {
                _trace.Exception(e, "BIZMATICA: Si è verificato un errore durante l'apertura della popup di esitazione");
                MessageBox.Show("Si è verificato un errore durante l'apertura della popup di esitazione:" + System.Environment.NewLine + e.Message);
            }

        }

        private void ComboBox_GotFocus(object sender, EventArgs e)
        {
            _trace.Always("BIZMATICA: WrapupWindow ComboBox_GotFocus ");
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.PerformStep();
            if (progressBar1.Value == progressBar1.Maximum)
            {
                try
                {
                    _trace.Always("BIZMATICA: Timeout maschera esitazione invio notification");
                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "Esitazione", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { Interaction.InteractionId };

                    request.Write(parametri);

                    _trace.Always("BIZMATICA:custom notification Esitazione\n" + parametri[0]);
                    _Notifier.SendServerRequestNoResponse(request);
                    _trace.Always("BIZMATICA:custom notification Inviata\n" + parametri[0]);
                }
                catch (Exception ex)
                {
                    _trace.Exception(ex, "BIZMATICA: Si è verificato un errore durante l'esitazione dopo Timeout");
                    notificationService.Notify("Si è verificato un errore durante l'esitazione", "Esitazione", NotificationType.Error, new TimeSpan(0, 0, 30));
                }
                this.Dispose();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _trace.Always("BIZMATICA: WrapupWindow button1_Click");
            try { 
                if (comboBox1.SelectedItem==null)
                {
                    label1.Visible = true;
                    label2.Visible = true;
                    label3.Visible = true;
                    return;
                }
                if (comboBox2.SelectedItem == null)
                {
                    label2.Visible = true;
                    label3.Visible = true;
                    return;
                }
                if (comboBox3.SelectedItem == null)
                {
                    label3.Visible = true;
                    return;
                }
                _trace.Always("BIZMATICA: WrapupWindow registrazione esito");


                Interaction.SetAttribute("Eic_callnote", textBox1.Text);

                Interaction.SetAttribute("ContactTypeID", ((ItemCmb)comboBox2.SelectedItem).value());

                if (Interaction.GetAttribute(InteractionAttributes.Direction).Equals("O"))
                {
                    Interaction.SetAttribute("contactOutcomeID", ((ItemCmb)comboBox3.SelectedItem).value());
                }
                else
                {
                    Interaction.SetAttribute("reasonID", ((ItemCmb)comboBox3.SelectedItem).value());
                }

                _trace.Always("BIZMATICA: " + Interaction.InteractionId + " Registrato esito su attributi: " + ((ItemCmb)comboBox1.SelectedItem).value() + " - " + ((ItemCmb)comboBox2.SelectedItem).value() + " - " + ((ItemCmb)comboBox3.SelectedItem).value());

                CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "Esitazione", _session.UserId);
                CustomRequest request = new CustomRequest(header);
                string[] parametri = new string[] { Interaction.InteractionId };

                request.Write(parametri);

                _trace.Always("BIZMATICA:custom notification Esitazione\n" + parametri[0]);
                _Notifier.SendServerRequestNoResponse(request);
                _trace.Always("BIZMATICA:custom notification Inviata\n" + parametri[0]);
            }
            catch (Exception ex)
            {
                _trace.Exception(ex, "BIZMATICA: Si è verificato un errore durante l'esitazione");
                notificationService.Notify("Si è verificato un errore durante l'esitazione ", "Esitazione", NotificationType.Error, new TimeSpan(0, 0, 30));
            }

            this.Dispose();
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try {
                _trace.Always("BIZMATICA: ComboBox_SelectedIndexChanged");
                ((ComboBox)(((ComboBox)sender).Tag)).Items.Clear();
                _trace.Always("BIZMATICA: combo figlia svuotata");
                ((ComboBox)((ComboBox)(((ComboBox)sender).Tag)).Tag).Items.Clear();
                _trace.Always("BIZMATICA: combo nipote svuotata e ripopolata con query " + "//item[@id='" + System.Security.SecurityElement.Escape(((ItemCmb)((ComboBox)sender).SelectedItem).Id) + "']/*");

                foreach (XmlNode item in xDom.SelectNodes("//item[@id='" + System.Security.SecurityElement.Escape(((ItemCmb)((ComboBox)sender).SelectedItem).Id) + "']/*"))
                {
                    _trace.Always("BIZMATICA: combo aggiunta item " + item.Attributes["id"].Value + ">" + item.Attributes["desc"].Value);
                    ((ComboBox)(((ComboBox)sender).Tag)).Items.Add(new ItemCmb(item.Attributes["id"].Value, item.Attributes["desc"].Value));
                }
            }
            catch (Exception ex)
            {
                _trace.Exception(ex, "BIZMATICA: Si è verificato un errore durante  il popolamento menu esitazione");
                notificationService.Notify("Si è verificato un errore durante il popolamento menu esitazione", "Esitazione", NotificationType.Error, new TimeSpan(0, 0, 30));
            }

}

        private void WrapupWindow_Load(object sender, EventArgs e)
        {

        }
    }

    public partial class ItemCmb
    {
        public string Id;
        public string Valore;

        public ItemCmb(string _Id, string _Valore)
        {
            Id = _Id;
            Valore = _Valore;
        }
        public override string ToString()
        {
            return Valore;
        }
        public string value()
        {
            return Id;

        }

    }

 
}
