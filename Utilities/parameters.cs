﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ININ.IceLib;
using ININ.IceLib.Configuration;

namespace BizmaticaAddin
{


    using System.Runtime.CompilerServices;

    public static class Extensions
    {
        private static readonly ConditionalWeakTable<object, RefId> _ids = new ConditionalWeakTable<object, RefId>();

        public static Guid GetRefId<T>(this T obj) where T : class
        {
            if (obj == null)
                return default(Guid);

            return _ids.GetOrCreateValue(obj).Id;
        }

        private class RefId
        {
            public Guid Id { get; } = Guid.NewGuid();
        }
    }

    public class Utilities
    {


        private static Utilities singleton;
        private static readonly object l = new object();
        public static Utilities getInstance()
        {

            lock (l)
            {
                if (singleton == null)
                    singleton = new Utilities();
            }
            return singleton;
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private Utilities()
        {
            _AuthSession = "";
            _clicktrackId = "";
            _tabs = new List<IntPtr>();
            _tabList = new Dictionary<string, IntPtr>();
            AuthUrl = "";
            AuthCookieName = "";

            PollingTimeout = 300;
            PollingUrl = "";
            UrlAutenticazione = "";
            MsgSessionExpired = "Sessione scaduta. Chiudere e riaprire";
            MsgCredenzialiErrate = "Credenziali non valide";
            ScheduledCallbackConfigurationInterval = 30;
            SMCookieTimestamp = DateTime.Now;
            SMCookie = "";
            LastContextCustomer = "";
            LastContextCustomerDate = new DateTime();

        
            workgroupsEmail = new List<WorkgroupConfiguration>();
            workgroupsEmailTrasferimento = new List<WorkgroupConfiguration>();
            workgroupsCB = new List<WorkgroupConfiguration>();
            workgroupsINVENTIA = new List<WorkgroupConfiguration>();
            users = new List<UserConfiguration>();
            EsitiDipendenze = new List<Dictionary<string, StructuredParameterConfiguration>>();


    }


    private Dictionary<string, IntPtr> _tabList;
        public Dictionary<string, IntPtr> tabList
        {
            get
            {
                return _tabList;
            }
            set
            {
                lock (l)
                {
                    this._tabList = value;
                }
            }
        }

        private List<IntPtr> _tabs;
        public List<IntPtr> tabs
        {
            get
            {
                return _tabs;
            }
            set
            {
                lock (l)
                {
                    this._tabs = value;
                }
            }
        }


        private string _clicktrackId;
        public string clicktrackId
        {
            get
            {
                return _clicktrackId;
            }
            set
            {

                lock (l)
                {
                    this._clicktrackId = value;
                }

            }
        }

        private string _AuthSession;
        public string AuthSession
        {
            get
            {
                return _AuthSession;
            }
            set
            {

                lock (l)
                {
                    this._AuthSession = value;
                }

            }
        }

        
        private string _AuthUrl;
        public string AuthUrl
        {
            get
            {
                return _AuthUrl;
            }
            set
            {

                lock (l)
                {
                    this._AuthUrl = value;
                }

            }
        }

        private string _AuthCookieName;
        public string AuthCookieName
        {
            get
            {
                return _AuthCookieName;
            }
            set
            {

                lock (l)
                {
                    this._AuthCookieName = value;
                }

            }
        }

        private string _unbluAutenticazioneUrl;
        public string unbluAutenticazioneUrl
        {
            get
            {
                return _unbluAutenticazioneUrl;
            }
            set
            {

                lock (l)
                {
                    this._unbluAutenticazioneUrl = value;
                }

            }
        }


        private string _unbluCredentials;
        public string unbluCredentials
        {
            get
            {
                return _unbluCredentials;
            }
            set
            {

                lock (l)
                {
                    this._unbluCredentials = value;
                }

            }
        }

        private string _unbluCookie;
        public string unbluCookie
        {
            get
            {
                return _unbluCookie;
            }
            set
            {

                lock (l)
                {
                    this._unbluCookie = value;
                }

            }
        }

        private string _unbluCookieValue;
        public string unbluCookieValue
        {
            get
            {
                return _unbluCookieValue;
            }
            set
            {

                lock (l)
                {
                    this._unbluCookieValue = value;
                }

            }
        }

        private string _Domain;
        public string Domain
        {
            get
            {
                return _Domain;
            }
            set
            {

                lock (l)
                {
                    this._Domain = value;
                }

            }
        }

        private string _UserName;
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {

                lock (l)
                {
                    this._UserName = value;
                }

            }
        }

        private string _Password;
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {

                lock (l)
                {
                    this._Password = value;
                }

            }
        }

        private int _PollingTimeout;
        public int PollingTimeout
        {
            get
            {
                return _PollingTimeout;
            }
            set
            {

                lock (l)
                {
                    this._PollingTimeout = value;
                }

            }
        }

        private int _ScheduledCallbackConfigurationInterval;
        public int ScheduledCallbackConfigurationInterval
        {
            get
            {
                return _ScheduledCallbackConfigurationInterval;
            }
            set
            {

                lock (l)
                {
                    this._ScheduledCallbackConfigurationInterval = value;
                }

            }
        }

        private DateTime _SMCookieTimestamp;
        public DateTime SMCookieTimestamp
        {
            get
            {
                return _SMCookieTimestamp;
            }
            set
            {

                lock (l)
                {
                    this._SMCookieTimestamp = value;
                }

            }
        }

        private DateTime _LastContextCustomerDate;
        public DateTime LastContextCustomerDate
        {
            get
            {
                return _LastContextCustomerDate;
            }
            set
            {

                lock (l)
                {
                    this._LastContextCustomerDate = value;
                }

            }
        }

        private string _PollingUrl;
        public string PollingUrl
        {
            get
            {
                return _PollingUrl;
            }
            set
            {

                lock (l)
                {
                    this._PollingUrl = value;
                }

            }
        }

        private string _UrlAutenticazione;
        public string UrlAutenticazione
        {
            get
            {
                return _UrlAutenticazione;
            }
            set
            {

                lock (l)
                {
                    this._UrlAutenticazione = value;
                }

            }
        }


        private string _MsgSessionExpired;
        public string MsgSessionExpired
        {
            get
            {
                return _MsgSessionExpired;
            }
            set
            {

                lock (l)
                {
                    this._MsgSessionExpired = value;
                }

            }
        }

        private string _MsgCredenzialiErrate;
        public string MsgCredenzialiErrate
        {
            get
            {
                return _MsgCredenzialiErrate;
            }
            set
            {

                lock (l)
                {
                    this._MsgCredenzialiErrate = value;
                }

            }
        }

        private string _SMCookie;
        public string SMCookie
        {
            get
            {
                return _SMCookie;
            }
            set
            {

                lock (l)
                {
                    this._SMCookie = value;
                }

            }
        }

        private string _LastContextCustomer;
        public string LastContextCustomer
        {
            get
            {
                return _LastContextCustomer;
            }
            set
            {

                lock (l)
                {
                    this._LastContextCustomer = value;
                }

            }
        }


        private System.Collections.ObjectModel.ReadOnlyCollection<StructuredParameterConfiguration> _structureParameters;
        public System.Collections.ObjectModel.ReadOnlyCollection<StructuredParameterConfiguration> structureParameters
        {
            get
            {
                return _structureParameters;
            }
            set
            {

                lock (l)
                {
                    this._structureParameters = value;
                }

            }
        }

        private List<WorkgroupConfiguration> _workgroupsEmail;
        public List<WorkgroupConfiguration> workgroupsEmail
        {
            get
            {
                return _workgroupsEmail;
            }
            set
            {

                lock (l)
                {
                    this._workgroupsEmail = value;
                }

            }
        }

        private List<WorkgroupConfiguration> _workgroupsEmailTrasferimento;
        public List<WorkgroupConfiguration> workgroupsEmailTrasferimento
        {
            get
            {
                return _workgroupsEmailTrasferimento;
            }
            set
            {

                lock (l)
                {
                    this._workgroupsEmailTrasferimento = value;
                }

            }
        }
        private List<WorkgroupConfiguration> _workgroupsCB;
        public List<WorkgroupConfiguration> workgroupsCB
        {
            get
            {
                return _workgroupsCB;
            }
            set
            {

                lock (l)
                {
                    this._workgroupsCB = value;
                }

            }
        }

        private List<WorkgroupConfiguration> _workgroupsINVENTIA;
        public List<WorkgroupConfiguration> workgroupsINVENTIA
        {
            get
            {
                return _workgroupsINVENTIA;
            }
            set
            {

                lock (l)
                {
                    this._workgroupsINVENTIA = value;
                }

            }
        }

        private List<UserConfiguration> _users;
        public List<UserConfiguration> users
        {
            get
            {
                return _users;
            }
            set
            {

                lock (l)
                {
                    this._users = value;
                }

            }
        }
        private List<Dictionary<string, StructuredParameterConfiguration>> _EsitiDipendenze;
        public List<Dictionary<string, StructuredParameterConfiguration>> EsitiDipendenze
        {
            get
            {
                return _EsitiDipendenze;
            }
            set
            {

                lock (l)
                {
                    this._EsitiDipendenze = value;
                }

            }
        }


    }
}
