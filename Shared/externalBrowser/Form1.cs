﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using SHDocVw;
using System.Diagnostics;
using System.Web;
using System.Reflection;
using System.Threading;
using ININ.IceLib;

namespace BizmaticaAddin
{
    public partial class Form1 : Form
    {

        string portalName = "";
        string portalVersion = "";

        int thisWidth = 0;
        int thisHeight = 0;
        public SHDocVw.WebBrowser axBrowser;
        SHDocVw.InternetExplorer myIE;
        public string resourceForAuth = "";
        public string loginInTab = "";
        public string toolIcon = "";



        public Form1()
        {
            try { 
                
                InitializeComponent();
                Tracing.TraceAlways("Inizializzazione Portale");
                this.webBrowser1.ObjectForScripting = new WebBrowserIntegrationObject();
                this.webBrowser1.ProgressChanged += new WebBrowserProgressChangedEventHandler(webBrowser_ProgressChanged);
                bHnd = this.Handle;

                portalName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                portalVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

                Tracing.TraceAlways("BIZMATICA: " + portalName + " (" + portalVersion + ") Started");
                if (!portalName.Equals("Unblu"))
                {
                    Tracing.TraceAlways("Creazione gestore eventi per popup");
                    axBrowser = (SHDocVw.WebBrowser)(webBrowser1.ActiveXInstance);
                    axBrowser.RegisterAsBrowser = true;
                    axBrowser.NewWindow3 += AxBrowser_NewWindow3;
                    axBrowser.BeforeNavigate2 += AxBrowser_BeforeNavigate2;
                    axBrowser.NavigateError += Wb_NavigateError;
                }
                Tracing.TraceAlways("BIZMATICA: Inizializzazione Completata");
            }
            catch(Exception ex)
            {
                Tracing.TraceException(ex, "BIZMATICA: Errore durante inizializzazione");
            }
        }

        private void Wb_NavigateError(object pDisp, ref object URL, ref object Frame, ref object StatusCode, ref bool Cancel)
        {
            Tracing.TraceError("BIZMATICA: Errore HTTP " + StatusCode.ToString() + " durante apertura URL: " + URL.ToString());
            Win32.SendArgs(addinPtr, "traceError:" + portalName +" - Apertura URL" + ":Errore HTTP + " + StatusCode.ToString());
            Cancel = false;
        }


        private void AxBrowser_BeforeNavigate2(object pDisp, ref object URL, ref object Flags, ref object TargetFrameName, ref object PostData, ref object Headers, ref bool Cancel)
        {
            try {
                Win32.ChangeUserAgent();
            }
            catch(Exception ex)
            {
                Tracing.TraceException(ex, "BIZMATICA: Errore durante ChangeUserAgent");
            }
    
            Headers += string.Format("User-Agent: {0}\r\n", "InteractiveIntelligence/1.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko");
        }

        private void AxBrowser_NewWindow3(ref object ppDisp, ref bool Cancel, uint dwFlags, string bstrUrlContext, string bstrUrl)
        {
            Tracing.TraceAlways("BIZMATICA: Richiesta apertura popup: " + bstrUrl +"\n da pagina " + bstrUrlContext);

            try
            { 
                string cookies = Win32.WebBrowserEx.GetCookiesInternal(new Uri(bstrUrlContext));
                Tracing.TraceAlways("BIZMATICA: Cookies recuperati per apertura popup:\n" + cookies);
                Win32.SendArgs(addinPtr, "traceInfo:" + portalName + ":" + "Renderizzazione documento");

                string outCookie = "";
                foreach (string sCookie in cookies.Split(';'))
                {
                    if (sCookie.Split('=').Length >= 2)
                    {
                        //if (!(sCookie.Split('=')[0]).Replace(System.Environment.NewLine, "").Trim().Equals(SMCookie + "111", StringComparison.OrdinalIgnoreCase) && (!outCookie.Contains(sCookie.Split('=')[0] + "=")))
                        if(outCookie.Contains(sCookie.Split('=')[0] + "=")) {

                            Tracing.TraceError("Cookies doppi");
                        }
                        if (!outCookie.Contains(sCookie.Split('=')[0] + "="))
                                outCookie += (sCookie.Split('=')[0]).Replace(System.Environment.NewLine, "").Trim() + "=" + (sCookie.Split('=')[1]).Replace(System.Environment.NewLine, "") + ";";
                    }
                }

                if (downloadUrl.Equals("") || bstrUrl.StartsWith(downloadUrl) || bstrUrl.ToLower().Equals("about:blank"))
                {
                    if (resourceForAuth.Equals("1"))
                    {
                        //webBrowser2.Navigate(downloadUrl + "?url=" + System.Web.HttpUtility.UrlEncode(bstrUrl), "_blank", Encoding.Default.GetBytes(outCookie), "");
                        Tracing.TraceAlways("Apertura nuovo IE per popup:\n" + downloadUrl + "?url=" + System.Web.HttpUtility.UrlEncode(downloadUrl));
                        myIE = new SHDocVw.InternetExplorer();

                        object Flags = new object();
                        object TargetFrameName = new object();
                        object PostData = new object();
                        object Headers = new object();
                        myIE.Navigate(downloadUrl + "?url=" + System.Web.HttpUtility.UrlEncode(downloadUrl), ref Flags, ref TargetFrameName, Encoding.Default.GetBytes(outCookie), ref Headers);
                        myIE.Visible = false;
                        Thread.Sleep(500);
                        timer3.Enabled = true;
                        myIE.Visible = false;
                        Tracing.TraceAlways("Apertura nuovo IE completata"); 
                    }
                    return; 
                } 
                Cancel = true;

                Tracing.TraceAlways("Download url: " + downloadUrl + "?url=" + System.Web.HttpUtility.UrlEncode(bstrUrl));
                webBrowser2.Navigate(downloadUrl + "?url=" + System.Web.HttpUtility.UrlEncode(bstrUrl), "_blank", Encoding.Default.GetBytes(outCookie), "");
            }
            catch (Exception ex)
            {
                Tracing.TraceException(ex, "Errore durante AxBrowser_NewWindow3");
            }


        }


        //se dovesse servire una progress bar su caricamento
        internal void webBrowser_ProgressChanged(Object sender, WebBrowserProgressChangedEventArgs e)
        {
            timer4.Enabled = true;
            timer4.Start();
            if (e.CurrentProgress < e.MaximumProgress)
            {

                pictureBox1.Visible = true;
            }
            else
            {
                pictureBox1.Visible = false;
            }

            if (!navigated && e.CurrentProgress >= (e.MaximumProgress/5))
            {
                navigated = true;
                Tracing.TraceAlways("navigated=true");
            }
        }


        internal static IntPtr bHnd=new IntPtr(0);
        internal static IntPtr addinPtr = new IntPtr(0);
        
        internal string credentials = "";
        internal string SMSESSION = "";
        bool navigated=false;
        string downloadUrl = "";
        string SMCookie = "";
        string SMCookieValue = "";
        string additionalCookie = "";
        string additionalCookieValue = "";

        string clicktrackId = "";
        string clicktrackIdValue = "";

        
        DateTime SMCookieTimestamp = DateTime.Now;

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case Win32.WM_COPYDATA:
                    try
                    { 
                        Win32.CopyDataStruct st = (Win32.CopyDataStruct)Marshal.PtrToStructure(m.LParam, typeof(Win32.CopyDataStruct));
                        string strData = Marshal.PtrToStringUni(st.lpData);

                        AsyncManageArgsFromAnotherProcess("TraceAlways:" + strData);

                        if (strData.StartsWith("resizeTo:"))
                        {
                            thisWidth = Int32.Parse(strData.Replace("resizeTo:", "").Split(new char[] { ',' })[0]);
                            thisHeight = Int32.Parse(strData.Replace("resizeTo:", "").Split(new char[] { ',' })[1]);
                            AsyncManageArgsFromAnotherProcess(strData);
                        }
                        else if (strData.StartsWith("injectedJavascript:"))
                        {
                            strData = strData.Replace("injectedJavascript:", "");
                            MyInvokeScript(webBrowser1.Document.Window, "eval", new Object[] { strData });
                        }
                        else if (strData.StartsWith("credentials:"))
                        {
                            SMCookie = strData.Split(':')[1];
                            SMCookieValue = strData.Substring(("credentials:" + SMCookie + ":").Length);
                        }
                        else if (strData.StartsWith("additionalCookie:"))
                        {
                            additionalCookie = strData.Split(':')[1];
                            additionalCookieValue = strData.Substring(("additionalCookie:" + additionalCookie + ":").Length);
                        }
                        else if (strData.StartsWith("clicktrackId:"))
                        {
                            clicktrackId = strData.Split(':')[1];
                            clicktrackIdValue = strData.Substring(("clicktrackId:" + clicktrackId + ":").Length);
                        }
                        else if (strData.StartsWith("resourceForAuth:"))
                        {
                            resourceForAuth = strData.Split(':')[1];
                        }
                        else if (strData.StartsWith("loginInTab:"))
                        {
                            loginInTab = strData.Split(':')[1];
                        }
                        else if (strData.StartsWith("toolIcon:"))
                        {
                            AsyncManageArgsFromAnotherProcess(strData);
                        }
                        else if (strData.StartsWith("downloadUrl:"))
                        {
                            downloadUrl = strData.Substring(("downloadUrl:").Length);
                        }
                        else {
                            AsyncManageArgsFromAnotherProcess(strData);
                        }
                    }
                    catch (Exception exc)
                    {
                        Tracing.TraceException(exc, "Errore durante WndProc");
                    }
                    break;
                default:
                    // let the base class deal with it
                    base.WndProc(ref m);
                    break;

            }
            
        }

        async void AsyncManageArgsFromAnotherProcess(string param)
        {
            await Task.Run(() => ManageArgsFromAnotherProcess(param));
        }
        delegate void delegateManageArgsFromAnotherProcess(string parametri);
        private void ManageArgsFromAnotherProcess(string parametri)
        {
            if (this.InvokeRequired)
            {
                delegateManageArgsFromAnotherProcess d = new delegateManageArgsFromAnotherProcess(ManageArgsFromAnotherProcess);
                this.Invoke(d, new object[] { parametri });
            }
            else
            {   if (parametri.StartsWith("TraceAlways:"))
                {
                    Tracing.TraceAlways("WM_COPYDATA >> " + parametri.Substring(("TraceAlways:").Length));
                }
                else if (parametri.StartsWith("resizeTo:"))
                {
                    this.Width = thisWidth;
                    this.Height = thisHeight;
                }
                else if (parametri.StartsWith("toolIcon:"))
                {
                    toolIcon = parametri.Split(':')[1];
                    if (toolIcon.Equals("1"))
                        pictureBox2.Visible = true;
                    else
                        pictureBox2.Visible = false;
                }
                else
                {

                    Tracing.TraceAlways("Navigating to: " + parametri);

                    try
                    {
                        if (!String.IsNullOrEmpty(SMCookie))
                        {
                            if (!String.IsNullOrEmpty(SMCookieValue))
                            {
                                Tracing.TraceAlways("Setting cookie: " + SMCookie + System.Environment.NewLine + SMCookieValue);
                                Win32.InternetSetCookie(parametri, SMCookie, SMCookieValue);
                            }
                        }
                        if (!String.IsNullOrEmpty(additionalCookie))
                        {
                            if (!String.IsNullOrEmpty(additionalCookieValue))
                            {
                                Tracing.TraceAlways("Setting cookie: " + additionalCookie + System.Environment.NewLine + additionalCookieValue);
                                Win32.InternetSetCookie(parametri, additionalCookie, additionalCookieValue);
                            }
                        }
                        if (!String.IsNullOrEmpty(clicktrackId))
                        {
                            if (!String.IsNullOrEmpty(clicktrackIdValue))
                            {
                                Tracing.TraceAlways("Setting cookie: " + clicktrackId + System.Environment.NewLine + clicktrackIdValue);
                                Win32.InternetSetCookie(parametri, clicktrackId, clicktrackIdValue);
                            }
                        }


                        Win32.ChangeUserAgent();
                        Tracing.TraceAlways("Setting user agent");

                        webBrowser1.Navigate(parametri);

                        Tracing.TraceAlways("Enabling timers");
                        timer2.Enabled = true;
                        timer1.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        Tracing.TraceException(ex, "Errore durante navigazione");
                        Win32.SendArgs(addinPtr, "traceError:" + portalName + ":" + "Non è stato possibile caricare o autenticare l'utente sul portale " + portalName);
                    }
                }

            }

        }
        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            navigated = true;
            string tempVal = Win32.WebBrowserEx.GetCookieInternal(e.Url, SMCookie);
            if (!String.IsNullOrEmpty(tempVal))
            {
                if (!SMCookieValue.Equals(tempVal) )
                {
                    SMCookieValue = tempVal;
                    Win32.SendArgs(addinPtr, "refreshCookie:" + SMCookie+":"+ SMCookieTimestamp.Ticks.ToString() + ":" + tempVal);
                    Tracing.TraceAlways("refreshCookie:" + SMCookie + ":" + SMCookieTimestamp.Ticks.ToString());
                }
                else
                {
                    Win32.SendArgs(addinPtr, "confirmCookie:" + SMCookie + ":" + SMCookieTimestamp.Ticks.ToString());
                }
            }
            
            if (e.Url.ToString().Contains("visualHtml"))
            {
                Win32.SendArgs(addinPtr, "unblu:" + e.Url.ToString().Substring(e.Url.ToString().IndexOf("visualHtml",0)+11));
            }
            
        }

        internal object MyInvokeScript(HtmlWindow frm, string name, params object[] args)
        {
            return frm.Document.InvokeScript(name, args);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            addinPtr = Win32.GetRealParent(this.Handle);
            if (addinPtr.ToInt32() <= 0) {
                Tracing.TraceAlways("timer1_Tick:Dispose chiudo portale per mancanza parent process");
                this.Dispose();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (!navigated)
            {
                webBrowser1.Stop();
                webBrowser1.DocumentText = ("timer2_Tick:Timeout durante la connessione al portale " + portalName);
                timer2.Enabled = false;
            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            Tracing.TraceAlways("timer3_Tick");
            timer3.Enabled = false;
            if (myIE != null)
            {
                try {
                    myIE.Quit();
                    Tracing.TraceAlways("timer3_Tick:myIE.Quit()");
                }
                catch (Exception ex)
                {
                    Tracing.TraceException(ex,"timer3_Tick:myIE.Quit()");
                }
            }
            
        }

        private void aggiornaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tracing.TraceAlways("aggiornaToolStripMenuItem_Click");
            webBrowser1.Refresh();  
        }

        private void indietroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tracing.TraceAlways("indietroToolStripMenuItem_Click");
            webBrowser1.GoBack();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tracing.TraceAlways("loginToolStripMenuItem_Click");
            Win32.SendArgs(addinPtr, "loginInTab:1");
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            Tracing.TraceAlways("contextMenuStrip1_Opening:" + loginInTab.ToString());
            if (!loginInTab.Equals("1"))
            {
                toolStripSeparator1.Visible = false;
                loginToolStripMenuItem.Visible = false;
            }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            Tracing.TraceAlways("timer4_Tick");
            timer4.Enabled = false;
            pictureBox1.Visible = false;
        }
    }


    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IApp
    {
        void click2call(string phone, string idOperazionale);

        void click2mail(string email, string idOperazionale);

        void showInteraction(string interactionId);

        void setCurrentCustomer(string idOperazionale);

        void setAppointment(string idOperazionale, string phone);

        void setUnbluEvent(string session, string eventType, string data);
    }

    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IWebBrowserIntegration
    {
        IApp CIC { get; }
    }

    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    [ComDefaultInterface(typeof(IWebBrowserIntegration))]
    public class WebBrowserIntegrationObject : IWebBrowserIntegration
    {
        readonly CIC _app = new CIC();

        public IApp CIC
        {
            get { return _app; }
        }
    }

    [ComVisible(true)]
    public class CIC : IApp
    {
        public void click2call(string phone, string idOperazionale="-1")
        {
            IntPtr parentHnd = Win32.GetRealParent(Form1.bHnd);
            Win32.SendArgs(parentHnd, "click2call:" + phone + ":" + idOperazionale);
        }
        public void showInteraction(string interactionId)
        {
            IntPtr parentHnd = Win32.GetRealParent(Form1.bHnd);
            Win32.SendArgs(parentHnd, "showInteraction:" + interactionId);
        }
        public void setCurrentCustomer(string idOperazionale)
        {
            IntPtr parentHnd = Win32.GetRealParent(Form1.bHnd);
            Win32.SendArgs(parentHnd, "setCurrentCustomer:" + idOperazionale);
        }
        public void setAppointment(string idOperazionale, string phone)
        {
            IntPtr parentHnd = Win32.GetRealParent(Form1.bHnd);
            Win32.SendArgs(parentHnd, "setAppointment:" + idOperazionale+":" + phone);
        }
        public void click2mail(string email, string idOperazionale)
        {
            IntPtr parentHnd = Win32.GetRealParent(Form1.bHnd);
            Win32.SendArgs(parentHnd, "click2mail:" + idOperazionale + ":" + email);
        }

        public void setUnbluEvent(string session, string eventType, string data)
        {
            IntPtr parentHnd = Win32.GetRealParent(Form1.bHnd);
            Win32.SendArgs(parentHnd, "setUnbluEvent:" + session + ":" + eventType + ":" + data);
        }

    }



}

