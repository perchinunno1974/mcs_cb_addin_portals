using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Net;
using System.Linq;
using System.ComponentModel;
using ININ.IceLib;
using System.Threading.Tasks;

namespace BizmaticaAddin
{

    public class Win32
    {
        public const int WM_CLOSE = 16;
        public const int BN_CLICKED = 245;
        public const int WM_COPYDATA = 0x004A;







        public struct CopyDataStruct : IDisposable
        {
            public IntPtr dwData;
            public int cbData;
            public IntPtr lpData;

            public void Dispose()
            {
                if (this.lpData != IntPtr.Zero)
                {
                    Marshal.FreeCoTaskMem(this.lpData);
                    this.lpData = IntPtr.Zero;
                }
            }
        }

        [DllImport("user32.dll")]
        private static extern int GetDlgCtrlID(IntPtr hwndCtl);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);


        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool InternetSetCookie(string UrlName, string CookieName, string CookieData);


        [System.Runtime.InteropServices.DllImport("urlmon.dll", CharSet = System.Runtime.InteropServices.CharSet.Ansi)]
        public static extern int UrlMkSetSessionOption(
            int dwOption, string pBuffer, int dwBufferLength, int dwReserved);

        public const int URLMON_OPTION_USERAGENT = 0x10000001;
        public static void ChangeUserAgent()
        {
            List<string> userAgent = new List<string>();
            string ua = "InteractiveIntelligence/1.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko";
            UrlMkSetSessionOption(URLMON_OPTION_USERAGENT, ua, ua.Length, 0);
        }

        public static void CloseWindow(IntPtr hwnd)
        {
            SendMessage((int)hwnd, WM_CLOSE, 0, IntPtr.Zero);
        }


        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool MoveWindow(IntPtr handle, int x, int y, int width, int height, bool redraw);

        public static bool SendArgs(IntPtr targetHWnd, string args)
        {
            CopyDataStruct cds = new CopyDataStruct();
            try
            {
                cds.cbData = (args.Length + 1) * 2;
                cds.lpData = Win32.LocalAlloc(0x40, cds.cbData);
                Marshal.Copy(args.ToCharArray(), 0, cds.lpData, args.Length);
                cds.dwData = (IntPtr)1;
                SendMessage(targetHWnd, WM_COPYDATA, IntPtr.Zero, ref cds);
            }
            finally
            {
                cds.Dispose();
            }

            return true;
        }


        /// <summary>
        /// Contains message information from a thread's message queue.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct Message
        {
            public IntPtr hWnd;
            public uint msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }

        /// <summary>
        /// The WM_COMMAND message is sent when the user selects a command 
        /// item from a menu, when a control sends a notification message to 
        /// its parent window, or when an accelerator keystroke is translated.
        /// </summary>
        public const int WM_COMMAND = 0x111;

        /// <summary>
        /// The FindWindow function retrieves a handle to the top-level 
        /// window whose class name and window name match the specified strings.
        /// This function does not search child windows. This function does not perform a case-sensitive search.
        /// </summary>
        /// <param name="strClassName">the class name for the window to search for</param>
        /// <param name="strWindowName">the name of the window to search for</param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern int FindWindow(string strClassName, string strWindowName);

        /// <summary>
        /// The FindWindowEx function retrieves a handle to a window whose class name
        /// and window name match the specified strings.
        /// The function searches child windows, beginning with the one following the specified child window.
        /// This function does not perform a case-sensitive search.
        /// </summary>
        /// <param name="hwndParent">a handle to the parent window </param>
        /// <param name="hwndChildAfter">a handle to the child window to start search after</param>
        /// <param name="strClassName">the class name for the window to search for</param>
        /// <param name="strWindowName">the name of the window to search for</param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern int FindWindowEx(int hwndParent, int hwndChildAfter, string strClassName, string strWindowName);

        /// <summary>
        /// The FindWindowEx API
        /// </summary>
        /// <param name="parentHandle">a handle to the parent window </param>
        /// <param name="childAfter">a handle to the child window to start search after</param>
        /// <param name="className">the class name for the window to search for</param>
        /// <param name="windowTitle">the name of the window to search for</param>
        /// <returns></returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);


        /// <summary>
        /// The SendMessage function sends the specified message to a
        /// window or windows. It calls the window procedure for the specified
        /// window and does not return until the window procedure
        /// has processed the message.
        /// </summary>
        /// <param name="hWnd">handle to destination window</param>
        /// <param name="Msg">message</param>
        /// <param name="wParam">first message parameter</param>
        /// <param name="lParam">second message parameter</param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern Int32 SendMessage(int hWnd, int Msg, int wParam, [MarshalAs(UnmanagedType.LPStr)] string lParam);

        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="hWnd">handle to destination window</param>
        /// <param name="Msg">message</param>
        /// <param name="wParam">first message parameter</param>
        /// <param name="lParam">second message parameter</param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern Int32 SendMessage(int hWnd, int Msg, int wParam, int lParam);

        /// <summary>
        /// The SendMessage API
        /// </summary>
        /// <param name="hWnd">handle to the required window</param>
        /// <param name="msg">the system/Custom message to send</param>
        /// <param name="wParam">first message parameter</param>
        /// <param name="lParam">second message parameter</param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(int hWnd, int msg, int wParam, IntPtr lParam);

        /// <summary>
        /// The SendMessage API
        /// </summary>
        /// <param name="hWnd">handle to the required window</param>
        /// <param name="Msg">the system/Custom message to send</param>
        /// <param name="wParam">first message parameter</param>
        /// <param name="lParam">second message parameter</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, ref CopyDataStruct lParam);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr LocalAlloc(int flag, int size);

        /// <summary>
        /// The PeekMessage function dispatches incoming sent messages, 
        /// checks the thread message queue for a posted message, 
        /// and retrieves the message (if any exist).
        /// </summary>
        [System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool PeekMessage(out Message msg, IntPtr hWnd, uint messageFilterMin, uint messageFilterMax, uint flags);

        [DllImport("user32.dll", ExactSpelling = true)]
        public static extern IntPtr GetAncestor(IntPtr hwnd, uint gaFlags);

        [DllImport("user32.dll", EntryPoint = "GetDesktopWindow")]
        public static extern IntPtr GetDesktopWindow();
        public static IntPtr GetRealParent(IntPtr hWnd)
        {
            IntPtr hParent;

            hParent = GetAncestor(hWnd, 1);
            if (hParent.ToInt64() == 0 || hParent == GetDesktopWindow())
            {
                return IntPtr.Zero;
                /* hParent = GetParent(hWnd);
                 if (hParent.ToInt64() == 0 || hParent == GetDesktopWindow())
                 {
                     hParent = hWnd;
                 }
                 */

            }

            return hParent;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetParent(IntPtr hWnd);


        [DllImport("user32.dll")]
        public static extern IntPtr PostMessage(IntPtr hWnd, Message msg, int wParam, int lParam);

        internal static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public class AuthCookies
        {
            public string SMSESSION = "";
            public string cbclicktrackId = "";
        }

        public static AuthCookies ContactServlet(string authUrl, string userName, string password, string domain, string machineName, string cookiename)
        {
            Tracing.TraceAlways("BIZMATICA: ContactServlet con credenziali " + domain + "\\" + userName);
            //string resultCookies = "";
            AuthCookies resultCookies = new AuthCookies();
            try
            {

                bool authOk = false;
                bool reqOk = false;

                string request = authUrl;
                int hops = 0;
                int maxHops = 5;
                HttpWebResponse webResp;
                System.Net.ServicePointManager.Expect100Continue = true;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

                CookieContainer cookies = new CookieContainer();
                NetworkCredential credential = null;



                if (!userName.Equals(""))
                    credential = new System.Net.NetworkCredential(userName, password, domain);
                else
                    credential = System.Net.CredentialCache.DefaultCredentials.GetCredential(new Uri(request), "Basic");
                do
                {
                    hops++;

                    Tracing.TraceAlways("BIZMATICA: Chiamata URL Autenticazione (step +" + hops.ToString() + ") " + request);

                    System.Net.HttpWebRequest webReq = System.Net.WebRequest.Create(request) as System.Net.HttpWebRequest;
                    webReq.Credentials = credential;
                    webReq.PreAuthenticate = true;
                    webReq.CookieContainer = cookies;
                    webReq.KeepAlive = false;
                    webReq.Timeout = 20000;
                    //WebProxy myProxy = new WebProxy("prdwswsg1c.cbmain.cbdom.it", 8080);
                    //// Create a NetworkCredential object and is assign to the Credentials property of the Proxy object.
                    //myProxy.Credentials = new NetworkCredential(userName, password, domain);
                    //webReq.Proxy = myProxy;

                    webReq.ServicePoint.ConnectionLeaseTimeout = 20000;
                    webReq.ServicePoint.MaxIdleTime = 20000;

                    webReq.Method = "GET";
                    webReq.AllowAutoRedirect = false;
                    try
                    {
                        // Send the data.
                        webResp = webReq.GetResponse() as HttpWebResponse;
                        string sWebResponse = new System.IO.StreamReader(webResp.GetResponseStream()).ReadToEnd();
                        webResp.Close();

                        Tracing.TraceAlways("BIZMATICA: risposta ottenuta:" + System.Environment.NewLine + sWebResponse);

                        foreach (System.Net.Cookie cookie in webResp.Cookies)
                        {
                            Tracing.TraceAlways("BIZMATICA: Ricevuto cookie " + cookie.Name + System.Environment.NewLine + cookie.Value);
                            if (cookie.Name.Equals("cb-clicktrack-id"))
                            {
                                resultCookies.cbclicktrackId = cookie.Value;
                            }
                        }

                        foreach (System.Net.Cookie cookie in webResp.Cookies)
                        {
                            if (cookie.Name.Equals(cookiename))//"SMSESSION")
                            {
                                resultCookies.SMSESSION = cookie.Value;
                                if (!String.IsNullOrEmpty(resultCookies.SMSESSION) && !String.IsNullOrEmpty(resultCookies.cbclicktrackId))
                                {
                                    Tracing.TraceAlways("BIZMATICA: cookie trovati SMSESSION e CBCLICKTRACKID");
                                    return resultCookies;
                                }
                            }
                        }

                        foreach (Cookie co in webResp.Cookies)
                        {
                            cookies.Add(co);
                            Tracing.TraceAlways("BIZMATICA: aggiunto alla prossima richiesta http il cookie " + co.Name);
                        }


                        if (webResp.StatusCode == HttpStatusCode.Found)
                        {
                            //Tracing.TraceAlways("BIZMATICA: stato 302");
                            request = webResp.Headers["Location"] as String;
                            Tracing.TraceAlways("BIZMATICA: nuovo indirizzo " + request);
                        }
                        else
                        {
                            if (!authOk)
                            {
                                request = authUrl;


                                foreach (System.Net.Cookie cookie in webResp.Cookies)
                                {
                                    if (cookie.Name.Equals("cb-clicktrack-id"))
                                    {
                                        resultCookies.cbclicktrackId = cookie.Value;
                                    }
                                }

                                foreach (System.Net.Cookie cookie in webResp.Cookies)
                                {
                                    if (cookie.Name.Equals(cookiename))//"SMSESSION")
                                    {
                                        resultCookies.SMSESSION = cookie.Value;
                                        Tracing.TraceAlways("BIZMATICA: cookie trovati \nSMSESSION: " + resultCookies.SMSESSION + "\nCBCLICKTRACKID:" + resultCookies.cbclicktrackId);
                                        return resultCookies;
                                    }
                                }

                                authOk = true;
                            }
                            else
                            {

                                reqOk = true;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Tracing.TraceException(ex, "BIZMATICA: Errore autenticazione: " + ex.Message);
                        maxHops = 0;
                    }
                    finally
                    {
                        webReq.Abort();
                        webReq = null;
                    }
                } while ((hops < maxHops) && (!reqOk));
            }
            catch (Exception) { }

            return resultCookies;
        }


        public static bool InventiaHeartBeat(string authUrl, string userid, string status)
        {
            bool result = false;
            try
            {
                Tracing.TraceAlways("BIZMATICA: InventiaHeartBeat " + authUrl + " " + userid + " " + status);
                var request = (HttpWebRequest)WebRequest.Create(authUrl);
                var postData = "{\"operatorCode\":\"" + userid + "\"}";
                var data = Encoding.ASCII.GetBytes(postData);
                request.Timeout = 2000;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;
                
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse()) { 
                    var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                    Tracing.TraceAlways("InventiaHeartBeat:" + System.Environment.NewLine + responseString);
                }
                result = true;
            }
            catch (Exception ex)
            {
                Tracing.TraceException(ex, "InventiaHeartBeat " + status + " " + authUrl);
            }
            
            return result;
        }



        public static string refreshSession(string reqUrl, string cookieName, string cookieValue)
        {
            string resultCookies = "";
            string request = reqUrl;


            //System.Net.ServicePointManager.Expect100Continue = true;
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls;
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

            CookieContainer cookies = new CookieContainer();
            Cookie scookie = new Cookie(cookieName, cookieValue);
            cookies.Add(new System.Uri(request), scookie);

            //var scookie2 = new Cookie("cb-clicktrack-id", cookieValue2);
            //cookies.Add(new System.Uri(request), scookie2);


            System.Net.HttpWebRequest webReq = System.Net.WebRequest.Create(request) as System.Net.HttpWebRequest;
            webReq.PreAuthenticate = true;
            webReq.CookieContainer = cookies;
            webReq.KeepAlive = false;
            webReq.Timeout = 10000;
            //WebProxy myProxy = new WebProxy("127.0.0.1", 8888);
            //// Create a NetworkCredential object and is assign to the Credentials property of the Proxy object.
            //myProxy.Credentials = new NetworkCredentialserName, password, domain);
            //webReq.Proxy = myProxy;

            webReq.ServicePoint.ConnectionLeaseTimeout = 5000;
            webReq.ServicePoint.MaxIdleTime = 5000;

            webReq.Method = "GET";
            webReq.AllowAutoRedirect = false;

            /*
            webReq.Headers.Add("cb-application-token", "5fedb4f54e212ab4da0aa6adbdf0f3c42139c5440d042e0d311876dc");
            webReq.Headers.Add("ambito", "USR");
            webReq.Headers.Add("Origin", "https://sysapi.chebanca.it/");
            //webReq.Headers.Add("domain", ".cbsysdom.it");
            */


            try
            {
                // Send the data.

                HttpWebResponse webResp = webReq.GetResponse() as HttpWebResponse;
                string sWebResponse = new System.IO.StreamReader(webResp.GetResponseStream()).ReadToEnd();
                webResp.Close();

                foreach (System.Net.Cookie cookie in webResp.Cookies)
                {
                    if (cookie.Name.Equals(cookieName))//"SMSESSION")
                    {
                        resultCookies = cookie.Value;
                        return resultCookies;
                    }
                }



            }
            catch (Exception ex)
            {
                return "#Error refreshingSession: " + ex.Message;
            }
            finally
            {
                webReq.Abort();
                webReq = null;
            }


            return resultCookies;
        }

        public static string unbluSession(string unbluAutenticazioneUrl, string unbluCredentials, string unbluCookie)
        {
            try
            {
                CookieContainer cookies = new CookieContainer();
                System.Net.HttpWebRequest request = System.Net.WebRequest.Create(unbluAutenticazioneUrl) as System.Net.HttpWebRequest;

                request.Method = "POST";
                request.CookieContainer = cookies;
                Tracing.TraceAlways("BIZMATICA: url autenticazione unblu: " + unbluAutenticazioneUrl);

                // Create POST data and convert it to a byte array.
                Guid g = Guid.NewGuid();

                string postData = unbluCredentials.Replace("XXXXXXXX", g.ToString().Substring(25));
                Tracing.TraceAlways("BIZMATICA: messaggio autenticazione unblu: " + postData);

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.
                //request.ContentType = "application/x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;
                request.Timeout = 10000;
                // Get the request stream.
                System.IO.Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();

                // Get the response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                // Display the status.
                Tracing.TraceAlways("BIZMATICA: stato chiamata autenticazione unblu: " + ((HttpWebResponse)response).StatusDescription);



                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                Tracing.TraceAlways("BIZMATICA: risposta chiamata autenticazione unblu: " + responseFromServer);
                // Clean up the streams.
                reader.Close();
                dataStream.Close();
                response.Close();
                Tracing.TraceAlways("BIZMATICA: Cookie cercato: " + unbluCookie);
                foreach (System.Net.Cookie cookie in response.Cookies)
                {
                    Tracing.TraceAlways("BIZMATICA: Cookie trovato: " + cookie.Name);
                    if (cookie.Name.Equals(unbluCookie))
                    {
                        Tracing.TraceAlways("BIZMATICA: Valore cookie trovato: " + cookie.Value);
                        return cookie.Value;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.TraceException(ex, "BIZMATICA: Errore autenticazione unblu: " + ex.Message);
            }

            return "";
        }



        /// <summary>
        /// Constructor
        /// </summary>
        public Win32()
        {
        }

        /// <summary>
        /// Deconstructor
        /// </summary>
        ~Win32()
        {
        }


        /// <summary>
        /// Subclassed WebBrowser that suppresses error pop-ups.
        /// 
        /// Notes.
        /// ScriptErrorsSuppressed property is not used because this actually suppresses *all* pop-ups.
        /// 
        /// More info at:
        /// http://stackoverflow.com/questions/2476360/disable-javascript-error-in-webbrowser-control
        /// </summary>
        public class WebBrowserEx : System.Windows.Forms.WebBrowser
        {
            #region Constructor

            /// <summary>
            /// Default constructor.
            /// Initialise browser control and attach customer event handlers.
            /// </summary>
            public WebBrowserEx()
            {
                this.ScriptErrorsSuppressed = false;
            }


            /////////////////////////////////////////


            public static string GetCookieInternal(Uri uri, string cookie)
            {
                uint pchCookieData = 0;
                string url = UriToString(uri);
                uint flag = ((uint)NativeMethods.InternetFlags.INTERNET_COOKIE_HTTPONLY);// | (uint)NativeMethods.InternetFlags.INTERNET_COOKIE_THIRD_PARTY | (uint)NativeMethods.InternetFlags.INTERNET_FLAG_RESTRICTED_ZONE);

                //Gets the size of the string builder   
                if (NativeMethods.InternetGetCookieEx(url, null, null, ref pchCookieData, flag, IntPtr.Zero))
                {
                    pchCookieData++;
                    StringBuilder cookieData = new StringBuilder((int)pchCookieData);

                    //Read the cookie   
                    if (NativeMethods.InternetGetCookieEx(url, null, cookieData, ref pchCookieData, flag, IntPtr.Zero))
                    {
                        DemandWebPermission(uri);

                        foreach (string co in cookieData.ToString().Split(';'))
                        {
                            if (cookieData.ToString().StartsWith(cookie + "=")) return co.Substring(cookie.Length + 1);
                        }
                    }
                }
                return null;
            }


            public static string GetCookiesInternal(Uri uri)
            {
                uint pchCookieData = 0;
                string url = UriToString(uri);
                uint flag = ((uint)NativeMethods.InternetFlags.INTERNET_COOKIE_HTTPONLY);// | (uint)NativeMethods.InternetFlags.INTERNET_COOKIE_THIRD_PARTY | (uint)NativeMethods.InternetFlags.INTERNET_FLAG_RESTRICTED_ZONE);

                //Gets the size of the string builder   
                if (NativeMethods.InternetGetCookieEx(url, null, null, ref pchCookieData, flag, IntPtr.Zero))
                {
                    pchCookieData++;
                    StringBuilder cookieData = new StringBuilder((int)pchCookieData);

                    //Read the cookie   
                    if (NativeMethods.InternetGetCookieEx(url, null, cookieData, ref pchCookieData, flag, IntPtr.Zero))
                    {
                        DemandWebPermission(uri);
                        return cookieData.ToString();
                    }
                }
                var errori = Marshal.GetLastWin32Error();
                return "*";
            }


            private static void DemandWebPermission(Uri uri)
            {
                string uriString = UriToString(uri);

                if (uri.IsFile)
                {
                    string localPath = uri.LocalPath;
                    new System.Security.Permissions.FileIOPermission(System.Security.Permissions.FileIOPermissionAccess.Read, localPath).Demand();
                }
                else
                {
                    new WebPermission(NetworkAccess.Connect, uriString).Demand();
                }
            }
            private static string UriToString(Uri uri)
            {
                if (uri == null)
                {
                    throw new ArgumentNullException("uri");
                }

                UriComponents components = (uri.IsAbsoluteUri ? UriComponents.AbsoluteUri : UriComponents.SerializationInfoString);
                return new StringBuilder(uri.GetComponents(components, UriFormat.SafeUnescaped), 2083).ToString();
            }








            //////////////////////////////////////////




            #endregion

            #region Overrides

            /// <summary>
            /// Override to allow custom script error handling.
            /// </summary>
            /// <returns></returns>
            protected override WebBrowserSiteBase CreateWebBrowserSiteBase()
            {
                return new WebBrowserSiteEx(this);
            }

            #endregion

            #region Inner Class [WebBrowserSiteEx]

            /// <summary>
            /// Sub-class to allow custom script error handling.
            /// </summary>
            protected class WebBrowserSiteEx : WebBrowserSite, NativeMethods.IOleCommandTarget
            {
                /// <summary>
                /// Default constructor.
                /// </summary>
                public WebBrowserSiteEx(WebBrowserEx webBrowser) : base(webBrowser)
                {
                }

                /// <summary>Queries the object for the status of one or more commands generated by user interface events.</summary>
                /// <param name="pguidCmdGroup">The GUID of the command group.</param>
                /// <param name="cCmds">The number of commands in <paramref name="prgCmds" />.</param>
                /// <param name="prgCmds">An array of OLECMD structures that indicate the commands for which the caller needs status information. This method fills the <paramref name="cmdf" /> member of each structure with values taken from the OLECMDF enumeration.</param>
                /// <param name="pCmdText">An OLECMDTEXT structure in which to return name and/or status information of a single command. This parameter can be null to indicate that the caller does not need this information.</param>
                /// <returns>This method returns S_OK on success. Other possible return values include the following.
                /// E_FAIL The operation failed.
                /// E_UNEXPECTED An unexpected error has occurred.
                /// E_POINTER The <paramref name="prgCmds" /> argument is null.
                /// OLECMDERR_E_UNKNOWNGROUP The <paramref name="pguidCmdGroup" /> parameter is not null but does not specify a recognized command group.</returns>
                public int QueryStatus(ref Guid pguidCmdGroup, int cCmds, NativeMethods.OLECMD prgCmds, IntPtr pCmdText)
                {
                    if ((int)NativeMethods.OLECMDID.OLECMDID_SHOWSCRIPTERROR == prgCmds.cmdID)
                    {   // Do nothing (suppress script errors)

                        return NativeMethods.S_OK;
                    }

                    // Indicate that command is unknown. The command will then be handled by another IOleCommandTarget.
                    return NativeMethods.OLECMDERR_E_UNKNOWNGROUP;
                }

                /// <summary>Executes the specified command.</summary>
                /// <param name="pguidCmdGroup">The GUID of the command group.</param>
                /// <param name="nCmdID">The command ID.</param>
                /// <param name="nCmdexecopt">Specifies how the object should execute the command. Possible values are taken from the <see cref="T:Microsoft.VisualStudio.OLE.Interop.OLECMDEXECOPT" /> and <see cref="T:Microsoft.VisualStudio.OLE.Interop.OLECMDID_WINDOWSTATE_FLAG" /> enumerations.</param>
                /// <param name="pvaIn">The input arguments of the command.</param>
                /// <param name="pvaOut">The output arguments of the command.</param>
                /// <returns>This method returns S_OK on success. Other possible return values include 
                /// OLECMDERR_E_UNKNOWNGROUP The <paramref name="pguidCmdGroup" /> parameter is not null but does not specify a recognized command group.
                /// OLECMDERR_E_NOTSUPPORTED The <paramref name="nCmdID" /> parameter is not a valid command in the group identified by <paramref name="pguidCmdGroup" />.
                /// OLECMDERR_E_DISABLED The command identified by <paramref name="nCmdID" /> is currently disabled and cannot be executed.
                /// OLECMDERR_E_NOHELP The caller has asked for help on the command identified by <paramref name="nCmdID" />, but no help is available.
                /// OLECMDERR_E_CANCELED The user canceled the execution of the command.</returns>
                public int Exec(ref Guid pguidCmdGroup, int nCmdID, int nCmdexecopt, object[] pvaIn, int pvaOut)
                {
                    if ((int)NativeMethods.OLECMDID.OLECMDID_SHOWSCRIPTERROR == nCmdID)
                    {   // Do nothing (suppress script errors)
                        return NativeMethods.S_OK;
                    }

                    // Indicate that command is unknown. The command will then be handled by another IOleCommandTarget.
                    return NativeMethods.OLECMDERR_E_UNKNOWNGROUP;
                }

            }

            #endregion
        }
    }

    /// <summary>
    /// Native (unmanaged) methods, required for custom command handling for the WebBrowser control.
    /// </summary>
    public static class NativeMethods
    {
        /// From docobj.h
        public const int OLECMDERR_E_UNKNOWNGROUP = -2147221244;

        /// <summary>
        /// From Microsoft.VisualStudio.OLE.Interop (Visual Studio 2010 SDK).
        /// </summary>
        public enum OLECMDID
        {
            /// <summary />
            OLECMDID_OPEN = 1,
            /// <summary />
            OLECMDID_NEW,
            /// <summary />
            OLECMDID_SAVE,
            /// <summary />
            OLECMDID_SAVEAS,
            /// <summary />
            OLECMDID_SAVECOPYAS,
            /// <summary />
            OLECMDID_PRINT,
            /// <summary />
            OLECMDID_PRINTPREVIEW,
            /// <summary />
            OLECMDID_PAGESETUP,
            /// <summary />
            OLECMDID_SPELL,
            /// <summary />
            OLECMDID_PROPERTIES,
            /// <summary />
            OLECMDID_CUT,
            /// <summary />
            OLECMDID_COPY,
            /// <summary />
            OLECMDID_PASTE,
            /// <summary />
            OLECMDID_PASTESPECIAL,
            /// <summary />
            OLECMDID_UNDO,
            /// <summary />
            OLECMDID_REDO,
            /// <summary />
            OLECMDID_SELECTALL,
            /// <summary />
            OLECMDID_CLEARSELECTION,
            /// <summary />
            OLECMDID_ZOOM,
            /// <summary />
            OLECMDID_GETZOOMRANGE,
            /// <summary />
            OLECMDID_UPDATECOMMANDS,
            /// <summary />
            OLECMDID_REFRESH,
            /// <summary />
            OLECMDID_STOP,
            /// <summary />
            OLECMDID_HIDETOOLBARS,
            /// <summary />
            OLECMDID_SETPROGRESSMAX,
            /// <summary />
            OLECMDID_SETPROGRESSPOS,
            /// <summary />
            OLECMDID_SETPROGRESSTEXT,
            /// <summary />
            OLECMDID_SETTITLE,
            /// <summary />
            OLECMDID_SETDOWNLOADSTATE,
            /// <summary />
            OLECMDID_STOPDOWNLOAD,
            /// <summary />
            OLECMDID_ONTOOLBARACTIVATED,
            /// <summary />
            OLECMDID_FIND,
            /// <summary />
            OLECMDID_DELETE,
            /// <summary />
            OLECMDID_HTTPEQUIV,
            /// <summary />
            OLECMDID_HTTPEQUIV_DONE,
            /// <summary />
            OLECMDID_ENABLE_INTERACTION,
            /// <summary />
            OLECMDID_ONUNLOAD,
            /// <summary />
            OLECMDID_PROPERTYBAG2,
            /// <summary />
            OLECMDID_PREREFRESH,
            /// <summary />
            OLECMDID_SHOWSCRIPTERROR,
            /// <summary />
            OLECMDID_SHOWMESSAGE,
            /// <summary />
            OLECMDID_SHOWFIND,
            /// <summary />
            OLECMDID_SHOWPAGESETUP,
            /// <summary />
            OLECMDID_SHOWPRINT,
            /// <summary />
            OLECMDID_CLOSE,
            /// <summary />
            OLECMDID_ALLOWUILESSSAVEAS,
            /// <summary />
            OLECMDID_DONTDOWNLOADCSS,
            /// <summary />
            OLECMDID_UPDATEPAGESTATUS,
            /// <summary />
            OLECMDID_PRINT2,
            /// <summary />
            OLECMDID_PRINTPREVIEW2,
            /// <summary />
            OLECMDID_SETPRINTTEMPLATE,
            /// <summary />
            OLECMDID_GETPRINTTEMPLATE
        }

        /// <summary>
        /// From Microsoft.VisualStudio.Shell (Visual Studio 2010 SDK).
        /// </summary>
        public const int S_OK = 0;

        /// <summary>
        /// OLE command structure.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class OLECMD
        {
            /// <summary>
            /// Command ID.
            /// </summary>
            [MarshalAs(UnmanagedType.U4)]
            public int cmdID;
            /// <summary>
            /// Flags associated with cmdID.
            /// </summary>
            [MarshalAs(UnmanagedType.U4)]
            public int cmdf;
        }

        /// <summary>
        /// Enables the dispatching of commands between objects and containers.
        /// </summary>
        [ComVisible(true), Guid("B722BCCB-4E68-101B-A2BC-00AA00404770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport]
        public interface IOleCommandTarget
        {
            /// <summary>Queries the object for the status of one or more commands generated by user interface events.</summary>
            /// <param name="pguidCmdGroup">The GUID of the command group.</param>
            /// <param name="cCmds">The number of commands in <paramref name="prgCmds" />.</param>
            /// <param name="prgCmds">An array of <see cref="T:Microsoft.VisualStudio.OLE.Interop.OLECMD" /> structures that indicate the commands for which the caller needs status information.</param>
            /// <param name="pCmdText">An <see cref="T:Microsoft.VisualStudio.OLE.Interop.OLECMDTEXT" /> structure in which to return name and/or status information of a single command. This parameter can be null to indicate that the caller does not need this information.</param>
            /// <returns>This method returns S_OK on success. Other possible return values include the following.
            /// E_FAIL The operation failed.
            /// E_UNEXPECTED An unexpected error has occurred.
            /// E_POINTER The <paramref name="prgCmds" /> argument is null.
            /// OLECMDERR_E_UNKNOWNGROUPThe <paramref name="pguidCmdGroup" /> parameter is not null but does not specify a recognized command group.</returns>
            [PreserveSig]
            [return: MarshalAs(UnmanagedType.I4)]
            int QueryStatus(ref Guid pguidCmdGroup, int cCmds, [In] [Out] NativeMethods.OLECMD prgCmds, [In] [Out] IntPtr pCmdText);

            /// <summary>Executes the specified command.</summary>
            /// <param name="pguidCmdGroup">The GUID of the command group.</param>
            /// <param name="nCmdID">The command ID.</param>
            /// <param name="nCmdexecopt">Specifies how the object should execute the command. Possible values are taken from the <see cref="T:Microsoft.VisualStudio.OLE.Interop.OLECMDEXECOPT" /> and <see cref="T:Microsoft.VisualStudio.OLE.Interop.OLECMDID_WINDOWSTATE_FLAG" /> enumerations.</param>
            /// <param name="pvaIn">The input arguments of the command.</param>
            /// <param name="pvaOut">The output arguments of the command.</param>
            /// <returns>This method returns S_OK on success. Other possible return values include 
            /// OLECMDERR_E_UNKNOWNGROUP The <paramref name="pguidCmdGroup" /> parameter is not null but does not specify a recognized command group.
            /// OLECMDERR_E_NOTSUPPORTED The <paramref name="nCmdID" /> parameter is not a valid command in the group identified by <paramref name="pguidCmdGroup" />.
            /// OLECMDERR_E_DISABLED The command identified by <paramref name="nCmdID" /> is currently disabled and cannot be executed.
            /// OLECMDERR_E_NOHELP The caller has asked for help on the command identified by <paramref name="nCmdID" />, but no help is available.
            /// OLECMDERR_E_CANCELED The user canceled the execution of the command.</returns>
            [PreserveSig]
            [return: MarshalAs(UnmanagedType.I4)]
            int Exec(ref Guid pguidCmdGroup, int nCmdID, int nCmdexecopt, [MarshalAs(UnmanagedType.LPArray)] [In] object[] pvaIn, int pvaOut);
        }



        public enum ErrorFlags
        {
            ERROR_INSUFFICIENT_BUFFER = 122,
            ERROR_INVALID_PARAMETER = 87,
            ERROR_NO_MORE_ITEMS = 259
        }

        public enum InternetFlags
        {
            INTERNET_COOKIE_HTTPONLY = 8192, //Requires IE 8 or higher   
            INTERNET_COOKIE_THIRD_PARTY = 131072,
            INTERNET_FLAG_RESTRICTED_ZONE = 16
        }





        [DllImport("wininet.dll", EntryPoint = "InternetGetCookieExW", CharSet = CharSet.Unicode, SetLastError = true, ExactSpelling = true)]
        internal static extern bool InternetGetCookieEx([In] string Url, [In] string cookieName, [Out] StringBuilder cookieData, [In, Out] ref uint pchCookieData, uint flags, IntPtr reserved);

    }



    internal static class WindowsInterop
    {




        #region << Variable Declarations >>

        private const Int32 WM_COMMAND = 0x0111;
        private const Int32 WM_INITDIALOG = 0x0110;

        private static IntPtr _pWH_CALLWNDPROCRET = IntPtr.Zero;

        private static HookProcedureDelegate _WH_CALLWNDPROCRET_PROC = new HookProcedureDelegate(WindowsInterop.WH_CALLWNDPROCRET_PROC);

        //internal static event GenericDelegate<Boolean, Boolean> SecurityAlertDialogWillBeShown;
        //internal static event GenericDelegate<String, String, Boolean> ConnectToDialogWillBeShown;

        #endregion

        #region << DllImports >>

        [DllImport("user32.dll")]
        private static extern IntPtr SetWindowsHookEx(HookType hooktype, HookProcedureDelegate callback, IntPtr hMod, UInt32 dwThreadId);

        [DllImport("user32.dll")]
        private static extern IntPtr UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll")]
        private static extern Int32 CallNextHookEx(IntPtr hhk, Int32 nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern Int32 GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern Int32 GetWindowText(IntPtr hWnd, StringBuilder text, Int32 maxLength);

        [DllImport("user32.dll")]
        private static extern Boolean SetWindowText(IntPtr hWnd, String lpString);

        [DllImport("user32.dll")]
        private static extern Int32 GetClassName(IntPtr hWnd, StringBuilder lpClassName, Int32 nMaxCount);

        [DllImport("user32.dll")]
        private static extern Boolean EnumChildWindows(IntPtr hWndParent, EnumerateWindowDelegate callback, IntPtr data);

        [DllImport("user32.dll", SetLastError = false)]
        private static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern int GetDlgCtrlID(IntPtr hwndCtl);

        [DllImport("kernel32.dll")]
        static extern uint GetCurrentThreadId();

        #endregion

        #region << Managed Structures/Enums >>

        // Hook Types
        private enum HookType
        {
            WH_JOURNALRECORD = 0,
            WH_JOURNALPLAYBACK = 1,
            WH_KEYBOARD = 2,
            WH_GETMESSAGE = 3,
            WH_CALLWNDPROC = 4,
            WH_CBT = 5,
            WH_SYSMSGFILTER = 6,
            WH_MOUSE = 7,
            WH_HARDWARE = 8,
            WH_DEBUG = 9,
            WH_SHELL = 10,
            WH_FOREGROUNDIDLE = 11,
            WH_CALLWNDPROCRET = 12,
            WH_KEYBOARD_LL = 13,
            WH_MOUSE_LL = 14
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct CWPRETSTRUCT
        {
            public IntPtr lResult;
            public IntPtr lParam;
            public IntPtr wParam;
            public UInt32 message;
            public IntPtr hwnd;
        };

        #endregion

        #region << Managed Delegates For Unmanaged Functions >>

        // Delegate for a WH_ hook procedure
        private delegate Int32 HookProcedureDelegate(Int32 iCode, IntPtr pWParam, IntPtr pLParam);

        // Delegate for the EnumChildWindows method
        private delegate Boolean EnumerateWindowDelegate(IntPtr pHwnd, IntPtr pParam);

        #endregion

        #region << Internal Static Functions >>

        // Add a Hook into the CALLWNDPROCRET notification chain
        internal static void Hook()
        {
            if (WindowsInterop._pWH_CALLWNDPROCRET == IntPtr.Zero)
            {
                WindowsInterop._pWH_CALLWNDPROCRET = SetWindowsHookEx(HookType.WH_CALLWNDPROCRET, WindowsInterop._WH_CALLWNDPROCRET_PROC, IntPtr.Zero, GetCurrentThreadId());

                // NB: Visual Studio will likely be upset about 
                // the use of the Obsolete 'GetCurrentThreadId()' function
                // however, unless the app is running on fibres 
                // it seems to do the job quite nicely.
            }
        }

        // Remove the Hook into the CALLWNDPROCRET notification chain
        internal static void Unhook()
        {
            if (WindowsInterop._pWH_CALLWNDPROCRET != IntPtr.Zero)
            {
                UnhookWindowsHookEx(WindowsInterop._pWH_CALLWNDPROCRET);
            }
        }

        #endregion

        #region << Private Parts >>

        // Hook proceedure called by the OS when a message has been processed by the target Window
        private static Int32 WH_CALLWNDPROCRET_PROC(Int32 iCode, IntPtr pWParam, IntPtr pLParam)
        {
            if (iCode < 0)
                return CallNextHookEx(WindowsInterop._pWH_CALLWNDPROCRET, iCode, pWParam, pLParam);

            CWPRETSTRUCT cwp = (CWPRETSTRUCT)
                Marshal.PtrToStructure(pLParam, typeof(CWPRETSTRUCT));

            if (cwp.message == WM_INITDIALOG)
            {
                // A dialog was initialised, find out what sort it was via it's Caption text
                Int32 iLength = GetWindowTextLength(cwp.hwnd);
                StringBuilder sb = new StringBuilder(iLength + 1);

                GetWindowText(cwp.hwnd, sb, sb.Capacity);
                if ("Security Alert".Equals(sb.ToString(), StringComparison.InvariantCultureIgnoreCase) || "Avviso di sicurezza".Equals(sb.ToString(), StringComparison.InvariantCultureIgnoreCase) || "Certificate Error".Equals(sb.ToString(), StringComparison.InvariantCultureIgnoreCase))
                {
                    // A "Security Alert" dialog was initialised, now need 
                    //
                    // a)   To know what type it is - e.g. is it an SSL related one or a switching b/w 
                    //      secure/non-secure modes etc one - and,
                    //
                    // b)   A handle to the 'Yes' button so a user-click can be simulated on it
                    //
                    Boolean blnIsSslDialog = true; // assumed true for now
                    IntPtr pYesButtonHwnd = IntPtr.Zero;

                    // Check out further properties of the dialog
                    foreach (IntPtr pChildOfDialog in WindowsInterop.listChildWindows(cwp.hwnd))
                    {
                        // Go through all of the child controls on the dialog and see what they reveal via their text
                        iLength = GetWindowTextLength(pChildOfDialog);
                        if (iLength > 0)
                        {
                            StringBuilder sbProbe = new StringBuilder(iLength + 1);
                            GetWindowText(pChildOfDialog, sbProbe, sbProbe.Capacity);



                            if ("&S�".Equals(sbProbe.ToString(), StringComparison.InvariantCultureIgnoreCase) || "&Yes".Equals(sbProbe.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            {
                                // Hey, this one says 'Yes', so cache a pointer to it
                                pYesButtonHwnd = pChildOfDialog;

                                // NB: Could also confim it is a button we're pointing at, at this point, and
                                // not a label or something by using the Win32 API's GetClassName() function (see below for e.g.),
                                // but in this case it is already known that a button is the only thing saying 'Yes' 
                                // on this particular dialog
                            }
                        }
                    }

                    // Ok, some sort of "Security Alert" dialog was initialised, fire a message 
                    // to anyone who's listening i.e. In this case, ask the event receiver if 
                    // they want to ignore it

                    if (blnIsSslDialog && pYesButtonHwnd != IntPtr.Zero)
                    {
                        // Alert the dialog's message-pump that the 'Yes' button was 'Pressed'
                        Int32 ctrlId = GetDlgCtrlID(pYesButtonHwnd);
                        SendMessage(cwp.hwnd, WM_COMMAND, new IntPtr(ctrlId), pYesButtonHwnd);

                        // Block any further processing of the WM_INITDIALOG message by anyone else
                        // This is important: by doing this, the dialog never shows up on the screen
                        return 1;
                    }

                }
            }
            // Call the next hook in the chain
            return CallNextHookEx(WindowsInterop._pWH_CALLWNDPROCRET, iCode, pWParam, pLParam);
        }

        // Populate a list of all the child windows of a given parent 
        // (Uses the Win32 API's EnumChildWindows() function)
        private static List<IntPtr> listChildWindows(IntPtr p)
        {
            List<IntPtr> lstChildWindows = new List<IntPtr>();
            GCHandle gchChildWindows = GCHandle.Alloc(lstChildWindows);
            try
            {
                WindowsInterop.EnumChildWindows(p,
                    new EnumerateWindowDelegate(WindowsInterop.enumWindowsCallback),
                        GCHandle.ToIntPtr(gchChildWindows));
            }
            finally
            {
                if (gchChildWindows.IsAllocated)
                    gchChildWindows.Free();
            }

            return lstChildWindows;
        }

        // Callback method called when EnumChildWindows is enumerating windows.
        // (Called by the Win32API EnumChildWindows function)
        private static bool enumWindowsCallback(IntPtr hwnd, IntPtr p)
        {
            GCHandle gch = GCHandle.FromIntPtr(p);
            if (gch != null)
            {
                List<IntPtr> lstChildWindows = gch.Target as List<IntPtr>;
                if (lstChildWindows == null)
                {
                    // This should NEVER happen
                    //throw new InvalidCastException(StringConstants.GCHandleTargetIsNotExpectedType);
                }

                lstChildWindows.Add(hwnd);
            }

            // Continue processing down the parent-child chain
            return true;
        }


        #endregion
    }

   
}
