﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ININ.InteractionClient.AddIn;
using System.Threading;
using ININ.IceLib.QualityManagement;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;

namespace BizmaticaAddin
{
    internal partial class RecorderCall : Form
    {
        public RecorderCall()
        {
            InitializeComponent();
        }
        public SynchronizationContext SyncContext { get; set; }
        public INotificationService NotificationService { get; set; }
        public ITraceContext Trace { get; set; }

        public RecordingsManager recordingsManager { get; set; }

        public object recording { get; set; }
        public object numAttachment { get; set; }
        public object media { get; set; }
        public object callId { get; set; }



        string[] recordings;
        private void RecorderCall_Load(object sender, EventArgs e)
        {

                recordings = (string[])recording;

                this.Text = "Registrazioni chiamata: " + ((string)callId);



                this.Height=100 + 20 * ((recordings.Length>2?(recordings.Length - 2):0) / 2);
                for (int i = 1; i <= recordings.Length; i++)
                {

                    try { 
                        LinkLabel attachment = new LinkLabel();

                        attachment.AutoSize = true;
                        attachment.BackColor = System.Drawing.SystemColors.Control;
                        attachment.Location = new System.Drawing.Point(i%2==1?40:170, 20 +((i-1)/2)*20);
                        attachment.Name = "linkLabel_"+i;
                        attachment.Size = new System.Drawing.Size(55, 13);
                        attachment.Text = "Registrazione " + i.ToString();
                        attachment.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);

                        attachment.Tag = recordingsManager.GetExportUri(recordings[i-1], ININ.IceLib.QualityManagement.RecordingMediaType.PrimaryMedia, "", 0).OriginalString;
                        attachment.BringToFront();
                        this.Controls.Add(attachment);
                    }
                    catch (Exception ex)
                    {
                        Trace.Exception(ex, "Errore recupero registrazione");
                        NotificationService.Notify("Si è verificato un errore durante il recupero della registrazione", "Avviso", NotificationType.Warning, TimeSpan.FromSeconds(8));
                        return;
                    }
        }





            
        }

            
        public void ShowForm()
        {
            Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            webBrowser1.Navigate((string)((LinkLabel)sender).Tag);
        }
    }
}
