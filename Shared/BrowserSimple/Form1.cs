﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace BizmaticaAddin
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            timer1.Enabled = true;
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case Win32.WM_COPYDATA:
                    Win32.CopyDataStruct st = (Win32.CopyDataStruct)Marshal.PtrToStructure(m.LParam, typeof(Win32.CopyDataStruct));
                    string strData = Marshal.PtrToStringUni(st.lpData);
                    if (strData.Substring(0, 9).Equals("resizeTo:"))
                    {
                        this.Width = Int32.Parse(strData.Replace("resizeTo:", "").Split(new char[] { ',' })[0]);
                        this.Height = Int32.Parse(strData.Replace("resizeTo:", "").Split(new char[] { ',' })[1]);
                    }
                    else if (strData.Substring(0, 19).Equals("injectedJavascript:"))
                    {
                        strData = strData.Replace("injectedJavascript:", "");
                        MyInvokeScript(webBrowser1.Document.Window, "eval", new Object[] { strData });
                    }
                    else
                        webBrowser1.Navigate(strData);
                    break;
                default:
                    // let the base class deal with it
                    base.WndProc(ref m);
                    break;
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            
        }

        public object MyInvokeScript(HtmlWindow frm, string name, params object[] args)
        {
            return frm.Document.InvokeScript(name, args);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (Win32.GetRealParent(this.Handle).ToInt32() <= 0) { this.Dispose(); }
        }


       




    }
}

