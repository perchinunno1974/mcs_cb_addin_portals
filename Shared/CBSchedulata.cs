﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using System.Threading;
using ININ.InteractionClient.AddIn;
using System.Text.RegularExpressions;


namespace BizmaticaAddin
{


    


    public partial class CBSchedulata : Form
    {


        public SynchronizationContext SyncContext { get; set; }
        public Session _session { get; set; }
        public ITraceContext _trace { get; set; }
        public CustomNotification _Notifier { get; set; }

        public string idSoggetto { get; set; }
        public string telefono { get; set; }

        private DateTime mPrevDate;
        //private bool mBusy;



        public CBSchedulata()
        {
            InitializeComponent();
            dateTimePicker2.GotFocus += DateTimePicker2_GotFocus;
        }

        private void DateTimePicker2_GotFocus(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

            /*if (!mBusy)
            {
                mBusy = true;
                DateTime dt = dateTimePicker2.Value;

                TimeSpan diff = dt - mPrevDate;
                if (diff.Minutes==1 )
                    dateTimePicker2.Value = mPrevDate.AddMinutes(AddinQM.ScheduledCallbackConfigurationInterval);
                else if(diff.Minutes == 59)
                    dateTimePicker2.Value = mPrevDate.AddMinutes(-AddinQM.ScheduledCallbackConfigurationInterval);
                else
                    dateTimePicker2.Value = dt.AddMilliseconds(-dt.Millisecond).AddSeconds(-dt.Second).AddMinutes(-(dt.Minute % AddinQM.ScheduledCallbackConfigurationInterval));

                mBusy = false;
            }
            mPrevDate = dateTimePicker2.Value;*/
        }

        private void CBSchedulata_Load(object sender, EventArgs e)
        {

        }

        public void ShowForm()
        {
            mPrevDate = DateTime.Now.AddMilliseconds(-DateTime.Now.Millisecond).AddSeconds(-DateTime.Now.Second).AddMinutes(30 - (DateTime.Now.Minute % 30));
            if ((mPrevDate-DateTime.Now).TotalMinutes<15) mPrevDate=mPrevDate.AddMinutes(30);
            dateTimePicker2.Value = mPrevDate;
            dateTimePicker1.Value = mPrevDate;

            textBox1.Text = telefono.Replace(" ","");
            this.Text = "Presa appuntamento cliente " + (idSoggetto.Equals("")? "non specificato" : idSoggetto) ;
            Show();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                dateTimePicker1.Value = dateTimePicker1.Value.AddMilliseconds(-dateTimePicker1.Value.Millisecond).AddSeconds(-dateTimePicker1.Value.Second).AddMinutes(-dateTimePicker1.Value.Minute).AddHours(-dateTimePicker1.Value.Hour);

                DateTime appointment = dateTimePicker1.Value.AddHours(dateTimePicker2.Value.Hour).AddMinutes(dateTimePicker2.Value.Minute);

                if ((appointment - DateTime.Now).TotalMinutes < 1)
                {
                    label4.Visible = true;
                    label5.Visible = true;
                    return;
                }


                Regex regex = new Regex(@"(^[0|3|+]{1}[0-9]{5,13}$)");
                Match match = regex.Match(textBox1.Text);

                if (!match.Success && !textBox1.Text.Trim().Equals(""))
                {
                    label3.Visible = true;
                    return;
                }




                CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "CallbackCreateSchedule", _session.UserId);
                CustomRequest request = new CustomRequest(header);
                string[] parametri = new string[] { appointment.ToString("yyyyMMddHHmm"), idSoggetto, textBox1.Text, textBox2.Text.Replace(System.Environment.NewLine, " _$ "), checkBox1.Checked ? _session.UserId : "", checkBox2.Checked ? "Videochat" : "" };
                request.Write(parametri);

                _trace.Always("BIZMATICA:custom notification CallbackCreateSchedule\n" + parametri[0]);
                _Notifier.SendServerRequestNoResponse(request);
                this.Dispose();
            }
            catch(Exception ex)
            {
                _trace.Exception(ex, "CBSchedulata - button1_Click");
            }
        }

        private void textBox1_GotFocus(object sender, EventArgs e)
        {
            label3.Visible = false;
        }

        private void dateTimePicker1_GotFocus(object sender, EventArgs e)
        {
            label4.Visible = false;
            label5.Visible = false;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            label3.Visible = false;
        }

        private void label4_Click(object sender, EventArgs e)
        {
            label4.Visible = false;
        }

        private void label5_Click(object sender, EventArgs e)
        {
            label5.Visible = false;
        }
    }
}
