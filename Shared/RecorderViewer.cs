﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ININ.InteractionClient.AddIn;
using ININ.IceLib.QualityManagement;
using System.Threading;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;

namespace BizmaticaAddin
{
    internal partial class RecorderViewer : Form
    {
        public RecorderViewer()
        {
            InitializeComponent();
        }
        public SynchronizationContext SyncContext { get; set; }
        public INotificationService NotificationService { get; set; }
        public ITraceContext Trace { get; set; }
        
        public RecordingsManager recordingsManager { get; set; }

        public object recording { get; set; }
        public object numAttachment { get; set; }
        public object media { get; set; }
        public object callId { get; set; }



        byte[][] recordings;
        //MemoryStream[] mstream;
        string[] filenames;
        string[] paths; 
        int[] sizes;
        private void RecorderViewer_Load(object sender, EventArgs e)
        {
            try { 
                recordings = new byte[Int32.Parse(numAttachment.ToString())>0? Int32.Parse(numAttachment.ToString())+1:0][];
                paths = new string[Int32.Parse(numAttachment.ToString()) > 0 ? Int32.Parse(numAttachment.ToString()) + 1 : 0];
                filenames = new string[Int32.Parse(numAttachment.ToString()) > 0 ? Int32.Parse(numAttachment.ToString()) + 1 : 0];
                sizes = new int[Int32.Parse(numAttachment.ToString()) > 0 ? Int32.Parse(numAttachment.ToString()) + 1 : 0];

                for (int i=0; i< recordings.Length; i++)
                {
                    Uri downloadUri = recordingsManager.GetExportUri((string)recording, ININ.IceLib.QualityManagement.RecordingMediaType.EmailAttachment, "", i);
                    var wc = new WebClient();
                    recordings[i] = wc.DownloadData(downloadUri.OriginalString);
                    paths[i] = downloadUri.OriginalString;
                    filenames[i]= wc.ResponseHeaders["Content-Disposition"].Substring(wc.ResponseHeaders["Content-Disposition"].IndexOf("filename=") + 9).Replace("\"", "");
                    sizes[i]=Int32.Parse(wc.ResponseHeaders["Content-Length"]) / 1024;
                }
            }
            catch(Exception ex)
            {
                Trace.Exception(ex, "Errore recupero registrazione");
                NotificationService.Notify("Si è verificato un errore durante il recupero della registrazione", "Avviso", NotificationType.Warning, TimeSpan.FromSeconds(8));
                return;
            }



            if (((string)media).Equals("2"))
            {

                var Stream = System.Text.Encoding.UTF8.GetString(recordings[0]);
                webBrowser1.Visible = true;
                this.Text = "Trascrizione chat: " + ((string)callId);
                try
                {
                    string pattern = @"^(.*)\s\[(\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d)\.\d*\]:\s(.*)";
                    RegexOptions options = RegexOptions.Multiline;
                    string final = "<style> body { background-color: rgb(240, 247, 247); } .left{ border: 1px solid rgb(120, 250, 100); width: 500px; background-color:rgb(127, 255, 102); color: black; padding; 5px; margin-left: 200px; margin-top: 10px; margin-bottom: 10px; font-family:tahoma; font-size:medium; } .right{ border: 1px solid rgb(222, 222, 222); width: 400px; background-color:white; color: black; padding: 5px; margin-left: 0px margin-top: 10px; margin-bottom: 10px; font-family:tahoma; font-size:medium; } .data{ color: rgb(140, 140, 140); margin-right:10px; margin-top:3px; margin-bottom:8px; font-size:smaller; text-align:right; } .testo{ color: black; margin-left:12px; margin-top:0px; margin-bottom:0px; text-align:left; padding: 0px; font-size:smaller; } .nome{ color: black; margin-left:12px; margin-top:10px; margin-bottom:5px; text-align:left; padding: 0px; font-size:smaller; font-weight:700; }</style>";
                    string fname = "";
                    string pos = "left";
                    foreach (Match m in Regex.Matches(Stream, pattern, options))
                    {
                        
                        if (!fname.Equals(m.Groups[1].Value))
                        {
                            fname = m.Groups[1].Value;
                            if (pos.Equals("left")) pos ="right";
                            else pos = "left";

                            final += String.Concat("<div class=\"", pos, "\"><p class=\"nome\">", System.Net.WebUtility.HtmlEncode(m.Groups[1].Value), "</p>");
                        }
                        
                        

                        
                        final += String.Concat("<p class=\"testo\">", System.Net.WebUtility.HtmlEncode(m.Groups[3].Value), "</p><p class=\"data\">" + System.Net.WebUtility.HtmlEncode(m.Groups[2].Value) + "</p>");

                        Match next = m.NextMatch();
                        if (!(next != null && next.Groups[1].Value.Equals(fname))) final += String.Concat("</div>");
                    }

                    webBrowser1.DocumentText = final;
                }
                catch(Exception ex)
                {
                    webBrowser1.DocumentText = ex.Message;
                }
            }
            else if (((string)media).Equals("3"))
            {
                this.Text = "Email: " + ((string)callId);
                textBox1.Visible = true;

                textBox1.Text = System.Text.Encoding.UTF8.GetString(recordings[0]);
                recordings[0] = null;
                this.Height=400 + 20 * ((sizes.Length>2?(sizes.Length - 2):0) / 2);
                for (int i = 1; i < sizes.Length; i++)
                {
                    LinkLabel attachment = new LinkLabel();

                    attachment.AutoSize = true;
                    attachment.BackColor = System.Drawing.SystemColors.Control;
                    attachment.Location = new System.Drawing.Point(i%2==1?120:420, 340+((i-1)/2)*20);
                    attachment.Name = "linkLabel_"+i;
                    attachment.Size = new System.Drawing.Size(55, 13);
                    attachment.Text = filenames[i];
                    attachment.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
                    attachment.Tag = paths[i];// new MemoryStream(recordings[i]);
                    attachment.BringToFront();
                    this.Controls.Add(attachment);
                    //mstream[i] = new MemoryStream(recordings[i]);

                }




            }
            
        }

            
        public void ShowForm()
        {
            Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //webBrowser1.DocumentStream = (MemoryStream)((LinkLabel)sender).Tag;
            webBrowser1.Navigate((string)((LinkLabel)sender).Tag);
        }
    }
}
