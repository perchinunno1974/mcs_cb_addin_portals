﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ININ.InteractionClient.AddIn;
using System.Threading;
using System.Net;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.Configuration;

namespace BizmaticaAddin
{
    internal partial class sceltaEmail : Form
    {
        public sceltaEmail()
        {
            InitializeComponent();
            



        }

        public SynchronizationContext SyncContext { get; set; }
        public object workgroups { get; set; }

        public CustomNotification Notifier;

        public string user { get; set; }
        public string email { get; set; }
        public string idOperazionale { get; set; }

        public string SMSESSION { get; set; }
        public string CBCLICKTRACKID { get; set; }

        bool stop = false;
        public void ShowForm()
        {
            Show();



            foreach (WorkgroupConfiguration wg in (List<WorkgroupConfiguration>)workgroups)
            {
                lwitem currwg = new lwitem();
                currwg.Tag = wg;
                currwg.Text = wg.ConfigurationId.DisplayName;
                listView1.Items.Add(currwg);
                listView1.Click += ListView1_Click;
            }
            if (listView1.Items.Count == 1)
            {
                WorkgroupConfiguration wg = (WorkgroupConfiguration)((ListViewItem)listView1.Items[0]).Tag;
                string name, email;
                //wg.StandardProperties.CustomAttributes.Value.TryGetValue("DISPLAYINFILTER", out name);
                name = wg.ConfigurationId.Id;
                wg.StandardProperties.CustomAttributes.Value.TryGetValue("ADDRESSINFILTER", out email);
                createEmail(name, email);
                this.Dispose();
            }
        }

        private void ListView1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                WorkgroupConfiguration wg = (WorkgroupConfiguration)((ListViewItem)listView1.SelectedItems[0]).Tag;
                string name, email;
                //wg.StandardProperties.CustomAttributes.Value.TryGetValue("DISPLAYINFILTER", out name);
                name = wg.ConfigurationId.Id;
                wg.StandardProperties.CustomAttributes.Value.TryGetValue("ADDRESSINFILTER", out email);
                createEmail(name, email);
                this.Dispose();
            }
        }

        private void createEmail(string workgroup, string emailWorkgroup)
        {
            
            CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "email", "createdraft");
            CustomRequest request = new CustomRequest(header);
            string[] parametri = new string[] { user, email, emailWorkgroup, workgroup, idOperazionale, SMSESSION, CBCLICKTRACKID };
            request.Write(parametri);
            if (stop) return;
            stop = true;
            Notifier.SendServerRequestNoResponse(request);

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            
        }
    }
    internal class lwitem : System.Windows.Forms.ListViewItem
    {
        public override string ToString()
        {
            return this.Text;
        }
    }
}
