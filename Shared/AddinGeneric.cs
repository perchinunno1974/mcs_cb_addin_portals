﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml;
using System.Net;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using ININ.IceLib.Data.TransactionBuilder;
using System.ComponentModel;
using System.Diagnostics;
using System.Text.RegularExpressions;
using ININ.InteractionClient.AddIn;
using ININ.IceLib.Configuration;
using ININ.IceLib.Connection;
using ININ.IceLib.QualityManagement;
using System.Reflection;
using System.Runtime.InteropServices;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib;
using System.Threading.Tasks;

namespace BizmaticaAddin
{
    internal partial class AddinGeneric : UserControl, IWindow
    {
        
        private Session _session;
        private ITraceContext _trace;
        private CustomNotification _Notifier;
        private SynchronizationContext _syncContext;
        private TransactionClient _transactionClient;
        private INotificationService _notificationService;
        private ConfigurationManager _configurationManager;
        private ININ.IceLib.Interactions.InteractionsManager _interactionManager;
        private RecordingsManager _RecordingsManager;
        private QualityManagementManager _QualityManagementManager;

        //private static IInteraction activeIncomingCall;
        private IInteraction SelectedInteraction;

        private string StartingUrl = "";
        private string InteractionUrl = "";
        private string Version = "";
        private string portalName = "Non inizializzato";

        internal Dictionary<string, string> _interactions=new Dictionary<string, string>();
        internal static object _interactionsListLock = new object();

        private bool confError = false;

        private readonly string RECORDING_STORED_PROCEDURE = "Bizmatica_getRecordingId";


        private string RefreshableChannels = "";

        private string downloadUrl = "";
        private string resourceForAuth = "";
        private string loginInTab = "";

        private string toolIcon = "";

        private bool externalBrowserEnabled = false;

        private bool detectCookieChange = false;
            
        private IntPtr hwnd = IntPtr.Zero;
        private Process externalBrowser = new Process();
        private ProcessStartInfo psi;
        //public static CustomNotification _Notifier;

        private void setCurrentCustomer(string idOperazionale)
        {
            try { 
                var parameters = Utilities.getInstance();
                _trace.Always("BIZMATICA: " + portalName + " setCurrentCustomer " + idOperazionale.ToString());

                parameters.LastContextCustomer = idOperazionale;
                parameters.LastContextCustomerDate = DateTime.Now;

                _trace.Always("BIZMATICA: " + portalName + " setCurrentCustomer Agente_UserId " + _session.UserId);
                _trace.Always("BIZMATICA: " + portalName + " setCurrentCustomer Agente_cb_clicktrack_id " + parameters.clicktrackId);
                _trace.Always("BIZMATICA: " + portalName + " setCurrentCustomer Agente_SMSESSION\n" + parameters.AuthSession);

                if (SelectedInteraction != null)
                {
                    _trace.Always("BIZMATICA: " + portalName + " Interazione selezionata " + SelectedInteraction.InteractionId.ToString());
                    SelectedInteraction.SetAttribute("Agente_UserId", _session.UserId);
                    SelectedInteraction.SetAttribute("Agente_SMSESSION", parameters.AuthSession);
                    SelectedInteraction.SetAttribute("Agente_cb_clicktrack_id", parameters.clicktrackId);

                    if (SelectedInteraction.GetAttribute("idSoggetto").Equals("-1") || SelectedInteraction.GetAttribute("idSoggetto").Equals(""))
                    {
                        _trace.Always("BIZMATICA: " + portalName + " Impostato idSoggetto " + idOperazionale.ToString() + "ad interazione " + SelectedInteraction.InteractionId.ToString());
                        SelectedInteraction.SetAttribute("idSoggetto", idOperazionale);
                    }

                    _trace.Always("BIZMATICA: " + portalName + " Invio contestualizzazione");
                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "Contestualizzazione", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { SelectedInteraction.InteractionId };

                    request.Write(parametri);

                    _trace.Always("BIZMATICA: " + portalName + " Contestualizzazione \n" + parametri[0]);
                    _Notifier.SendServerRequestNoResponse(request);

                }
                else
                {
                    _trace.Always("BIZMATICA: " + portalName + " Contestualizzazione selezionata senza interazione selezionata");
                    //notificationService.Notify("Selezionare una interazione", "Contestualizzazione", NotificationType.Error, new TimeSpan(0, 0, 4));
                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "Contestualizzazione", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { "", idOperazionale, _session.UserId, parameters.AuthSession, parameters.clicktrackId };

                    request.Write(parametri);

                    _trace.Always("BIZMATICA: " + portalName + " custom notification Context interazione solo soggetto\n" + parametri[0]);
                    _Notifier.SendServerRequestNoResponse(request);
                }
                _trace.Always("BIZMATICA: Fine contestualizzazione");
            }
            catch (Exception exc)
            {
                _trace.Exception(exc, "BIZMATICA: " + portalName + " Errore setCurrentCustomer");
                _notificationService.Notify("Si è verificato un errore durante la contestualizzazione","Errore", NotificationType.Error, new TimeSpan(0, 0, 5));
            }
        }


       
        private void setAppointment(string idOperazionale, string telefono)
        {
            try { 
                var parameters = Utilities.getInstance();
                _trace.Always("BIZMATICA: " + portalName + " setAppointment " + idOperazionale + " " + telefono);

                if (SelectedInteraction != null)
                {
                    _trace.Always("BIZMATICA: " + portalName + " Interazione selezionata " + SelectedInteraction.InteractionId.ToString());

                    SelectedInteraction.SetAttribute("Agente_UserId", _session.UserId);
                    _trace.Always("BIZMATICA: " + portalName + " setCurrentCustomer Agente_UserId " + _session.UserId);

                    SelectedInteraction.SetAttribute("Agente_SMSESSION", parameters.AuthSession);
                    _trace.Always("BIZMATICA: " + portalName + " setCurrentCustomer Agente_SMSESSION\n" + parameters.AuthSession);

                    SelectedInteraction.SetAttribute("Agente_cb_clicktrack_id", parameters.clicktrackId);
                    _trace.Always("BIZMATICA: " + portalName + " setCurrentCustomer Agente_cb_clicktrack_id " + parameters.clicktrackId);
                    
                    if (SelectedInteraction.GetAttribute("idSoggetto").Equals("-1") || SelectedInteraction.GetAttribute("idSoggetto").Equals(""))
                    {
                        SelectedInteraction.SetAttribute("idSoggetto", idOperazionale);
                    }


                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "Contestualizzazione", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { SelectedInteraction.InteractionId };

                    request.Write(parametri);

                    _trace.Always("BIZMATICA: " + portalName + " custom notification setAppointment \n" + parametri[0]);
                    _Notifier.SendServerRequestNoResponse(request);

                }
                else
                {
                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "Contestualizzazione", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { "", idOperazionale, _session.UserId, parameters.AuthSession, parameters.clicktrackId };
                    request.Write(parametri);
                    _trace.Always("BIZMATICA: " + portalName + " custom notification Context interazione solo soggetto\n" + parametri[0]);
                    _Notifier.SendServerRequestNoResponse(request);
                }
                _trace.Always("BIZMATICA: " + portalName + " Fine contestualizzazione per appointment");

                string[] param = new string[2];
                param[0] = idOperazionale;
                param[1] = telefono;
                CBSchedulataThread(param);
            }
            catch(Exception exc)
            {
                _trace.Exception(exc, "BIZMATICA: " + portalName + " Errore setAppointment");
                _notificationService.Notify("Si è verificato un errore durante la presa dell'appuntamento","Errore", NotificationType.Error, new TimeSpan(0, 0, 5));
            }
        }




        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case Win32.WM_COPYDATA:
                    try { 
                        Win32.CopyDataStruct st = (Win32.CopyDataStruct)Marshal.PtrToStructure(m.LParam, typeof(Win32.CopyDataStruct));
                        string strData = Marshal.PtrToStringUni(st.lpData);
                        AsyncManageArgsFromAnotherProcess(strData);
                    }
                    catch (Exception ex)
                    {
                        _notificationService.Notify("Si è verificato un errore.", "Messagging Portali", NotificationType.Error, new TimeSpan(0, 0, 5));
                        _trace.Exception(ex, "BIZMATICA:" + "BIZMATICA:WndProc");
                    }
                    break;
                default:
                    // let the base class deal with it
                    base.WndProc(ref m);
                    break;

            }
        }

        async void AsyncManageArgsFromAnotherProcess(string param)
        {
            await Task.Run(() => ManageArgsFromAnotherProcess(param));
        }
        delegate void delegateManageArgsFromAnotherProcess(string parametri);
        private void ManageArgsFromAnotherProcess(string parametri)
        {
            if (this.InvokeRequired)
            {
                delegateManageArgsFromAnotherProcess d = new delegateManageArgsFromAnotherProcess(ManageArgsFromAnotherProcess);
                this.Invoke(d, new object[] { parametri });
            }
            else
            {
                var parameters = Utilities.getInstance();
                try { 
                    Tracing.TraceAlways(portalName + " WM_COPYDATA ->" + parametri);
                    switch (parametri.Split(':')[0])
                    {
                        case "traceInfo":
                            _notificationService.Notify(parametri.Split(':')[2], parametri.Split(':')[1], NotificationType.Info, new TimeSpan(0, 0, 5));
                            break;
                        case "traceError":
                            _notificationService.Notify(parametri.Split(':')[2], parametri.Split(':')[1], NotificationType.Error, new TimeSpan(0, 0, 15));
                            break;
                        case "click2call":

                            ININ.IceLib.Interactions.CallInteractionParameters parms = new ININ.IceLib.Interactions.CallInteractionParameters(parametri.Split(':')[1], ININ.IceLib.Interactions.CallMadeStage.Allocated);
                            ININ.IceLib.Interactions.Interaction interaction = _interactionManager.MakeCall(parms);
                            Tracing.TraceAlways("CALLID outbound ->" + interaction.CallIdKey);
                            interaction.SetStringAttribute("idSoggetto", parametri.Split(':')[2]);
                            interaction.SetStringAttribute("Agente_UserId", _session.UserId);
                            interaction.SetStringAttribute("Agente_SMSESSION", parameters.AuthSession);
                            interaction.SetStringAttribute("Agente_cb_clicktrack_id", parameters.clicktrackId);
                            _trace.Always("BIZMATICA: click2call Agente_UserId " + _session.UserId);
                            _trace.Always("BIZMATICA: click2call Agente_cb_clicktrack_id " + parameters.clicktrackId);
                            _trace.Always("BIZMATICA: click2call Agente_SMSESSION\n" + parameters.AuthSession);
                            break;
                        case "showInteraction":
                            showRecording(parametri.Split(':')[1]);
                            break;
                        case "disconnectInteraction":
                            MessageBox.Show("disconnectInteraction");
                            break;
                        case "loginInTab":
                            _trace.Always("BIZMATICA: Richiesto nuovo login per " + portalName);
                            Cursor.Current = Cursors.WaitCursor;
                            _notificationService.Notify("Portale " + portalName + " in caricamento", "Autenticazione", NotificationType.Info, new TimeSpan(0, 0, 4));
                            System.Timers.Timer timerLogin = new System.Timers.Timer(200);
                            timerLogin.Elapsed += new System.Timers.ElapsedEventHandler(timerLogin_Elapsed);
                            timerLogin.Start();
                            _trace.Always("BIZMATICA: Richiesto nuovo login partito per " + portalName);

                            break;
                        case "setUnbluEvent":
                            MessageBox.Show(parametri);
                            break;
                        case "setAppointment":
                            setAppointment(parametri.Split(':')[1], parametri.Split(':')[2]);
                            break;
                        case "setCurrentCustomer":
                            setCurrentCustomer(parametri.Split(':')[1]);
                            break;
                        case "unblu":
                            startUnblu(parametri.Split(':')[1]);
                            break;
                        case "click2mail":
                            List<string> param = new List<string>(2);
                            param.Add(parametri.Split(':')[2]);
                            param.Add(parametri.Split(':')[1]);
                            param.Add(parameters.AuthSession);
                            param.Add(parameters.clicktrackId);
                            SceltaEmail(param);
                            break;
                        case "refreshCookie":
                            if (detectCookieChange)
                            {
                                parameters.SMCookieTimestamp = new DateTime(Int64.Parse(parametri.Split(':')[2]));
                                parameters.AuthSession = parametri.Split(':')[3];
                                _trace.Always("BIZMATICA:" + GetConfigurationInfo() + " addin - received refreshCookie \n" + parametri);
                            }
                            break;
                        case "confirmCookie":
                            if (detectCookieChange)
                            {
                                _trace.Always("BIZMATICA:" + GetConfigurationInfo() + " addin - received confirmCookie");
                                parameters.SMCookieTimestamp = new DateTime(Int64.Parse(parametri.Split(':')[2]));
                            }
                            break;
                        default:
                            _trace.Error("BIZMATICA: WM_COPYDATA non riconosciuto\n" + parametri);
                            MessageBox.Show("Non riconosciuto: " + parametri.Split(':')[0]);
                            break;

                    }
                }
                catch (Exception ex)
                {
                    _notificationService.Notify("Si è verificato un errore.", "Messagging Portali", NotificationType.Error, new TimeSpan(0, 0, 5));
                    _trace.Exception(ex, "BIZMATICA:" + "BIZMATICA:WndProc");
                }

            }
        }

        private void startUnblu(string v)
        {
            try
            { 
                if (SelectedInteraction != null)
                {
                    if (SelectedInteraction.GetAttribute("unbluSessionId").Equals(""))
                    {
                        SelectedInteraction.SetAttribute("unbluSessionId", v);
                        SelectedInteraction.SetAttribute("unbluSessionStart", DateTime.Now.ToString("yyyyMMddHHmmss"));

                        CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "UnbluStart", _session.UserId);
                        CustomRequest request = new CustomRequest(header);
                        string[] parametri = new string[] { SelectedInteraction.InteractionId, SelectedInteraction.GetAttribute("WebTools_customerId").Equals("")?SelectedInteraction.GetAttribute("idSoggetto"):SelectedInteraction.GetAttribute("WebTools_customerId"), SelectedInteraction.GetAttribute("contactID"),v};

                        _trace.Always("BIZMATICA:" + "Iniziata session cobrowse " + v + System.Environment.NewLine + String.Join("|", parametri));
                        request.Write(parametri);
                        _Notifier.SendServerRequestNoResponse(request);
                        _trace.Always("BIZMATICA: " + "Session cobrowse notificata");
                    }
                }
            }
            catch (Exception ex)
            {
                _notificationService.Notify("Si è verificato un errore.", "startUnblu", NotificationType.Error, new TimeSpan(0, 0, 5));
                _trace.Exception(ex, "BIZMATICA:" + "startUnblu");
            }
}

        private void SceltaEmail(object param)
        {
            _syncContext.Post(_sceltaEmail, param);
        }
        private void _sceltaEmail(object param)
        {
            try
            {
                var parameters = Utilities.getInstance();
                sceltaEmail popup = new sceltaEmail()
                {
                    SyncContext = _syncContext,
                    workgroups = parameters.workgroupsEmail,
                    Notifier = _Notifier,
                    user = _session.UserId,
                    email = ((List<string>)param)[0],
                    idOperazionale = ((List<string>)param)[1],
                    SMSESSION = ((List<string>)param)[2],
                    CBCLICKTRACKID = ((List<string>)param)[3]
                };
                popup.ShowForm();
            }
            catch (Exception exc)
            {
                _trace.Exception(exc, "BIZMATICA: " + portalName + " Errore SceltaEmail");
                _notificationService.Notify("Si è verificato un errore durante la scelta email", "Errore", NotificationType.Error, new TimeSpan(0, 0, 5));
            }

        }



        #region Interaction Events

        delegate void InteractionAdded(object sender, InteractionEventArgs e);
        private void interactionAdded(object sender, InteractionEventArgs e)
        {

            if (this.InvokeRequired)
            {
                InteractionAdded d = new InteractionAdded(interactionAdded);
                this.Invoke(d, new object[] { sender, e });
            }
            else
            {

                try
                {

                    _trace.Always("BIZMATICA:" + portalName + " Interaction added: " + e.Interaction.InteractionId);

                    string status = e.Interaction.GetAttribute(InteractionAttributes.State);
                    string media = e.Interaction.GetAttribute(InteractionAttributes.InteractionType);
                    string direction = e.Interaction.GetAttribute(InteractionAttributes.Direction);
                    string EsitazioneNecessaria = e.Interaction.GetAttribute("EsitazioneNecessaria");
                    _trace.Always("BIZMATICA:" + portalName + " Interaction " + e.Interaction.InteractionId + " status: " + status);
                    _trace.Always("BIZMATICA:" + portalName + " Interaction " + e.Interaction.InteractionId + " media: " + media);
                    _trace.Always("BIZMATICA:" + portalName + " Interaction " + e.Interaction.InteractionId + " direction: " + direction);
                    _trace.Always("BIZMATICA:" + portalName + " Interaction " + e.Interaction.InteractionId + " EsitazioneNecessaria: " + EsitazioneNecessaria);

                    _trace.Always("BIZMATICA:" + portalName + " Interaction added " + e.Interaction.InteractionId + " : " + status+ " "+ media + " " + direction);

                    lock (_interactionsListLock)
                    {
                        if (!_interactions.ContainsKey(e.Interaction.InteractionId)) _interactions.Add(e.Interaction.InteractionId, e.Interaction.GetAttribute(InteractionAttributes.State));
                    }

                    _trace.Always("BIZMATICA:" + portalName + ": -E->" + EsitazioneNecessaria + " -S-> " + e.Interaction.GetAttribute(InteractionAttributes.State) + " -pS-> " + (_interactions.ContainsKey(e.Interaction.InteractionId) ? _interactions[e.Interaction.InteractionId] : "") + " -D-> " + e.Interaction.GetAttribute(InteractionAttributes.Direction) + " -W-> " + e.Interaction.GetAttribute("Eic_AcdWorkgroup"));

                    if (EsitazioneNecessaria.Equals("1") && e.Interaction.GetAttribute(InteractionAttributes.State).Equals( InteractionAttributeValues.State.Connected))
                    {
                        if (e.Interaction.GetAttribute(InteractionAttributes.Direction).Equals(InteractionAttributeValues.Direction.Incoming))// && !e.Interaction.GetAttribute("Eic_AcdWorkgroup").Equals(""))
                        {
                            _trace.Always("BIZMATICA:" + portalName + " checking contextualization");

                            if (confError)
                            {
                                _trace.Error("BIZMATICA:" + portalName + " skipping because of confError");
                                return;
                            }
                            string contextUrl = InteractionUrl;
                            

                            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                            UriBuilder uri = new UriBuilder(codeBase);
                            contextUrl = InteractionUrl.Replace("http://localhost/", uri.Uri.AbsoluteUri.Replace("Bizmatica" + GetConfigurationInfo() + ".dll", ""));

                            _trace.Always("BIZMATICA:" + portalName + " Interaction Url: " + contextUrl);

                            string channel = "";
                            if (e.Interaction.GetAttribute("WebTools_Videochat").Equals("1") || e.Interaction.GetAttribute("Videochat").Equals("1"))
                            {
                                channel = "video";
                            }
                            else if (e.Interaction.GetAttribute("IOX").Equals("1"))
                            {
                                channel = "iox";
                            }
                            else if (media.Equals("Email Interaction"))
                            {
                                channel = "email";
                            }
                            else if (media.Equals("Call"))
                            {
                                channel = "call";
                            }
                            else if (media.Equals("Chat"))
                            {
                                channel = "chat";
                            }
                            else if (media.Equals("Callback"))
                            {
                                channel = "callback";
                            }

                            _trace.Always("BIZMATICA:" + portalName + " Interaction channel: " + channel);



                            if (RefreshableChannels.Contains(channel) && !(String.IsNullOrEmpty(InteractionUrl) || String.IsNullOrWhiteSpace(InteractionUrl)))
                            {

                                for (int i = 0; i < Regex.Matches(InteractionUrl, @"\$\(([^)]*)\)").Count; i++)
                                {
                                    string attribute = Regex.Matches(InteractionUrl, @"\$\(([^)]*)\)")[i].Groups[1].ToString();
                                    string value = e.Interaction.GetAttribute(Regex.Matches(InteractionUrl, @"\$\(([^)]*)\)")[i].Groups[1].ToString());
                                    _trace.Always("BIZMATICA: " + portalName + " Attribute $(" + attribute + "): " + value);
                                    if (!channel.Equals("video")) value = System.Net.WebUtility.UrlEncode(value);
                                    contextUrl = contextUrl.Replace(Regex.Matches(InteractionUrl, @"\$\(([^)]*)\)")[i].Groups[0].ToString(), value);
                                }


                                _trace.Always("BIZMATICA:" + portalName + " Navigating to: " + contextUrl);

                                if (!externalBrowserEnabled)
                                {
                                    webBrowser1.Navigate(contextUrl);
                                }
                                else
                                {
                                    Win32.SendArgs((IntPtr)hwnd, contextUrl);
                                }
                            }
                            else
                            {
                                _trace.Always("BIZMATICA: " + portalName + " Context Url is empty or channel rule prevent contextualizing. Doing nothing");
                            }
                        }

                    }


                }
                catch (Exception ex)
                {
                    if (_trace != null) _trace.Exception(ex, "BIZMATICA: Exception in interactionAdded");
                    if (_notificationService != null) _notificationService.Notify("Errore nell'esecuzione di interactionAdded", "Contact administrator", NotificationType.Error, new TimeSpan(0, 0, 10));

                }

            }

        }





        delegate void InteractionChanged(object sender, InteractionEventArgs e);
        private void interactionChanged(object sender, InteractionEventArgs e)
        {

            if (this.InvokeRequired)
            {
                InteractionChanged d = new InteractionChanged(interactionChanged);
                this.Invoke(d, new object[] { sender, e });
            }
            else
            {
                try
                {


                    string status = e.Interaction.GetAttribute(InteractionAttributes.State);
                    string media = e.Interaction.GetAttribute(InteractionAttributes.InteractionType);
                    _trace.Always("BIZMATICA:" + portalName + " Interaction changed " + e.Interaction.InteractionId + " status: " + status + " media: " + media);

                    string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                    UriBuilder uri = new UriBuilder(codeBase);
                    InteractionUrl = InteractionUrl.Replace("http://localhost/", uri.Uri.AbsoluteUri.Replace("Bizmatica" + GetConfigurationInfo() + ".dll", ""));
                    _trace.Always("BIZMATICA:" + portalName + " InteractionUrl: " + InteractionUrl);

                    string channel = "";
                    if (e.Interaction.GetAttribute("WebTools_Videochat").Equals("1") || e.Interaction.GetAttribute("Videochat").Equals("1"))
                    {
                        channel = "video";
                    }
                    else if (e.Interaction.GetAttribute("IOX").Equals("1"))
                    {
                        channel = "iox";
                    }
                    else if (media.Equals("Email Interaction"))
                    {
                        channel = "email";
                    }
                    else if (media.Equals("Call"))
                    {
                        channel = "call";
                    }
                    else if (media.Equals("Chat"))
                    {
                        channel = "chat";
                    }
                    else if (media.Equals("Callback"))
                    {
                        channel = "callback";
                    }
                    _trace.Always("BIZMATICA:" + portalName + " channel: " + channel);


                    string EsitazioneNecessaria = e.Interaction.GetAttribute("EsitazioneNecessaria");

                    _trace.Always("BIZMATICA:" + portalName + ": -E->" + EsitazioneNecessaria + " -S-> " + e.Interaction.GetAttribute(InteractionAttributes.State) + " -pS-> " + (_interactions.ContainsKey(e.Interaction.InteractionId)?_interactions[e.Interaction.InteractionId]:"") + " -D-> " + e.Interaction.GetAttribute(InteractionAttributes.Direction) + " -W-> " + e.Interaction.GetAttribute("Eic_AcdWorkgroup"));
                    //System.Diagnostics.Debug.WriteLine(GetConfigurationInfo()+ ": -E->" + e.Interaction.GetAttribute("EsitazioneNecessaria") + " -S-> " + e.Interaction.GetAttribute(InteractionAttributes.State) + " -pS-> " + (_interactions.ContainsKey(e.Interaction.InteractionId) ? _interactions[e.Interaction.InteractionId] : "") + " -D-> " + e.Interaction.GetAttribute(InteractionAttributes.Direction) + " -W-> " + e.Interaction.GetAttribute("Eic_AcdWorkgroup"));

                    if (EsitazioneNecessaria.Equals("1") && e.Interaction.GetAttribute(InteractionAttributes.State).Equals(InteractionAttributeValues.State.Connected) && (_interactions[e.Interaction.InteractionId].Equals("A") || _interactions[e.Interaction.InteractionId].Equals("O") || _interactions[e.Interaction.InteractionId].Equals("P"))) {
                        lock (_interactionsListLock)
                        {
                            SelectedInteraction = e.Interaction;
                        }
                        if (e.Interaction.GetAttribute(InteractionAttributes.Direction).Equals(InteractionAttributeValues.Direction.Incoming) || (e.Interaction.GetAttribute(InteractionAttributes.Direction).Equals(InteractionAttributeValues.Direction.Outgoing) && e.Interaction.GetAttribute("authLevel").Equals("4")))// && !e.Interaction.GetAttribute("Eic_AcdWorkgroup").Equals(""))
                        {

                            if (confError)
                            {
                                _trace.Error("BIZMATICA:" + portalName + " skipping because of confError");
                                return;
                            }
                            _trace.Always("BIZMATICA:" + portalName + " processing contextualization");
                            string contextUrl = InteractionUrl;

                            if (channel.Equals("video")) e.Interaction.SetAttribute("Vidyo_Member", _session.DisplayName);
                            if (RefreshableChannels.Contains(channel) && !(String.IsNullOrEmpty(InteractionUrl) || String.IsNullOrWhiteSpace(InteractionUrl)))
                            {

                                for (int i = 0; i < Regex.Matches(InteractionUrl, @"\$\(([^)]*)\)").Count; i++)
                                {
                                    string attribute = Regex.Matches(InteractionUrl, @"\$\(([^)]*)\)")[i].Groups[1].ToString();
                                    string value = e.Interaction.GetAttribute(Regex.Matches(InteractionUrl, @"\$\(([^)]*)\)")[i].Groups[1].ToString());
                                    _trace.Always("BIZMATICA:" + portalName + " Attribute $(" + attribute + "): " + value);
                                    if (!channel.Equals("video")) value = System.Net.WebUtility.UrlEncode(value);
                                    contextUrl = contextUrl.Replace(Regex.Matches(InteractionUrl, @"\$\(([^)]*)\)")[i].Groups[0].ToString(), value);
                                }
                                //System.Diagnostics.Debug.WriteLine("Contestualizzazione " + GetConfigurationInfo() + " navigating to: " + contextUrl);
                                _trace.Always("BIZMATICA:" + portalName + " Navigating to: " + contextUrl);
                                _notificationService.Notify("caricamento " + GetConfigurationInfo() + " in corso", "", NotificationType.Info, TimeSpan.FromSeconds(3));

                                if (channel.Equals("video"))  e.Interaction.SetAttribute("Vidyo_Member", _session.DisplayName);
                                
                                if (!externalBrowserEnabled)
                                {
                                    if(webBrowser1!=null) webBrowser1.Navigate(contextUrl);
                                }
                                else
                                {
                                    Win32.SendArgs((IntPtr)hwnd, contextUrl);
                                }
                            }
                            else
                            {
                                _trace.Always("BIZMATICA:" + portalName + " Context Url is empty or channel rule not applied. Doing nothing");
                            }
                        }
                        
                    }

                    if (portalName.Equals("Vidyo") && (e.Interaction.GetAttribute("WebTools_Videochat").Equals("1") || e.Interaction.GetAttribute("Videochat").Equals("1")) &&(e.Interaction.GetAttribute(InteractionAttributes.State).Equals(InteractionAttributeValues.State.ExternalDisconnect) || e.Interaction.GetAttribute(InteractionAttributes.State).Equals(InteractionAttributeValues.State.InternalDisconnect)) )
                    {
                        webBrowser1.Navigate(StartingUrl);
                    }

                    lock (_interactionsListLock)
                    {
                        if (_interactions.ContainsKey(e.Interaction.InteractionId))
                        {
                            _interactions[e.Interaction.InteractionId] = e.Interaction.GetAttribute(InteractionAttributes.State);
                        }
                        else
                        {
                            _interactions.Add(e.Interaction.InteractionId, e.Interaction.GetAttribute(InteractionAttributes.State));
                        }
                    }


                }
                catch (Exception ex)
                {
                    if (_trace != null) _trace.Exception(ex, "BIZMATICA:" + portalName + " Exception in interactionChanged");
                    if (_notificationService != null) _notificationService.Notify("Errore nell'esecuzione di interactionChanged", "Contact administrator", NotificationType.Error, new TimeSpan(0, 0, 10));
                }
            }
        }



        delegate void InteractionRemoved(object sender, InteractionEventArgs e);
        private void interactionRemoved(object sender, InteractionEventArgs e)
        {
            if (this.InvokeRequired)
            {
                InteractionRemoved d = new InteractionRemoved(interactionRemoved);
                this.Invoke(d, new object[] { sender, e });
            }
            else
            {
                try
                {
                    lock (_interactionsListLock)
                    {
                        if (_interactions.ContainsKey(e.Interaction.InteractionId)) _interactions.Remove(e.Interaction.InteractionId);
                    }
                }
                catch (Exception ex)
                {
                    if (_trace != null) _trace.Exception(ex, "BIZMATICA:" + portalName + " Exception in interactionRemoved");
                    if (_notificationService != null) _notificationService.Notify("Errore nell'esecuzione di interactionRemoved", "Contact administrator", NotificationType.Error, new TimeSpan(0, 0, 5));
                }


            }

        }



        private void onSelectedInteractionChanged(object sender, EventArgs e)
        {
            try
            {
                lock (_interactionsListLock)
                {
                    if (sender == null)
                    {
                        SelectedInteraction = null;
                        return;
                    }

                    else
                    {
                        SelectedInteraction = ((IInteractionSelector)sender).SelectedInteraction;
                    }
                }
            }
            catch (Exception EX)
            {
                _notificationService.Notify("Si è verificato un errore contattare l'amministratore.", "SelectedInteractionChanged", NotificationType.Warning, new TimeSpan(0, 0, 20));
                _trace.Exception(EX, "BIZMATICA: " + portalName + " Errore durante la selezione dell'interazione");
                SelectedInteraction = null;
                return;
            }
        }
        #endregion

        internal AddinGeneric(IServiceProvider serviceProvider)
        {
            
            InitializeComponent();

            var parameters = Utilities.getInstance();
            try
            {
                portalName = GetConfigurationInfo();

                _syncContext = SynchronizationContext.Current;

                //per generare log visibile col log viewer di Interactive
                _trace = serviceProvider.GetService(typeof(ITraceContext)) as ITraceContext;

                //recupero versione addin
                Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                _trace.Always("BIZMATICA:" + portalName + " addin loading: " + FileVersionInfo.GetVersionInfo(assembly.Location).FileVersion);

                //recupero la sessione utente dall'addin per generare una seesion IceLib
                _session = (Session)serviceProvider.GetService(typeof(Session));
                _session.ConnectionStateChanged += _session_ConnectionStateChanged;

                //gestore di funzionalità ICELIB per generare chiamate in uscita
                _interactionManager = ININ.IceLib.Interactions.InteractionsManager.GetInstance(_session);
                _transactionClient = new TransactionClient(_session);
                _configurationManager = ConfigurationManager.GetInstance(_session);
                
                _Notifier = new CustomNotification(_session);


                this.webBrowser1.ObjectForScripting = new WebBrowserIntegrationObject(serviceProvider);
                 
                //inizializzazione evento di selezione di interazione dal client
                IInteractionSelector selectInteractionEvent = (IInteractionSelector)serviceProvider.GetService(typeof(IInteractionSelector));
                selectInteractionEvent.SelectedInteractionChanged += new EventHandler(onSelectedInteractionChanged);
                _notificationService = (INotificationService)serviceProvider.GetService(typeof(INotificationService));



                _QualityManagementManager = QualityManagementManager.GetInstance(_session);
                _RecordingsManager = _QualityManagementManager.RecordingsManager;

                //_Notifier = new CustomNotification(_session);
                _trace.Always("BIZMATICA:" + portalName + " inizializzati oggetti CIC");

                //Eseguo un monitor della coda senza utilizzare la classe QueueMonitor
                IQueueService iQs = (IQueueService)serviceProvider.GetService(typeof(IQueueService));
                IQueue iQ = iQs.GetQueue(iQs.MyInteractionsQueueName, new string[] { "Eic_callidkey", InteractionAttributes.State, InteractionAttributes.InteractionType });
                iQ.InteractionAdded += new EventHandler<InteractionEventArgs>(interactionAdded);
                iQ.InteractionChanged += new EventHandler<InteractionEventArgs>(interactionChanged);
                iQ.InteractionRemoved += new EventHandler<InteractionEventArgs>(interactionRemoved);

                _trace.Always("BIZMATICA:" + portalName + " addin interaction handlers configurated");


                try { StartingUrl = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("StartingUrl")).Values[0]; } catch (Exception) { _trace.Error("BIZMATICA:" + portalName + " StartingUrl missing"); };
                try { InteractionUrl = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("InteractionUrl")).Values[0]; } catch (Exception) { _trace.Always(portalName + " InteractionUrl not existing"); };
                try { Version = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("Version")).Values[0]; } catch (Exception) { _trace.Error("BIZMATICA:" + portalName + " Version missing"); };
                try { RefreshableChannels = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("RefreshableChannels")).Values[0]; } catch (Exception) { _trace.Always(portalName + " RefreshableChannels missing"); };
                try { externalBrowserEnabled = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("externalBrowser")).Values[0].Equals("1"); } catch (Exception) { _trace.Error("BIZMATICA:" + portalName + " externalBrowser missing"); };
                //try { detectCookieChange = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("detectCookieChange")).Values[0].Equals("1"); if (detectCookieChange) { timer1.Interval = BizmaticaAddin.AddinQM.PollingTimeout * 1000; timer1.Enabled = true; } } catch (Exception) { _trace.Error("BIZMATICA:" + portalName + " detectCookieChange missing"); };
                try { downloadUrl = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("downloadUrl")).Values[0]; } catch (Exception) { _trace.Warning("BIZMATICA:" + portalName + " downloadUrl missing"); };
                try { resourceForAuth = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("resourceForAuth")).Values[0]; } catch (Exception) { _trace.Warning("BIZMATICA:" + portalName + " resourceForAuth missing"); };
                try { loginInTab = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("loginInTab")).Values[0]; } catch (Exception) { _trace.Warning("BIZMATICA:" + portalName + " loginInTab missing"); };
                try { toolIcon = parameters.structureParameters.Single(x => x.ConfigurationId.Id.Equals(portalName)).Parameters.Value.Single(x => x.Name.Equals("toolIcon")).Values[0]; } catch (Exception) { _trace.Warning("BIZMATICA:" + portalName + " toolIcon missing"); };
                
                _trace.Always("BIZMATICA:" + portalName + " structureParameters configurated:"+ String.Join(System.Environment.NewLine, "StartingUrl: " + StartingUrl, "InteractionUrl: " + InteractionUrl, "Version: " + Version, "RefreshableChannels: " + RefreshableChannels, "externalBrowserEnabled: " + externalBrowserEnabled, "detectCookieChange: " + detectCookieChange, "downloadUrl: " + downloadUrl, "resourceForAuth: " + resourceForAuth, "loginInTab: " + loginInTab, "toolIcon: " + toolIcon));

                if (Version.CompareTo(FileVersionInfo.GetVersionInfo(assembly.Location).FileVersion) != 0)
                {
                    _notificationService.Notify(portalName + " addin version is different from server", "Contact administrator", NotificationType.Warning, new TimeSpan(0, 0, 15));
                    _trace.Error("BIZMATICA:" + portalName + " addin version is different from server");
                }

                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                StartingUrl = StartingUrl.Replace("http://localhost/", uri.Uri.AbsoluteUri.Replace("Bizmatica" + GetConfigurationInfo() + ".dll", ""));

                _trace.Always("BIZMATICA:" + portalName + " StartingUrl Check");

                if (String.IsNullOrEmpty(StartingUrl) || String.IsNullOrWhiteSpace(StartingUrl)) {
                    _notificationService.Notify(portalName + " StartingUrl parameter is missing in " + portalName + " configuration", "Contact administrator", NotificationType.Warning, new TimeSpan(0, 1, 0));
                    throw new Exception("BIZMATICA:" + portalName + " StartingUrl is missing");
                }
                


                _trace.Always("BIZMATICA:" + portalName + " parameter StartingUrl: " + StartingUrl);


                if (!externalBrowserEnabled) { 
                    _trace.Always("BIZMATICA:" + portalName + " using StartingUrl");
                    if (!(String.IsNullOrEmpty(StartingUrl) || String.IsNullOrWhiteSpace(StartingUrl)))
                    {

                        if (!String.IsNullOrEmpty(parameters.AuthSession))
                        { 
                            Win32.InternetSetCookie(StartingUrl, parameters.SMCookie, parameters.AuthSession);
                            _trace.Always("BIZMATICA:" + portalName + " " + parameters.SMCookie +": " + parameters.AuthSession);
                        }
                        if (!String.IsNullOrEmpty(parameters.unbluCookieValue))
                        {
                            Win32.InternetSetCookie(StartingUrl, parameters.unbluCookie, parameters.unbluCookieValue);
                            _trace.Always("BIZMATICA:" + portalName + " " + parameters.unbluCookie + ": " + parameters.unbluCookieValue);
                        }
                        _trace.Always("BIZMATICA:" + portalName + " start navigating to startingUrl");
                        webBrowser1.Navigate(StartingUrl);
                        _trace.Always("BIZMATICA:" + portalName + " initialization completed");
                    }
                }

                else
                {
                    webBrowser1.Visible = false;
                    _trace.Always("BIZMATICA:" + portalName + " Starting process " + AppDomain.CurrentDomain.BaseDirectory + @"Addins\Portali\" + portalName + ".exe");
                    var chromeDriverProcesses = Process.GetProcesses().Where(pr => pr.ProcessName.StartsWith(portalName));
                    foreach (var process in chromeDriverProcesses)
                    {
                        process.Kill();
                        _trace.Always("BIZMATICA:" + portalName + " Killing previous process " + process.Id.ToString());
                    }

                    if (parameters.tabList.ContainsKey(portalName)) parameters.tabList.Remove(portalName);
                    if (parameters.tabs.Contains(hwnd)) parameters.tabs.Remove(hwnd);

                    psi = new ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory + @"Addins\Portali\" + portalName + ".exe");


                    externalBrowser.StartInfo = psi;
                    externalBrowser.Start();

                    while (hwnd == IntPtr.Zero)
                    {
                        externalBrowser.WaitForInputIdle(1000); //wait for the window to be ready for input;
                        externalBrowser.Refresh();              //update process info
                        if (externalBrowser.HasExited)
                        {
                            return; //abort if the process finished before we got a handle.
                        }
                        hwnd = externalBrowser.MainWindowHandle; ;  //cache the window handle
                    }
                    parameters.tabs.Add(hwnd);
                    parameters.tabList.Add(portalName, hwnd);
                    _trace.Always("BIZMATICA:" + portalName + " Aggiunto processo " + portalName + ".exe con (" + hwnd.ToString() + ")");

                    Win32.SetParent(hwnd, this.Handle);
                    this.SizeChanged += new EventHandler(Addin_Resize);
                    Addin_Resize(new Object(), new EventArgs());


                    _trace.Always("BIZMATICA:" + portalName + " Inizio SendArgs " + portalName);
                    if (!String.IsNullOrEmpty(parameters.AuthSession))
                    {
                        Win32.SendArgs((IntPtr)hwnd, "credentials:" + parameters.SMCookie + ":" + parameters.AuthSession);
                    }
                    Win32.SendArgs((IntPtr)hwnd, "additionalCookie:" + parameters.unbluCookie + ":" + parameters.unbluCookieValue);
                    Win32.SendArgs((IntPtr)hwnd, "downloadUrl:" + downloadUrl);
                    Win32.SendArgs((IntPtr)hwnd, "clicktrackId:clicktrackId:" + parameters.clicktrackId);
                    Win32.SendArgs((IntPtr)hwnd, "resourceForAuth:" + resourceForAuth);
                    Win32.SendArgs((IntPtr)hwnd, "loginInTab:" + loginInTab);
                    Win32.SendArgs((IntPtr)hwnd, "toolIcon:" + toolIcon);
                    Win32.SendArgs((IntPtr)hwnd, StartingUrl);
                    _trace.Always("BIZMATICA:" + portalName + " Fine SendArgs " + portalName);
                }
            }
            catch (Exception ex)
            {
                if (_trace!=null) _trace.Exception(ex, "BIZMATICA:" + portalName + " Exception in " + portalName + " initialization");
                if (_notificationService != null) _notificationService.Notify("Errore inizializzazione portale " + portalName + ". ", "Contact administrator", NotificationType.Error, new TimeSpan(0, 1, 0));
            }



        }

        private void _session_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            if(e.State==ININ.IceLib.Connection.ConnectionState.Down)
            {
                if (_trace != null) _trace.Warning("BIZMATICA:" + portalName + " Detected disconnection");
            }

        }


        private void Addin_Resize(object sender, EventArgs e)
        {
            Win32.SendArgs((IntPtr)hwnd, "resizeTo:" + this.Width.ToString() + "," + this.Height.ToString());
        }


        public object Content
        {
            get { return this; }
        }
        public string Title
        {
            get { return GetConfigurationInfo(); }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaiseChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

 


        private string GetConfigurationInfo()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            Attribute att = AssemblyConfigurationAttribute.GetCustomAttribute(asm, typeof(AssemblyConfigurationAttribute));
            AssemblyConfigurationAttribute confAtt = att as AssemblyConfigurationAttribute;
            return confAtt.Configuration;
        }



 


        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //Tiziano Disabilitata
            /*
            if (detectCookieChange)
            {
                BizmaticaAddin.AddinQM.SMCookieTimestamp = DateTime.Now;
                string tempVal= Win32.WebBrowserEx.GetCookieInternal(e.Url, BizmaticaAddin.AddinQM.SMCookie);
                if (!BizmaticaAddin.AddinQM.AuthSession.Equals(tempVal))
                {
                    BizmaticaAddin.AddinQM.AuthSession = tempVal;
                }
            }*/
        }



        private void CBSchedulataThread(object param)
        {
            _syncContext.Post(CBSchedulataStart, param);
        }

        private void CBSchedulataStart(object param)
        {

            CBSchedulata CBSchedulataForm = new CBSchedulata()
            {

                SyncContext = _syncContext,

                _session = _session,
                _trace = _trace,
                _Notifier = _Notifier,
                telefono = ((string[])param)[1],
                idSoggetto = ((string[])param)[0]


            };
            CBSchedulataForm.ShowForm();
        }


        private void CallRegistrazioneOnUIThread(object recordings)
        {

            _syncContext.Post(CallRegistrazioneForm, recordings);
        }
        private void CallRegistrazioneForm(object _recordings)
        {

            RecorderViewer registrazione = new RecorderViewer()
            {
                recording = ((string[])_recordings)[0],
                numAttachment = ((string[])_recordings)[1],
                media = ((string[])_recordings)[2],
                callId = ((string[])_recordings)[3],
                SyncContext = _syncContext,
                Trace = _trace,
                NotificationService = _notificationService,
                recordingsManager= _RecordingsManager

            };
            registrazione.ShowForm();
        }


        private void CallRegistrazioneCallOnUIThread(object recordings)
        {

            _syncContext.Post(CallRegistrazioneCallForm, recordings);
        }
        private void CallRegistrazioneCallForm(object _recordings)
        {

            RecorderCall registrazione = new RecorderCall()
            {
                recording = ((string[])_recordings),
                SyncContext = _syncContext,
                Trace = _trace,
                NotificationService = _notificationService,
                recordingsManager = _RecordingsManager

            };
            registrazione.ShowForm();
        }


        public void showRecording(string callId)
        {
            _trace.Always("BIZMATICA:" + portalName + " showRecording");
            //callId = "200065091620170104";
            TransactionData transactionData = new TransactionData();
            transactionData.ProcedureName = RECORDING_STORED_PROCEDURE;

            TransactionParameter parCallId = new TransactionParameter();
            parCallId.Type = ParameterType.In;
            parCallId.DataType = ParameterDataType.String;
            parCallId.Value = callId;
            transactionData.Parameters.Add(parCallId);

            bool done = false;
            try
            {
                done = _transactionClient.Execute(ref transactionData);
            }
            catch (Exception ex)
            {
                _trace.Exception(ex, "BIZMATICA:" + portalName + " Exception in getting recordingId");
                _notificationService.Notify("Non è stato possibile recuperare la registrazione.", "Errore", NotificationType.Error, TimeSpan.FromSeconds(5));
                return;
            }

            string[] recordingId;
            string[] numAttachment;
            string[] media;


            if (done)
            {
                try
                {
                    // get result
                    DataSet ds = transactionData.ResultSet;
                    if (ds.Tables[1].Rows.Count == 0)
                    {
                        _notificationService.Notify("Non è stato possibile recuperare la registrazione.", "Avviso", NotificationType.Warning, TimeSpan.FromSeconds(5));
                        return;
                    }
                    recordingId = new string[ds.Tables[1].Rows.Count - 1];
                    numAttachment = new string[ds.Tables[1].Rows.Count - 1];
                    media = new string[ds.Tables[1].Rows.Count - 1];

                    for (int x = 1; x < ds.Tables[1].Rows.Count; x++)
                    {
                        try
                        {
                            recordingId[x-1] = ds.Tables[1].Rows[x].ItemArray[0].ToString();  
                            media[x - 1] = ds.Tables[1].Rows[x].ItemArray[2].ToString(); 
                            numAttachment[x - 1] = ds.Tables[1].Rows[x].ItemArray[3].ToString(); 
                        }
                        catch (Exception ex)
                        {
                            _trace.Exception(ex, "BIZMATICA:" + portalName + " Exception in getting recordingId from tables[1]");
                            _notificationService.Notify("Non è stato possibile recuperare la registrazione.", "Errore", NotificationType.Error, TimeSpan.FromSeconds(5));
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _trace.Exception(ex, "BIZMATICA:" + portalName + " Exception in getting recordingId");
                    _notificationService.Notify("Non è stato possibile recuperare la registrazione.", "Errore", NotificationType.Error, TimeSpan.FromSeconds(5));
                    return;
                }

            }
            else
            {
                _notificationService.Notify("Non è stato possibile recuperare la registrazione.", "Errore", NotificationType.Error, TimeSpan.FromSeconds(5));
                return;
            }

            try
            {
                string[] recparams = new string[4];
                if (!media[0].Equals("1")) { 
                
                    recparams[0] = recordingId[0];
                    recparams[1] = numAttachment[0];
                    recparams[2] = media[0];
                    recparams[3] = callId;
                    CallRegistrazioneOnUIThread(recparams);
                }
                else
                {
                    CallRegistrazioneCallOnUIThread(recordingId);
                }
                
            }
            catch (Exception ex)
            {
                _trace.Exception(ex, "BIZMATICA:" + portalName + " Show recording error");
                _notificationService.Notify("Non è stato possibile recuperare la registrazione.", "Errore", NotificationType.Error, TimeSpan.FromSeconds(5));
            }

        }

        delegate void SetCallback();
        public void Relogin()
        {
            if (this.InvokeRequired)
            {
                SetCallback d = new SetCallback(Relogin);
                this.BeginInvoke(d, new object[] { });
            }
            else
            {
                var parameters = Utilities.getInstance();
                _trace.Always("BIZMATICA:" + portalName + " SetCallback");
                Win32.SendArgs((IntPtr)hwnd, "credentials:" + parameters.SMCookie + ":" + parameters.AuthSession);
                Win32.SendArgs((IntPtr)hwnd, "clicktrackId:clicktrackId:" + parameters.clicktrackId);
                Win32.SendArgs((IntPtr)hwnd, StartingUrl);
            }
        }
        private void timerLogin_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var parameters = Utilities.getInstance();
            ((System.Timers.Timer)sender).Stop();
            var credentials = Win32.ContactServlet(parameters.AuthUrl, parameters.UserName, parameters.Password, parameters.Domain, "", parameters.AuthCookieName);

            Cursor.Current = Cursors.Default;
            if (String.IsNullOrEmpty(credentials.SMSESSION))
            {
                _trace.Always("BIZMATICA:" + portalName + " Si è verificato un problema durante il relogin su " + portalName);
                _notificationService.Notify("Si è verificato un problema durante il login", "Errore Autenticazione", NotificationType.Warning, new TimeSpan(0, 0, 10));
                return;
            }

            parameters.AuthSession = credentials.SMSESSION;
            parameters.clicktrackId = credentials.cbclicktrackId;

            Relogin();


        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            var parameters = Utilities.getInstance();
            timer2.Enabled = false;
            var credentials = Win32.ContactServlet(parameters.AuthUrl, parameters.UserName, parameters.Password, parameters.Domain, "", parameters.AuthCookieName);
            Cursor.Current = Cursors.Default;
            if (String.IsNullOrEmpty(credentials.SMSESSION))
            {
                _notificationService.Notify("Si è verificato un problema durante il login", "Errore Autenticazione", NotificationType.Warning, new TimeSpan(0, 0, 10));
                return;
            }

            parameters.AuthSession = credentials.SMSESSION;
            parameters.clicktrackId = credentials.cbclicktrackId;

            if (!String.IsNullOrEmpty(parameters.AuthSession))
            {
                Win32.SendArgs((IntPtr)hwnd, "credentials:" + parameters.SMCookie + ":" + parameters.AuthSession);
            }
            Win32.SendArgs((IntPtr)hwnd, "clicktrackId:clicktrackId:" + parameters.clicktrackId);
            Win32.SendArgs((IntPtr)hwnd, StartingUrl);
        }
    }


    public class AddInW : AddInWindow
    {
        protected override string CategoryId
        {
            get { return "CheBanca"; }
        }
        protected override string CategoryDisplayName
        {
            get { return "CheBanca Addins"; }
        }
        protected override string Id
        {
            get { return "CheBanca" + GetConfigurationInfo(); }
        }
        protected override string DisplayName
        {
            get { return GetConfigurationInfo(); }
        }
        public override object Content
        {
            get
            {
                return new AddinGeneric(ServiceProvider);
            }
        }

        private string GetConfigurationInfo()
        {
            Assembly asm = Assembly.GetExecutingAssembly();

            Attribute att = AssemblyConfigurationAttribute.GetCustomAttribute(asm, typeof(AssemblyConfigurationAttribute));
            AssemblyConfigurationAttribute confAtt = att as AssemblyConfigurationAttribute;

            return confAtt.Configuration;
        }
    }


    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IApp
    {
        void click2call(string phone, string idOperazionale);

        void click2mail(string email,string idOperazionale);

        void showInteraction(string interactionId);

        void disconnectInteraction(string interactionId);

        void setCurrentCustomer(string idOperazionale);
    }

    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IWebBrowserIntegration
    {
        IApp CIC { get; }
    }

    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    [ComDefaultInterface(typeof(IWebBrowserIntegration))]
    public class WebBrowserIntegrationObject : IWebBrowserIntegration
    {
        private IServiceProvider _serviceProvider;
        private CIC _app
        {
            get
            {
                return new CIC(_serviceProvider);
            }
        }


        public WebBrowserIntegrationObject(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IApp CIC
        {
            get { return _app; }
        }

    }

    [ComVisible(true)]
    public class CIC : IApp
    {


        private Session _session;
        private ITraceContext _trace;
        private CustomNotification _Notifier;
        private IInteraction SelectedInteraction;
        private INotificationService _notificationService;
        private ININ.IceLib.Interactions.InteractionsManager _interactionManager;

        public CIC(IServiceProvider serviceProvider)
        {
            try
            { 
                _trace = serviceProvider.GetService(typeof(ITraceContext)) as ITraceContext;
                _session = serviceProvider.GetService(typeof(Session)) as Session;
                _interactionManager = ININ.IceLib.Interactions.InteractionsManager.GetInstance(_session);
                _Notifier = new CustomNotification(_session);
                _notificationService = (INotificationService)serviceProvider.GetService(typeof(INotificationService));
                IInteractionSelector selectInteractionEvent = (IInteractionSelector)serviceProvider.GetService(typeof(IInteractionSelector));
                selectInteractionEvent.SelectedInteractionChanged += new EventHandler(onSelectedInteractionChanged);
            }
            catch(Exception exc)
            {
                _trace.Exception(exc, "BIZMATICA: Errore Inizializzazione CIC(IServiceProvider serviceProvider)");
                _notificationService.Notify("Si è verificato un errore durante l'inizializzazione","Errore", NotificationType.Error, new TimeSpan(0, 0, 30));
            }
}

        private void onSelectedInteractionChanged(object sender, EventArgs e)
        {
            try
            {
                if (sender == null)
                {
                    SelectedInteraction = null;
                    return;
                }

                else
                {
                    SelectedInteraction = ((IInteractionSelector)sender).SelectedInteraction;
                }
            }
            catch (Exception EX)
            {
                _notificationService.Notify("Si è verificato un errore contattare l'amministratore.", "SelectedInteractionChanged", NotificationType.Warning, new TimeSpan(0, 0, 20));
                _trace.Exception(EX, "Errore durante la selezione dell'interazione");
                SelectedInteraction = null;
                return;
            }
        }


        public void click2call(string phone, string idOperazionale)
        {
            var parameters = Utilities.getInstance();
            try { 
                ININ.IceLib.Interactions.CallInteractionParameters parms = new ININ.IceLib.Interactions.CallInteractionParameters(phone, ININ.IceLib.Interactions.CallMadeStage.Allocated);
                ININ.IceLib.Interactions.Interaction interaction = _interactionManager.MakeCall(parms);
                interaction.SetStringAttribute("idSoggetto", idOperazionale);
                interaction.SetStringAttribute("Agente_UserId", _session.UserId);
                interaction.SetStringAttribute("Agente_SMSESSION", parameters.AuthSession);
                interaction.SetStringAttribute("Agente_cb_clicktrack_id", parameters.clicktrackId);
            }
            catch(Exception ex)
            {
                _notificationService.Notify("Si è verificato un errore durante la chiamata", "click2call", NotificationType.Error, new TimeSpan(0, 0, 4));
                _trace.Exception(ex, "BIZMATICA:" + "click2call");
            }
}
        public void showInteraction(string interactionId)
        {
            MessageBox.Show(interactionId);
        }

        public void disconnectInteraction(string interactionId)
        {
            MessageBox.Show(interactionId);
        }
        public void setCurrentCustomer(string idOperazionale)
        {
            try {
                var parameters = Utilities.getInstance();
                _trace.Always("BIZMATICA: setCurrentCustomer " + idOperazionale.ToString());
                parameters.LastContextCustomer = idOperazionale;
                parameters.LastContextCustomerDate = DateTime.Now;
                if (SelectedInteraction != null) {
                    _trace.Always("BIZMATICA: Interazione selezionata " + SelectedInteraction.InteractionId.ToString());
                    SelectedInteraction.SetAttribute("Agente_UserId", _session.UserId);
                    SelectedInteraction.SetAttribute("Agente_SMSESSION", parameters.AuthSession);
                    SelectedInteraction.SetAttribute("Agente_cb_clicktrack_id", parameters.clicktrackId);

                    if(SelectedInteraction.GetAttribute("idSoggetto").Equals("-1") || SelectedInteraction.GetAttribute("idSoggetto").Equals(""))
                        SelectedInteraction.SetAttribute("idSoggetto", idOperazionale);


                    _trace.Always("BIZMATICA:" + "BIZMATICA:custom notification Context interazione");
                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "Contestualizzazione", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { SelectedInteraction.InteractionId,"" };

                    request.Write(parametri);

                
                    _Notifier.SendServerRequestNoResponse(request);
                    _trace.Always("BIZMATICA:" + "BIZMATICA:custom notification Context interazione inviata");
                }
                else{

                    _trace.Always("BIZMATICA: Nessuna Interazione selezionata");
                    CustomMessageHeader header = new CustomMessageHeader(CustomMessageType.ServerNotification, "Contestualizzazione", _session.UserId);
                    CustomRequest request = new CustomRequest(header);
                    string[] parametri = new string[] { "", idOperazionale, _session.UserId, parameters.AuthSession, parameters.clicktrackId };

                    request.Write(parametri);

                    _trace.Always("BIZMATICA:custom notification Context interazione solo soggetto\n" + parametri[0]);
                    _Notifier.SendServerRequestNoResponse(request);
                    _trace.Always("BIZMATICA:" + "BIZMATICA:custom notification solo soggetto inviata");
                }
            }
            catch(Exception ex)
            {
                _notificationService.Notify("Si è verificato un errore.", "Contestualizzazione", NotificationType.Error, new TimeSpan(0, 0, 4));
                _trace.Exception(ex, "BIZMATICA:" + "BIZMATICA:constestualizzazione");
            }
        }
        public void click2mail(string email, string idOperazionale)
        {
            MessageBox.Show(email);
        }
    }

}
